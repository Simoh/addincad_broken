#include "actionhook.h"

#include <QDebug>
#include <QRect>

#include "global_constants.h"

ActionHook::ActionHook(QObject *parent) :
    IAction(parent)
{
    clear();
}

ActionHook::ActionHook(int id_action, ElementInfoPtr element, QString comment, QImage image, QPoint point)
{
    m_id_action = id_action;
    m_ui_element = element;
    m_coordinate = point;
    m_comment = comment;
    m_image = image;
}

ActionHook::ActionHook(const QJsonObject &json, std::shared_ptr<Context> context)
{
    this->deserialize(json, context);
}

ActionHook::ActionHook(const ActionHook &action) :
    IAction(action)
{
    m_id_action = action.id_action();
    m_ui_element = action.element_info();
}

int ActionHook::id_action() const
{
    return m_id_action;
}

ElementInfoPtr ActionHook::element_info() const
{
    return m_ui_element;
}

bool ActionHook::is_client_action() const
{
    return m_id_action == WM_CLIENT_EVENT ;
}

QString ActionHook::comment() const
{
    if (m_comment.isEmpty()) {
        //generate comment
        QString preffix;
        switch (m_id_action)
        {
        case WM_CLIENT_CALL_MENU:
            preffix = "Вызвать меню в клиентской области ";
            break;
        case WM_CLIENT_EVENT:
            preffix = "Действие в клиенской области ";
            break;
        default:
            preffix = "Нажмите на ";
            break;
        }
        return preffix + m_ui_element->class_name() + " " + m_ui_element->name();
    } else
        return m_comment;
}

void ActionHook::deserialize(const QJsonObject &json, std::shared_ptr<Context> context)
{
    try
    {
        m_ui_element = context.get()->ui_tree().get_EI(json["id_element"].toInt());
        m_id_action = json["id_action"].toInt();

        if (m_id_action == WM_CLIENT_EVENT || m_id_action == WM_CLIENT_CALL_MENU) {
            QJsonObject coord = json["coordinate"].toObject();
            m_coordinate = QPoint(coord["x"].toInt(),
                                  coord["y"].toInt());
        }
        IAction::deserialize(json);
    }
    catch (const std::exception &error)
    {
        qCritical() << "Journal has incorrect data!: " << error.what();
        this->clear();
    }
}

void ActionHook::serialize(QJsonObject &json)
{
    try
    {
        json["id_element"] = m_ui_element->id_element();
        json["id_action"] = m_id_action;

        if (m_id_action == WM_CLIENT_EVENT) {
            QJsonObject coord;
            coord["x"] = m_coordinate.x();
            coord["y"] = m_coordinate.y();
            json["coordinate"] = coord;
        }
    }
    catch (const std::exception &error)
    {
        qCritical() << "Journal has incorrect data!: " << error.what();
        //TODO rethrow exception to top layer
    }
    IAction::serialize(json);
}

bool ActionHook::compare(const IAction &_rhs) const
{
    auto rhs_ptr = dynamic_cast<const ActionHook*>(&_rhs);
    //compare for hook action
    if (this->id_action() == rhs_ptr->id_action())
    {
        if(this->id_action() == WM_CLIENT_EVENT) {
            if (this->element_info() == rhs_ptr->element_info())
            {
                //TODO make QRect dynamic
                return (QRect(-5, -5, 5, 5).contains(this->coordinate() - rhs_ptr->coordinate()));
            }
        }
        return (this->element_info() == rhs_ptr->element_info());
    }

    return false;
}

bool operator==(const ActionHook &_lhs, const ActionHook &_rhs)
{
    return _lhs.compare(_rhs);
}

void ActionHook::clear()
{
    m_id_action = -1;
    m_ui_element = nullptr;
    IAction::clear();
}

bool ActionHook::is_empty()
{
    return m_id_action == -1;
}
