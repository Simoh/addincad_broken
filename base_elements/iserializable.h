#ifndef ISERIALIZABLE_H
#define ISERIALIZABLE_H

#include <QJsonObject>

class ISerializable
{
public:
    virtual ~ISerializable() {}
    virtual void serialize(QJsonObject& json) = 0;
    virtual void deserialize(const QJsonObject& json) = 0;
};

#endif // ISERIALIZABLE_H
