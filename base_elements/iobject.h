#ifndef IOBJECT
#define IOBJECT

#include <QObject>
#include "global_constants.h"

class IObject: public QObject
{
    Q_OBJECT
protected:
    Global_constant::state::States m_state;
public slots:
    virtual void start(ulong target_pid) = 0;
    virtual void stop(QString reason = Global_constant::stop_reasons::default_reason.c_str()) = 0;
    virtual void pause() = 0;
    virtual int get_status() = 0;

signals:
    void started();
    void stopped(QString reason = Global_constant::stop_reasons::default_reason.c_str());
    void state_changed(QString);
    void paused();
    void resumed();
};

#endif // IOBJECT

