#ifndef ACTIONINTEGRATION_H
#define ACTIONINTEGRATION_H

#include <QObject>
#include "action.h"

class ActionIntegration final : public IAction
{
    Q_OBJECT
public:
    explicit ActionIntegration(QObject *parent = nullptr);
    ActionIntegration(const QJsonObject& json);

    QString app_data() const;
    void set_app_data(const QString& raw_data);

    void deserialize(const QJsonObject& json);
    void serialize(QJsonObject& json);

    ActionIntegration &operator=(const ActionIntegration& _lhs);
    friend bool operator==(const ActionIntegration& _lhs, const ActionIntegration& _rhs);

    void clear();
    bool is_empty() override;

private:
    QString m_app_data;
};

#endif // ACTIONINTEGRATION_H
