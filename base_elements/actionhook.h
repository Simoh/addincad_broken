#ifndef ACTIONHOOK_H
#define ACTIONHOOK_H

#include "action.h"
#include "configurator/context.h"
#include <QObject>

#include <memory>

class ActionHook final : public IAction
{
    Q_OBJECT
public:
    ActionHook(QObject* parent = 0);
    ActionHook(int id_action, ElementInfoPtr element, QString comment, QImage image = QImage(), QPoint point = QPoint());
    ActionHook(const QJsonObject& json, std::shared_ptr<Context> context);
    ActionHook(const ActionHook& action);

    int id_action() const;
    ElementInfoPtr element_info() const;
    bool is_client_action() const;
    QString comment() const override;

    void deserialize(const QJsonObject& json, std::shared_ptr<Context> context);
    void serialize(QJsonObject& json) override;

    //TODO ActionHook &operator=(const ActionHook& _lhs);
    friend bool operator==(const ActionHook& _lhs, const ActionHook& _rhs);
    bool compare(const IAction& _rhs) const override;

    void clear() override;
    bool is_empty() override;

private:
    int m_id_action;
    ElementInfoPtr m_ui_element;
};

#endif // ACTIONHOOK_H
