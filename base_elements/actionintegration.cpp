#include "actionintegration.h"

#include <QDebug>

ActionIntegration::ActionIntegration(QObject *parent) :
    IAction(parent)
{

}

ActionIntegration::ActionIntegration(const QJsonObject &json)
{
    this->deserialize(json);
}

QString ActionIntegration::app_data() const
{
    return m_app_data;
}

void ActionIntegration::set_app_data(const QString &raw_data)
{
    m_app_data = raw_data;
}

void ActionIntegration::deserialize(const QJsonObject &json)
{
    m_app_data = json["app_info"].toString();
    IAction::deserialize(json);
}

void ActionIntegration::serialize(QJsonObject &json)
{
    json["app_info"] = m_app_data;
    IAction::serialize(json);
}

ActionIntegration &ActionIntegration::operator=(const ActionIntegration &_lhs)
{
    m_app_data = _lhs.app_data();
    IAction::operator =(_lhs);
    return *this;
}

bool operator==(const ActionIntegration &_lhs, const ActionIntegration &_rhs)
{
    qDebug() << "Coordinate: " << _lhs.coordinate() - _rhs.coordinate();
    /*if (!_lhs.coordinate().isNull() && !_rhs.coordinate().isNull())
        if (!QRect(-5, -5, 5, 5).contains(_lhs.coordinate() - _rhs.coordinate()))
            return false;*/
    return (_lhs.app_data() == _rhs.app_data());
}

void ActionIntegration::clear()
{
    m_app_data.clear();
    IAction::clear();
}

bool ActionIntegration::is_empty()
{
    return m_app_data.isEmpty();
}
