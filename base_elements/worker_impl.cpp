#include "worker_impl.h"
#include <QDebug>
#include "service/globalsettings.h"

worker_impl::worker_impl()
{
    qRegisterMetaType<JournalSession>("JournalSession");

    //connections with data_receiver
    QObject::connect(&m_data_connection, &Connection_manager::recv_data_in_channel, this, &worker_impl::recv_data_in_channel);
    QObject::connect(&m_data_connection, &Connection_manager::dll_connection_refused, this, &worker_impl::stop);

    m_target_pid = 0;

    m_state = Global_constant::state::Stopped;
}

worker_impl::~worker_impl()
{
    QObject::disconnect(&m_data_connection, &Connection_manager::recv_data_in_channel, this, &worker_impl::recv_data_in_channel);
    QObject::disconnect(&m_data_connection, &Connection_manager::dll_connection_refused, this, &worker_impl::stop);
}

void worker_impl::clear()
{
    m_journal_session.clear();
    m_data_connection.disconnect_app();
    m_target_pid = 0;
}

void worker_impl::send_sig_stopped(QString reason)
{
    emit stopped(reason);
}

bool worker_impl::request_info_from_app(std::shared_ptr<IAction> action)
{
    return m_data_connection.send_msg_to_connection_app(dynamic_cast<ActionIntegration*>(action.get())->app_data());
}

void worker_impl::set_update_event_checker()
{
    QObject::connect(&m_data_connection, &Connection_manager::update_msg, this, &worker_impl::recv_update_event);
    //m_data_connection.set_update_watchdog(m_target_pid);
}

void worker_impl::start(ulong target_pid)
{
    m_target_pid = target_pid;

    if (m_journal_session.configurator() == nullptr) {
        emit state_changed(tr("Сбор информации о приложении"));
        m_journal_session = JournalSession(std::make_shared<Context>(target_pid));
    }

    //activate window
    ShowWindow(GlobalSettings::Instance().get_main_wnd(), true);
    SetForegroundWindow(GlobalSettings::Instance().get_main_wnd());

    //set connection with app
    emit state_changed(tr("Установка соединения"));
    if (m_journal_session.connection_type() == Global_constant::connection_type::auto_select.c_str()) {
        m_journal_session.set_connection_type(m_data_connection.set_connection_with_app(m_target_pid));
    }
    else if (m_journal_session.connection_type() == Global_constant::connection_type::hook.c_str()) {
        m_data_connection.set_connection_with_app_hook(m_target_pid);
    }
    else if (m_journal_session.connection_type() == Global_constant::connection_type::integration.c_str()) {
        m_data_connection.set_connection_with_app_integration(m_target_pid);
    }

    m_state = Global_constant::state::Started;

    emit started();
}

void worker_impl::start(ulong target_pid, std::shared_ptr<Context> config)
{
    m_journal_session = JournalSession(config);
    worker_impl::start(target_pid);
}

void worker_impl::start(ulong target_pid, JournalSession session, int position)
{
    m_journal_session = session;
    m_journal_session.remove(position, m_journal_session.get_size());
    worker_impl::start(target_pid);
}

void worker_impl::stop(QString reason)
{
    //disconnect from data receiver
    QObject::disconnect(&m_data_connection, &Connection_manager::update_msg, this, &worker_impl::recv_update_event);
    m_data_connection.disconnect_app();

    send_sig_stopped(reason);
    m_state = Global_constant::state::Stopped;
    this->clear();
}

void worker_impl::pause()
{
    switch (m_state)
    {
    case Global_constant::state::Paused:
    {
        m_data_connection.unset_pause();
        m_state = Global_constant::state::States::Started;
        emit resumed();
        break;
    }
    case Global_constant::state::Started:
    {
        m_data_connection.set_pause();
        m_state = Global_constant::state::States::Paused;
        emit paused();
        break;
    }
    }
}

void worker_impl::recv_data_in_channel(QString msg)
{
    qDebug() << "Receive message from hook from registrator";
    //parse data
    if ( m_journal_session.connection_type() == QString(Global_constant::connection_type::hook.c_str()) ) {
        m_journal_session.add_data( parser_data.parse_hook_event( msg, m_journal_session.configurator()) );

    } else if ( m_journal_session.connection_type() == QString(Global_constant::connection_type::integration.c_str()) ) {
        m_journal_session.add_data( parser_data.parse_integration_event(msg) );
    }

#ifdef TRIAL
    if (m_journal_session.get_size() > 10)
        stop(Global_constant::stop_reasons::trial_reason);
#else
    //notification for child class
    on_data_received();
#endif
}

void worker_impl::recv_update_event(int type)
{
    on_update_event(type);
}

int worker_impl::get_status()
{
    qDebug() << "get_status (" << m_state << ")";
    return m_state;
}
