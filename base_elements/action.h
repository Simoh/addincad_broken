#ifndef ACTION_H
#define ACTION_H

#include <QObject>
#include <QPoint>
#include <QImage>
#include <QByteArray>

#include "iserializable.h"

class IAction : public QObject,
                public ISerializable
{
    Q_OBJECT
public:
    explicit IAction(QObject *parent = 0);
    IAction(const IAction& _input);
    virtual ~IAction();

    virtual QString comment() const;
    void set_comment(const QString& comment);

    QPoint coordinate() const;
    void set_coordinate(const QPoint _point);

    QImage image() const;
    void set_image(const QImage& img);

    void deserialize(const QJsonObject& json) override;
    void serialize(QJsonObject& json) override;

    IAction &operator=(const IAction& _lhs);
    friend bool operator==(const IAction& _lhs, const IAction& _rhs);
    virtual bool compare(const IAction& _lhs) const;

    virtual void clear();
    virtual bool is_empty() = 0;

protected:
    QString m_comment;
    QPoint m_coordinate;
    QImage m_image;
};

#endif // ACTION_H
