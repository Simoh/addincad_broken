#ifndef ELEMENT_INFO_H
#define ELEMENT_INFO_H

#include <QObject>
#include "iserializable.h"

class ElementInfo;
typedef ElementInfo* ElementInfoPtr;

class ElementInfo : public QObject,
                    public ISerializable
{
    Q_OBJECT
public:
    ElementInfo(const ElementInfo& _input, ElementInfoPtr parent = nullptr);
    ElementInfo(const QJsonObject& json, ElementInfoPtr parent = nullptr);
    ElementInfo(QString class_name = QString(), QString name = QString());
    ~ElementInfo();

    int id_element() const;
    void set_id_element(const int& id_element);

    QString class_name() const;
    void set_class_name(const QString& class_name);

    QString name() const;
    void set_name(const QString& name);

    void set_parent(QString class_name = QString(), QString name = QString());
    ElementInfoPtr parent() const;

    std::list<ElementInfo>& children() const;
    ElementInfo& last_child();

    int add_child(const ElementInfo& name);


    bool isEmpty() const;
    static ElementInfo empty();

    void deserialize(const QJsonObject& json) override;
    void serialize(QJsonObject& json) override;

    friend bool operator==(const ElementInfo& _lhs, const ElementInfo& _rhs);
    friend bool operator!=(const ElementInfo& _lhs, const ElementInfo& _rhs);
    ElementInfo& operator=(const ElementInfo& _lhs);
private:
    int m_id_element;
    QString m_class_name;
    QString m_name;
    mutable std::list<ElementInfo> m_children;

    ElementInfoPtr m_parent_ptr;
};

#endif // ELEMENT_INFO_H
