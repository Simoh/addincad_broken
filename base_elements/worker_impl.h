#ifndef WORKER_IMPL_H
#define WORKER_IMPL_H

#include <QObject>

#include "base_elements/iobject.h"
#include "receiver_data.h"
#include "service/dataparser.h"
#include "journals/journal_session.h"
#include "configurator/context.h"

class worker_impl : public IObject
{
    Q_OBJECT
public:
    worker_impl();
    virtual ~worker_impl();

private slots:
    void recv_data_in_channel(QString msg);
    void recv_update_event(int type);

public slots:
    virtual void start(ulong target_pid) override;
    virtual void start(ulong target_pid, std::shared_ptr<Context> config);
    virtual void start(ulong target_pid, JournalSession session, int position = 0);
    virtual void stop(QString reason = Global_constant::stop_reasons::default_reason.c_str()) override;
    void pause() override;
    int get_status() override;

protected:
    virtual void on_data_received() {}
    virtual void clear();
    virtual void send_sig_stopped(QString reason);
    bool request_info_from_app(std::shared_ptr<IAction> action);

    void set_update_event_checker();
    virtual void on_update_event(int type) { Q_UNUSED(type)}

    JournalSession m_journal_session;

private:
    DataParser parser_data;
    Connection_manager m_data_connection;

    uint m_target_pid;
};

#endif // WORKER_IMPL_H
