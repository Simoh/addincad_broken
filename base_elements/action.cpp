#include "action.h"

#include <QDebug>
#include <QBuffer>
#include <QPixmap>

IAction::IAction(QObject *parent) : QObject(parent)
{

}

IAction::IAction(const IAction& _input)
{
    m_comment = _input.m_comment;
    m_coordinate = _input.m_coordinate;
    m_image = _input.m_image;
}

IAction::~IAction()
{
}

QString IAction::comment() const
{
    if (m_comment.isEmpty())
        return "Unknown action";
    return m_comment;
}

void IAction::set_comment(const QString& comment)
{
    m_comment = comment;
}

QPoint IAction::coordinate() const
{
    return m_coordinate;
}

void IAction::set_coordinate(const QPoint _point)
{
    m_coordinate = _point;
}

void IAction::set_image(const QImage &img)
{
    if (img == m_image)
        return;
    m_image = img;
}

QImage IAction::image() const
{
    return m_image;
}

void IAction::deserialize(const QJsonObject& json)
{
    if (json.contains("comment"))
        m_comment = json["comment"].toString();

    if (json.contains("image")) {
        QByteArray encoded = json["image"].toString().toLatin1();
        QPixmap tmp_pixmap;
        tmp_pixmap.loadFromData(QByteArray::fromBase64(encoded), "BMP"); //TODO COMPRESS IT
        m_image = tmp_pixmap.toImage();
    }
}

void IAction::serialize(QJsonObject& json)
{
    if (!m_comment.isEmpty())
        json["comment"] = m_comment;

    if (!m_image.isNull()) {
        QByteArray data;
        QBuffer buffer(&data);
        buffer.open(QIODevice::WriteOnly);
        m_image.save(&buffer, "BMP");
        auto encoded = buffer.data().toBase64(); //TODO COMPRESS IT
        json["image"] = QJsonValue(QString::fromLatin1(encoded));
    }
}

IAction &IAction::operator=(const IAction &_lhs)
{
    m_comment = _lhs.m_comment;
    m_coordinate = _lhs.m_coordinate;
    m_image = _lhs.m_image;
    return *this;
}

bool IAction::compare(const IAction &_lhs) const
{
    qDebug() << "comparing CAD_Action element";
    Q_UNUSED(_lhs);
    return true;
}

bool operator==(const IAction &_lhs, const IAction &_rhs)
{
    return _lhs.compare(_rhs);
}

void IAction::clear()
{
    m_comment.clear();
    this->m_coordinate = QPoint();
}
