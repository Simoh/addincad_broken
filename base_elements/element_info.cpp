#include "element_info.h"

#include <QJsonArray>
#include <iterator>

ElementInfo::~ElementInfo()
{

}

ElementInfo::ElementInfo(const ElementInfo& _input, ElementInfoPtr parent)
{
    m_class_name = _input.m_class_name;
    m_id_element = _input.m_id_element;
    m_name = _input.m_name;
    m_parent_ptr = parent;
    for(auto& itChild: _input.m_children) {
        m_children.emplace_back(itChild, this);
    }
}

ElementInfo::ElementInfo(QString class_name, QString name)
{
    this->set_class_name(class_name);
    this->set_name(name);
    m_parent_ptr = nullptr;
}

ElementInfo::ElementInfo(const QJsonObject &json, ElementInfoPtr parent)
{
    m_parent_ptr = parent;
    this->deserialize(json);
}

int ElementInfo::id_element() const
{
    return m_id_element;
}

void ElementInfo::set_id_element(const int& _id_element)
{
    m_id_element = _id_element;
}

QString ElementInfo::class_name() const
{
    return m_class_name;
}

void ElementInfo::set_class_name(const QString& _class_name)
{
    m_class_name = _class_name;
}

QString ElementInfo::name() const
{
    return m_name;
}

void ElementInfo::set_name(const QString& _name)
{
    m_name = _name;
}

void ElementInfo::set_parent(QString class_name, QString name)
{
    m_parent_ptr = new ElementInfo(class_name, name);
}

ElementInfoPtr ElementInfo::parent() const
{
    return m_parent_ptr;
}

std::list<ElementInfo> &ElementInfo::children() const
{
    return m_children;
}

int ElementInfo::add_child(const ElementInfo& child)
{
    //сделать проверку на - есть ли такой элемент уже?
    int new_id = m_children.empty() ? m_id_element + 1 :
                                      m_children.back().id_element() + 1;
    m_children.emplace_back(child, this);
    m_children.back().set_id_element(new_id);

    return new_id;
}

void ElementInfo::deserialize(const QJsonObject& json)
{
    this->m_id_element = json["id_element"].toDouble();
    this->m_class_name = json["class_name"].toString();
    this->m_name = json["name"].toString();

    if (json.contains("child")) {
        QJsonArray buf_arr = json["child"].toArray();
        for (int index_arr = 0; index_arr < buf_arr.size(); index_arr++) {
            m_children.emplace_back(buf_arr[index_arr].toObject(), this);
        }
    }
}

void ElementInfo::serialize(QJsonObject& json)
{
    json["id_element"] = this->m_id_element;
    json["class_name"] = this->m_class_name;
    json["name"] = this->m_name;
    if (this->m_children.size() > 0)
    {
        QJsonArray arr_child;
        for(auto itChild = m_children.begin(); itChild != m_children.end(); itChild++)
        {
            QJsonObject buf_child_object;
            itChild->serialize(buf_child_object);
            arr_child.append(buf_child_object);
        }
        json["child"] = arr_child;
    }
}

ElementInfo &ElementInfo::operator=(const ElementInfo &_lhs)
{
    this->set_class_name(_lhs.class_name());
    this->set_id_element(_lhs.id_element());
    this->set_name(_lhs.name());
    m_parent_ptr = _lhs.parent();
    for(auto& itChild: _lhs.m_children) {
        m_children.emplace_back(itChild, this);
    }

    return *this;
}

bool operator==(const ElementInfo &_lhs, const ElementInfo &_rhs)
{
    if (_lhs.class_name() == _rhs.class_name()) //TODO check its!!
        if (_lhs.name() == _rhs.name())
            return true;

    return false;
}

bool operator!=(const ElementInfo &_lhs, const ElementInfo &_rhs)
{
    return (_lhs == _rhs) ? false : true;
}

ElementInfo& ElementInfo::last_child()
{
   return this->m_children.back();
}

bool ElementInfo::isEmpty() const
{
    return (this->m_class_name.isEmpty());
}

ElementInfo ElementInfo::empty()
{
    ElementInfo result("","");
    result.set_id_element(-1);

    return result;
}
