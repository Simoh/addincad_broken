#include "receiver_data.h"
#include "ui_receiver_data.h"

#include <QDebug>

#include "global_constants.h"
#include "errors.h"
#include "connection/integrationconnection.h"
#include "connection/hook_connection.h"


Connection_manager::Connection_manager(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Receiver_data)
{
    ui->setupUi(this);

    QObject::connect(&update_watch, &Update_watchdog::update_msg, this, &Connection_manager::update_msg, Qt::QueuedConnection);
}

Connection_manager::~Connection_manager()
{
    delete ui;
}

void Connection_manager::connect_to_iconnection_signals()
{
    QObject::connect(m_connection.get(), &IConnection::receive_data_sig, this, &Connection_manager::data_in_link);
    QObject::connect(m_connection.get(), &IConnection::connection_lost_sig, this, &Connection_manager::connection_lost);
}

void Connection_manager::disconnect_from_iconnection_signals()
{
    QObject::disconnect(m_connection.get(), &IConnection::receive_data_sig, this, &Connection_manager::data_in_link);
    QObject::disconnect(m_connection.get(), &IConnection::connection_lost_sig, this, &Connection_manager::connection_lost);
}

QString Connection_manager::set_connection_with_app(unsigned int pid)
{
    m_connection = std::make_shared<IntegrationConnection>();
    if (m_connection.get()->connect_to_app(pid)) {
        qDebug() << "Integration connection was set";
        connect_to_iconnection_signals();
        return QString(Global_constant::connection_type::integration.c_str());
    }

    m_connection.reset(new Hook_connection());
    if (m_connection.get()->connect_to_app(pid)) {
        qDebug() << "Hook connection was set";
        connect_to_iconnection_signals();
        return QString(Global_constant::connection_type::hook.c_str());
    }

    throw Errors::Hook::cannot_connect_to_app;
}

void Connection_manager::set_connection_with_app_hook(unsigned int pid)
{
    m_connection = std::make_shared<Hook_connection>();
    if (m_connection.get()->connect_to_app(pid))
        connect_to_iconnection_signals();
    else
        throw Errors::Hook::cannot_connect_to_app;
}

void Connection_manager::set_connection_with_app_integration(unsigned int pid)
{
    m_connection = std::make_shared<IntegrationConnection>();
    if (m_connection.get()->connect_to_app(pid))
        connect_to_iconnection_signals();
    else
        throw Errors::Hook::cannot_connect_to_dll;
}

void Connection_manager::set_update_watchdog(unsigned int pid)
{
    update_watch.start(pid);
}

void Connection_manager::disconnect_app()
{
    if (update_watch.is_active())
        update_watch.stop();

    m_connection = nullptr;
}

void Connection_manager::set_pause()
{
    update_watch.pause();

    if (m_connection.get()->is_established()) {
        m_connection.get()->pause();
    }
}

void Connection_manager::unset_pause()
{
    update_watch.unpause();

    if (m_connection.get()->is_established()) {
        m_connection.get()->unpause();
    }
}

bool Connection_manager::send_msg_to_connection_app(QString raw_data)
{
    return dynamic_cast<IntegrationConnection*>(m_connection.get())->data_request(raw_data);
}

void Connection_manager::data_in_link(QString msg)
{
    qDebug() << "Receive data : " << msg;
    emit recv_data_in_channel(msg);
}

void Connection_manager::connection_lost()
{
    qDebug() << "Connection lost";
    this->disconnect_app();
    emit dll_connection_refused(Global_constant::stop_reasons::connection_refused.c_str());
}

void Connection_manager::disconnect_from_app()
{
    qDebug() << "Disconnect from target app";
    emit dll_connection_refused(Global_constant::stop_reasons::initiate_target_app.c_str());
}
