#include "journal_transaction.h"

#include <QJsonArray>

JournalTransaction::JournalTransaction()
{

}

JournalTransaction::~JournalTransaction()
{

}

void JournalTransaction::add_transaction(const Transaction& _input_transaction)
{
    transactions.push_back(_input_transaction);
}

void JournalTransaction::serialize(QJsonObject &json)
{
    QJsonArray result_arr;
    while(!transactions.empty())
    {
        QJsonObject buf_object;
        result_arr.append(buf_object);
        transactions.removeFirst();
    }
    json["data"] = result_arr;
}

void JournalTransaction::deserialize(const QJsonObject &_json)
{
    Q_UNUSED(_json);
}

bool JournalTransaction::clear()
{
    this->transactions.clear();
    return true;
}

