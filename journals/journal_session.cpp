#include "journal_session.h"
#include "global_constants.h"

#include <QJsonArray>
#include <QDebug>

#include "base_elements/actionhook.h"
#include "base_elements/actionintegration.h"

JournalSession::JournalSession(std::shared_ptr<Context> config) :
    m_context(config)
{
    m_connection_type = QString(Global_constant::connection_type::auto_select.c_str());
}

JournalSession::JournalSession(const JournalSession &session)
{
    m_data = session.get_data();
    m_context = session.configurator();
    m_connection_type = session.connection_type();
    m_name = session.m_name; //TODO fix it
}

JournalSession::JournalSession(const QJsonObject &json, std::shared_ptr<Context> config) :
    JournalSession(config)
{
    this->deserializing(json);
}

JournalSession::~JournalSession()
{
    m_data.clear();
    m_context = nullptr;
}

bool JournalSession::serializing(QJsonObject &json)
{
    QJsonArray result_arr;
    while(!m_data.empty())
    {
        QJsonObject buf_object;
        m_data.front().get()->serialize(buf_object);
        result_arr.append(buf_object);
        m_data.removeFirst();
    }
    json["data"] = result_arr;
    if (!m_name.isEmpty())
        json["name"] = m_name;
    json["connection"] = m_connection_type;

    return true;
}

bool JournalSession::deserializing(const QJsonObject &json)
{
    if (!json["data"].isArray())
        return false;

    if (!json.contains("connection"))
        return false;

    m_connection_type = json["connection"].toString();

    QJsonArray input_array = json["data"].toArray();
    for(int index_element = 0; index_element < input_array.size(); index_element++)
    {
        if (input_array[index_element].isObject())
        {
            QJsonObject buf_object = input_array[index_element].toObject();

            if ( m_connection_type == QString(Global_constant::connection_type::hook.c_str()) ) {
                m_data.push_back(std::make_shared<ActionHook>( buf_object, m_context) );
            } else if ( m_connection_type == QString(Global_constant::connection_type::integration.c_str()) ) {
                m_data.push_back(std::make_shared<ActionIntegration>(buf_object));
            }
            if (m_data.last().get()->is_empty()) {
                qCritical() << "Bad data in file!" << buf_object;
            }
        }
    }

    if (json.contains("name"))
        m_name = json["name"].toString();

    return true;
}

QStringList JournalSession::to_string_list() const
{
    QStringList result;
    for (int i = 0; i < m_data.size(); i++) {
        result.push_back(m_data[i].get()->comment());
    }

    return result;
}

bool JournalSession::clear()
{
    m_data.clear();
    m_name.clear();
    m_connection_type = QString(Global_constant::connection_type::auto_select.c_str());
    return true;
}

bool JournalSession::is_empty() const
{
    return m_data.isEmpty();
}

int JournalSession::get_size() const
{
    return m_data.size();
}

std::shared_ptr<IAction> JournalSession::get_action(int _id) const
{
    if (_id < 0 && _id >= m_data.size())
        return nullptr;

    return m_data[_id];
}

std::shared_ptr<IAction> JournalSession::get_last() const
{
    if (this->is_empty())
        return nullptr;

    return m_data.back();
}

QList<std::shared_ptr<IAction>> JournalSession::get_data() const
{
    return m_data;
}

bool JournalSession::remove(uint _id, size_t count)
{
    if ((_id + count) > m_data.size() )
        return false;

    m_data.erase(m_data.begin() + _id, m_data.begin() + _id + count);

    return true;
}

void JournalSession::set_comment_to_action(int _id, QString comment)
{
    if (_id < 0 || _id >= m_data.size())
        return;

    m_data[_id].get()->set_comment(comment);
}

QString JournalSession::connection_type() const
{
    return m_connection_type;
}

void JournalSession::set_connection_type(QString type)
{
    m_connection_type = type;
}

std::shared_ptr<Context> JournalSession::configurator() const
{
    return m_context;
}

JournalSession &JournalSession::operator=(const JournalSession &_lhs)
{
    /*for ( const auto& e : _lhs )
        m_data.push_back( std::make_shared<IAction>( *e ) );*/
    m_data = _lhs.get_data();
    m_connection_type = _lhs.connection_type();
    m_name = _lhs.m_name; //TODO fix it
    m_context = _lhs.configurator();
    return *this;
}

/*template<class T>
inline bool JournalSession::add_data(const T& _added_value)
{
    m_data.append(std::make_shared<T>(_added_value));
    return true;
}*/
