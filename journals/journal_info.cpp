#include "journal_info.h"

#include <QDebug>
#include <QJsonArray>

JournalInfo::JournalInfo()
{
    m_last_id = 0;
}

JournalInfo::JournalInfo(const JournalInfo &info)
{
    m_data = info.m_data;
}

JournalInfo::~JournalInfo()
{

}


bool JournalInfo::set_id_children(ElementInfo &_input)
{
    _input.set_id_element(++m_last_id);
    for (auto& itChildren : _input.children())
        set_id_children(itChildren);

    return true;
}

int JournalInfo::insert_element(const ElementInfo &data_value)
{
    int result_id = -1;

    //try to find element in tree
    for(auto& itJournal : m_data) {
        if (itJournal == data_value) {
            result_id = insert_element(itJournal, data_value);
            break;
        }
    }

    //add to root when not found
    if (result_id == -1) {
        m_data.push_back(data_value);
        set_id_children(m_data.back());
        result_id = m_last_id;
    }

    return result_id;
}

int JournalInfo::insert_element(ElementInfo &_source, const ElementInfo &_added)
{
    int result_id = -1;
    if (_source == _added) {
        //если в искомом есть ещё дети, то пройти по ним, иначе вернуть найденный id
        if (_added.children().empty())
            return result_id;

        for(auto& itChild : _source.children())
        {
            if (itChild == _added.children().front()) {
                result_id = insert_element(itChild, _added.children().front());
                if (result_id != -1)
                    return result_id;
            }
        }

        //если не найден в поддеревьях, то добавляем
        if (result_id == -1) {
            _source.add_child(_added.children().front());
            set_id_children(_source.children().back());
            result_id = m_last_id;
        }
    }

    return result_id;
}

int JournalInfo::get_id(const ElementInfo &_search_element)
{
    int result_id = -1;

    for(auto& itJournal : m_data)  {
        result_id = get_id(itJournal, _search_element);

        if (result_id != -1)
            return result_id;
    }

    return result_id;
}


ElementInfoPtr JournalInfo::get_EI(const std::list<ElementInfo> &_source, int _id) const
{
    for (auto itElement = _source.begin(); itElement != _source.end(); itElement++) {
        if ((*itElement).id_element() == _id ) {
            return (ElementInfoPtr)(&(*itElement));
        } else {
            ElementInfoPtr buf_ptr = get_EI(itElement->children(), _id);
            if (buf_ptr != nullptr)
                return buf_ptr;
        }
    }

   return nullptr;
}


ElementInfoPtr JournalInfo::get_EI(int _id) const
{
    return (_id < 0 ? nullptr : get_EI(m_data, _id));
}

bool JournalInfo::set_last_id(const std::list<ElementInfo> &_input_element)
{
    for (auto& itData : _input_element)
    {
        m_last_id = itData.id_element() > m_last_id ?
                    itData.id_element() : m_last_id;
        if (!itData.children().empty())
            set_last_id(itData.children());
    }

    return true;
}

int JournalInfo::get_id(const ElementInfo &_source, const ElementInfo &_search_element)
{
    int result_id = -1;
    if (_source == _search_element)
    {
        //запоминаем последнй id
        result_id = _source.id_element();
        //qDebug() << "Id is " << result_id;
        //если в искомом есть ещё дети, то пройти по ним, иначе вернуть найденный id
        if (_search_element.children().empty())
            return result_id;

        //ВНИМАНИЕ!!! УЧАСТОК КОДА МОЖЕТ БЫТЬ НЕДЕЙСТВИТЕЛЕН ПОСЛЕ ИЗМЕНЕНИЙ В КЛАССЕ ELEMENT_INFO
        for(auto& itChild : _source.children())
        {
            result_id = get_id(itChild, _search_element.children().front());
            //если найден - то возвращаем
            if (result_id != -1)
                return result_id;
        }
    }

    return result_id;
}

void JournalInfo::serialize(QJsonObject &json)
{
    QJsonArray result_arr;
    for(auto& it_element: m_data)
    {
        QJsonObject buf_object;
        it_element.serialize(buf_object);
        result_arr.append(buf_object);
    }
    json["data"] = result_arr;
}

void JournalInfo::deserialize(const QJsonObject &json)
{
    if (!json.contains("data"))
        return;

    //загрузить данные
    QJsonArray input_array = json["data"].toArray();
    for(int index_element = 0; index_element < input_array.size(); index_element++) {
        if (input_array[index_element].isObject()) {
            m_data.emplace_back(input_array[index_element].toObject());
        }
    }
    //присвоить last_id максимальному элементу в дереве
    set_last_id(m_data);
}

ElementInfo& JournalInfo::get_last_child()
{
    return m_data.back();
}

ElementInfo JournalInfo::vector2EI(std::vector<ElementInfo> _input_vector)
{
    ElementInfo result;

    ElementInfoPtr dataPtr = &result;
    //присваиваем значения первого элемента корню
    *dataPtr = _input_vector[0];
    for (size_t i = 1; i < _input_vector.size(); i++)
    {
        dataPtr->add_child(_input_vector[i]);
        dataPtr = &dataPtr->last_child();
    }

    return result;
}

bool JournalInfo::is_empty()
{
    return m_data.empty();
}

bool JournalInfo::clear()
{
    m_data.clear();
    m_last_id = 0;
    return true;
}

std::list<ElementInfo>& JournalInfo::get_data() const
{
    return m_data;
}

JournalInfo &JournalInfo::operator=(const JournalInfo &_lhs)
{
    m_data = _lhs.get_data();
    return *this;
}
