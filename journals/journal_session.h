#ifndef JOURNAL_SESSION_H
#define JOURNAL_SESSION_H

#include "base_elements/action.h"
#include "configurator/context.h"

#include <QList>
#include <QObject>

#include <memory>

class JournalSession : public QObject
{
    Q_OBJECT
private:
    QList<std::shared_ptr<IAction>> m_data;
    QString m_connection_type;
    std::shared_ptr<Context> m_context;

    bool deserializing(const QJsonObject &json);
public:
    explicit JournalSession(std::shared_ptr<Context> config = nullptr);
    JournalSession(const JournalSession& session);
    JournalSession(const QJsonObject &json, std::shared_ptr<Context> config);
    ~JournalSession();

    /* Property */
    QString m_name;

    /* Functions */
    template<class T>
    void add_data(const T& _added_value) { m_data.append(std::make_shared<T>(_added_value)); }

    bool serializing(QJsonObject &json);
    QStringList to_string_list() const;
    bool clear();
    bool is_empty() const;

    int get_size() const;
    std::shared_ptr<IAction> get_action(int _id) const;
    std::shared_ptr<IAction> get_last() const;
    QList<std::shared_ptr<IAction>> get_data() const;
    QString connection_type() const;

    bool remove(uint _id, size_t count = 1);
    void set_comment_to_action(int _id, QString comment);

    void set_connection_type(QString type);

    std::shared_ptr<Context> configurator() const;

    JournalSession &operator=(const JournalSession& _lhs);
};

#endif // JOURNAL_SESSION_H
