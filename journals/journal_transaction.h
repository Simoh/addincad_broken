#ifndef JOURNAL_TRANSACTION_H
#define JOURNAL_TRANSACTION_H

#include <QList>
#include "base_elements/transaction.h"
#include "base_elements/iserializable.h"

class JournalTransaction : public ISerializable
{
private:
    QList<Transaction> transactions;
public:
    JournalTransaction();
    ~JournalTransaction();
    void add_transaction(const Transaction& _input_transaction);
    void serialize(QJsonObject& _json) override;
    void deserialize(const QJsonObject& _json) override;
    bool clear();
};

#endif // JOURNAL_TRANSACTION_H
