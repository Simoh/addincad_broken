#ifndef JOURNAL_H
#define JOURNAL_H

#include <QObject>
#include <QList>

#include "base_elements/element_info.h"
#include "base_elements/iserializable.h"


class JournalInfo: public QObject,
                   public ISerializable
{
    Q_OBJECT
public:
    JournalInfo();
    JournalInfo(const JournalInfo& info);
    ~JournalInfo();

    int insert_element(const ElementInfo& data_value);
    int get_id(const ElementInfo& _search_element);
    ElementInfoPtr get_EI(int _id) const;
    ElementInfo& get_last_child();
    static ElementInfo vector2EI(std::vector<ElementInfo> _input_vector);

    void serialize(QJsonObject &json) override;
    void deserialize(const QJsonObject &json) override;
    bool is_empty();
    bool clear();

    std::list<ElementInfo>& get_data() const;

    JournalInfo &operator=(const JournalInfo& _lhs);

private:
    mutable std::list<ElementInfo> m_data;
    int m_last_id;

    int insert_element(ElementInfo& _source, const ElementInfo& _added);
    int get_id(const ElementInfo& _source, const ElementInfo& _search_element);
    bool set_id_children(ElementInfo& _input);
    ElementInfoPtr get_EI(const std::list<ElementInfo>& _source, int _id) const;
    bool set_last_id(const std::list<ElementInfo>& _input_element);
};

#endif // JOURNAL_H
