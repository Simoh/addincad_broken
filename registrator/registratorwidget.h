#ifndef REGISTRATORWIDGET_H
#define REGISTRATORWIDGET_H

#include <QWidget>
#include <QTreeWidgetItem>

namespace Ui {
class RegistratorWidget;
}

class RegistratorWidget : public QWidget
{
    Q_OBJECT

public:
    explicit RegistratorWidget(QWidget *parent = 0);
    ~RegistratorWidget();
    void clear();

private:
    Ui::RegistratorWidget *ui;

signals:
    void comment_changed(uint id, QString new_comment);

public slots:
    void add_action(QString description);
private slots:
    void on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column);
};

#endif // REGISTRATORWIDGET_H
