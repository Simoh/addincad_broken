#include "registratorwidget.h"
#include "ui_registratorwidget.h"

#include <QDebug>
#include "widgets/dialogcomment.h"

RegistratorWidget::RegistratorWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RegistratorWidget)
{
    ui->setupUi(this);
    ui->treeWidget->setColumnWidth(0, 30);
}

RegistratorWidget::~RegistratorWidget()
{
    delete ui;
}

void RegistratorWidget::clear()
{
    ui->treeWidget->clear();
}

void RegistratorWidget::add_action(QString description)
{
    if (ui->treeWidget->topLevelItemCount()) {
        ui->treeWidget->topLevelItem(ui->treeWidget->topLevelItemCount() - 1)->setIcon(0, QIcon());
        ui->treeWidget->topLevelItem(ui->treeWidget->topLevelItemCount() - 1)->setSelected(false);
    }
    QTreeWidgetItem *new_item = new QTreeWidgetItem;
    new_item->setIcon(0, QIcon(":/images/img/buttons/play_black.png"));
    new_item->setText(1, description);
    ui->treeWidget->addTopLevelItem(new_item);
    ui->treeWidget->topLevelItem(ui->treeWidget->topLevelItemCount() - 1)->setSelected(true);
}

void RegistratorWidget::on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    if (column == 1) {
        DialogComment dlg(this);
        QString new_comment = item->text(column);
        if (dlg.edit_comment(new_comment)) {
            item->setText(column, new_comment);
            emit comment_changed(ui->treeWidget->indexOfTopLevelItem(item), new_comment);
        }
    }
    //return select to last item
    item->setSelected(false);
    ui->treeWidget->topLevelItem(ui->treeWidget->topLevelItemCount() - 1)->setSelected(true);
}
