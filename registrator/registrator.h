#ifndef REGISTRATOR_H
#define REGISTRATOR_H

#include <QObject>

#include "base_elements/worker_impl.h"


class Registrator: public worker_impl
{
    Q_OBJECT
public:
    Registrator();
    ~Registrator();

public slots:
    void start(ulong target_pid) override;
    void start(ulong target_pid, std::shared_ptr<Context> config) override;
    void start(ulong target_pid, JournalSession session, int position = 0) override;
    void stop(QString reason = Global_constant::stop_reasons::default_reason.c_str()) override;

    void comment_changed(uint id, QString comment);

private:
    void initialize_enviroment(ulong target_pid);

protected:
    void send_sig_stopped(QString reason) override;
    void on_data_received() override;

signals:
    void stopped(QString reason, JournalSession session);
    void action_saved(QString description);
};

#endif // REGISTRATOR_H
