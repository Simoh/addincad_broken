#include "registrator.h"
#include <QApplication>
#include <QDebug>

#include "errors.h"
#include "service/globalsettings.h"


Registrator::Registrator()
{
    QObject::setObjectName("Registrator");
}

Registrator::~Registrator()
{
    //unset hook if started
    if (m_state != Global_constant::state::States::Stopped)
        clear();
}

/***************************************************************************************************
 *
 *                                P U B L I C    S L O T S
 *
 **************************************************************************************************/

void Registrator::start(ulong target_pid)
{
    qDebug() << "Receive signal START";
    try
    {
        initialize_enviroment(target_pid);
        worker_impl::start(target_pid);
    }
    catch (const cancel_event &_event)
    {
        qDebug() << _event.what();
        worker_impl::stop();
    }
    catch(const critical_error &error)
    {
        qCritical() << error.what();
        worker_impl::stop(error.what());
    }
    catch(const logic_error &error)
    {
        qWarning() << error.what();
        worker_impl::stop(Global_constant::stop_reasons::incorrect_input_data.c_str());
    }

    qDebug()<< "registration was started";
}

void Registrator::start(ulong target_pid, std::shared_ptr<Context> config)
{ //WARNIGN need rafactoring
    qDebug() << "Receive signal START";
    try
    {
        initialize_enviroment(target_pid);
        worker_impl::start(target_pid, config);
    }
    catch (const cancel_event &_event)
    {
        qDebug() << _event.what();
        clear();
        worker_impl::stop();
    }
    catch(const critical_error &error)
    {
        qCritical() << error.what();
        clear();
        worker_impl::stop(error.what());
    }
    catch(const logic_error &error)
    {
        qWarning() << error.what();
        clear();
        worker_impl::stop(Global_constant::stop_reasons::incorrect_input_data.c_str());
    }

    qDebug()<< "registration was started";
}

void Registrator::start(ulong target_pid, JournalSession session, int position)
{
    //WARNIGN need rafactoring
    qDebug() << "Receive signal START";
    try
    {
        initialize_enviroment(target_pid);
        worker_impl::start(target_pid, session, position);
    }
    catch (const cancel_event &_event)
    {
        qDebug() << _event.what();
        clear();
        worker_impl::stop();
    }
    catch(const critical_error &error)
    {
        qCritical() << error.what();
        clear();
        worker_impl::stop(error.what());
    }
    catch(const logic_error &error)
    {
        qWarning() << error.what();
        clear();
        worker_impl::stop(Global_constant::stop_reasons::incorrect_input_data.c_str());
    }

    qDebug()<< "registration was started";
}

void Registrator::stop(QString reason)
{
    qDebug() << "Receive signal STOP: " << reason;
    worker_impl::stop(reason);
    qDebug() << "************STOP REGISTRATION************";
}

void Registrator::comment_changed(uint id, QString comment)
{
    this->m_journal_session.set_comment_to_action(id, comment);
}

/***************************************************************************************************
 *
 *                                  F U N C T I O N S
 *
 **************************************************************************************************/

/**
 * @brief Function initialize receiver object, get process ID,
 * @param _target_window HWND window which be used for registration
 * @throw cancel_event if user cancel change file
 * @throw critical_error if some internal error happen
 * @throw std::logic_error if error in data base format or target application is closed
 */
void Registrator::initialize_enviroment(ulong target_pid)
{
    qDebug() << "************REGISTRATOR INITIALIZE************";

    if (m_state != Global_constant::state::States::Stopped)
        throw Errors::Registrator::registrator_cannot_start;

    this->clear();

    GlobalSettings::Instance().set_last_error_info("");

    //проверка выходных данных
    if (!Scanner::Instance().process_is_valid(target_pid))
        throw Errors::target_app_is_closed;
}

void Registrator::on_data_received()
{
    emit action_saved( m_journal_session.get_last()->comment() );
}

void Registrator::send_sig_stopped(QString reason)
{
    emit stopped(reason, m_journal_session);
}
