#ifndef PROCESS_MONITOR_H
#define PROCESS_MONITOR_H

#include <QObject>
#include <QStringList>
#include <Windows.h>
#include <map>

class Process_monitor : public QObject
{
    Q_OBJECT
public:
    static Process_monitor& Instance();
    QStringList get_open_processes_names();
    unsigned long get_pid_by_string(QString _str);
    HWND get_hwnd_by_string(QString _str);
    //get health process
    //get z-order place window
private:
    static BOOL CALLBACK EnumWindowsGetCheckBox(HWND hwnd, LPARAM lParam);
    std::map<QString, std::pair<unsigned long, HWND>> pid_map;
protected:
    explicit Process_monitor(QObject *parent = 0);
    ~Process_monitor();
    Process_monitor(Process_monitor const&) = delete;
    Process_monitor& operator= (Process_monitor const&) = delete;

signals:

public slots:
};

#endif // PROCESS_MONITOR_H
