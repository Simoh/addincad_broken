#include "runtime_object.h"
#include <QDebug>

RuntimeObject::RuntimeObject(IAccessible *pacc, long child_id):
    m_hwnd(NULL),
    m_child_id(child_id),
    m_pacc(NULL)
{
    HRESULT hr;
    hr = WindowFromAccessibleObject(pacc, &m_hwnd);
    if (hr == S_OK)
    {
        //this need for create new pointer to IAccessible UI element
        hr = AccessibleObjectFromWindow(m_hwnd, 0, IID_IAccessible, (void**)&m_pacc);
    }
}

RuntimeObject::RuntimeObject(HWND hwnd):
    m_hwnd(hwnd),
    m_child_id(CHILDID_SELF),
    m_pacc(NULL)
{
    AccessibleObjectFromWindow(m_hwnd, 0, IID_IAccessible, (void**)&m_pacc);
}

RuntimeObject::~RuntimeObject()
{
    if (m_pacc)
        m_pacc->Release();
}

void RuntimeObject::clear()
{
    m_child_id = CHILDID_SELF;
    m_hwnd = NULL;
    if (m_pacc)
        m_pacc->Release();
    m_pacc = NULL;
}

bool RuntimeObject::is_empty()
{
    return (m_hwnd == NULL);
}

QRect RuntimeObject::get_rect()
{
    QRect result;

    if (!m_pacc)
    {
        qCritical() << "Get rect last null accessible element";
        return result;
    }

    long x, y, width, height;
    VARIANT var;
    var.vt = VT_I4;
    var.lVal = m_child_id;

    HRESULT hr = this->m_pacc->accLocation(&x, &y, &width, &height, var);
    if (SUCCEEDED(hr))
        result = QRect(x, y, width, height);
    else
        qDebug() << "accLocation error";

    return result;
}

IAccessible *RuntimeObject::pacc() const
{
    return m_pacc;
}

HWND RuntimeObject::hwnd() const
{
    return m_hwnd;
}

long RuntimeObject::child_id() const
{
    return m_child_id;
}

RuntimeObject &RuntimeObject::operator=(const RuntimeObject &_lhs)
{
    this->m_hwnd = _lhs.m_hwnd;
    this->m_child_id = _lhs.m_child_id;
    if (this->m_pacc)
        this->m_pacc->Release();
    AccessibleObjectFromWindow(this->m_hwnd, 0, IID_IAccessible, (void**)&this->m_pacc);
    return *this;
}
