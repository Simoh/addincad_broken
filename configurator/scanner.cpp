#include "scanner.h"
#include "service/globalsettings.h"
#include "errors.h"

#include <tlhelp32.h>
#include <QDebug>

Scanner &Scanner::Instance()
{
    static Scanner _instance;
    return _instance;
}

Scanner::Scanner() { }

Scanner::~Scanner() { }

bool Scanner::scan_interface(JournalInfo& _info, ulong target_pid)
{
    if (!process_is_valid(target_pid))
        return false;

    std::vector<unsigned long> thread_ids = get_thread_ids(target_pid);
    if (thread_ids.empty()) {
        qCritical() << "Thread ids is empty";
        return false;
    }

    for(auto thread_id_iter : thread_ids) {
        EnumThreadWindows(thread_id_iter, EnumWndProc, (LPARAM)(&_info));
    }

    return true;
}

ElementInfo Scanner::get_element_under_cursor()
{
    ElementInfo result;

    POINT cursor_pt;
    IAccessible *current_pacc = NULL;
    VARIANT current_child;

    GetCursorPos(&cursor_pt);
    HRESULT hr = AccessibleObjectFromPoint(cursor_pt, &current_pacc, &current_child);
    if (FAILED(hr))
        return result;

    result.set_name(get_name(current_pacc, CHILDID_SELF));
    result.set_class_name(get_role(current_pacc, CHILDID_SELF));

    current_pacc->Release();

    return result;
}

ElementInfo Scanner::get_ui_info(HWND hwnd, ulong idObject, ulong idChild)
{
    ElementInfo result;
    IAccessible *pAcc = NULL;
    VARIANT varChild;
    HRESULT hr = AccessibleObjectFromEvent(hwnd, idObject, idChild, &pAcc, &varChild);

    result.set_name(get_name(pAcc, idChild));
    result.set_class_name(get_role(pAcc, idChild));
    ElementInfoPtr top_element = &result;

    while (hr == S_OK && pAcc != NULL) {
        //get parent
        IDispatch* disp_pAcc = NULL;
        hr = pAcc->get_accParent(&disp_pAcc);
        //if get info parent is success then pAcc = parent
        if (hr == S_OK && disp_pAcc != NULL) {
            hr = disp_pAcc->QueryInterface(IID_IAccessible, (void**)&pAcc);
            if (hr == S_OK) {
                top_element->set_parent(get_role(pAcc, CHILDID_SELF),
                                        get_name(pAcc, CHILDID_SELF) );
                top_element = top_element->parent();
            }
        }
    }

    pAcc->Release();

    return result;
}

std::pair<IAccessible *, ulong> Scanner::get_accessible(ElementInfoPtr)
{
    IAccessible *pacc = NULL;
    ulong child_id = CHILDID_SELF;
    return std::make_pair(pacc, child_id);
}

BOOL Scanner::EnumWndProc(HWND _target_wnd, LPARAM lParam)
{
    qDebug() << "EnumWndProc started hwnd:" << _target_wnd;
    JournalInfo* _info = reinterpret_cast<JournalInfo*>(lParam);
    //scan all windows this proc
    IAccessible *pacc = NULL;

    HRESULT hr = AccessibleObjectFromWindow(_target_wnd, 0, IID_IAccessible, (void**)&pacc);
    if (FAILED(hr))
        return false;

    //добавляем текущее окно в EI
    ElementInfo main(Scanner::Instance().get_role(pacc, CHILDID_SELF),
                      Scanner::Instance().get_name(pacc, CHILDID_SELF));

    //ищем потомков этого окна
    Scanner::Instance().walk_tree_Accessible(pacc, main);

    //добавляем в журнал
    _info->insert_element(main);

    pacc->Release();

    return true;
}

BOOL Scanner::EnumWndProcToValidElement(HWND _target_wnd, LPARAM lParam)
{
    vector<ElementInfo>* _info = reinterpret_cast<vector<ElementInfo>*>(lParam);

    IAccessible *pacc = NULL;

    HRESULT hr = AccessibleObjectFromWindow(_target_wnd, 0, IID_IAccessible, (void**)&pacc);
    if (FAILED(hr))
        return FALSE;

    bool result = !Scanner::Instance().find_element_info(pacc, *_info);
    pacc->Release();

    return result;
}

HRESULT Scanner::walk_tree_Accessible(IAccessible *_input_acc, ElementInfo& _parent)
{
    long childCount = 0;
    long returnCount = 0;

    HRESULT hr = _input_acc->get_accChildCount(&childCount);

    if (childCount == 0)
        return S_OK;

    VARIANT *pArray = new VARIANT[childCount];
    hr = ::AccessibleChildren(_input_acc, 0L, childCount, pArray, &returnCount);

    if (FAILED(hr))
        return hr;

    for (int x = 0; x < returnCount; x++)
    {
        VARIANT vtChild = pArray[x];
        if (vtChild.vt == VT_DISPATCH)
        {
            //если есть потомки
            IDispatch *pDisp = vtChild.pdispVal;
            IAccessible *pAccChild = NULL;

            hr = pDisp->QueryInterface(IID_IAccessible, (void**)&pAccChild);
            if (hr == S_OK)
            {
                ElementInfo main(get_role(pAccChild, CHILDID_SELF),
                                  get_name(pAccChild, CHILDID_SELF));
                _parent.add_child(main);

                walk_tree_Accessible(pAccChild, _parent.last_child());

                pAccChild->Release();
            }
            pDisp->Release();
        }
        else
        {
            //если нет потомков
            ElementInfo main;
            main.set_class_name(get_role(_input_acc, vtChild.lVal));
            if (main.class_name().count() > 0)
            {
                main.set_name(get_name(_input_acc, vtChild.lVal));
                _parent.add_child(main);
            }
        }
    }

    delete[] pArray;
    return S_OK;
}

bool Scanner::find_element_info(IAccessible *_input_acc, vector<ElementInfo>& _info)
{
    bool result = false;

    //получить ElementInfo текущего элемента
    ElementInfo current_EI(get_role(_input_acc, CHILDID_SELF),
                            get_name(_input_acc, CHILDID_SELF));

    //если совпадает, то удаляем элемент из очереди и идём по потомкам
    if (current_EI == _info.front())
    {
        qDebug() << "Equals elements. Name " << current_EI.name();
        //извлекаем первый элемент
        _info.erase(_info.begin(), _info.begin() + 1);
        //Save pointer to element in internal structure
        last_accessible = RuntimeObject(_input_acc, CHILDID_SELF);
        //если больше элементов в очереди нет, то выходим
        if (_info.empty())
            return true;
    }
    else
        return result; //иначе выходим из цикла

    //идём по предкам
    long childCount = 0;
    long returnCount = 0;

    HRESULT hr = _input_acc->get_accChildCount(&childCount);

    if (childCount == 0)
        return result;

    VARIANT *pArray = new VARIANT[childCount];
    hr = ::AccessibleChildren(_input_acc, 0L, childCount, pArray, &returnCount);

    if (FAILED(hr))
        return result;

    for (int x = 0; (x < returnCount) && (!result); x++)
    {
        VARIANT vtChild = pArray[x];
        if (vtChild.vt == VT_DISPATCH)
        {
            //если есть потомки
            IDispatch *pDisp = vtChild.pdispVal;
            IAccessible *pAccChild = NULL;

            hr = pDisp->QueryInterface(IID_IAccessible, (void**)&pAccChild);
            if (hr == S_OK)
            {
                //если значение найдено, то выходим из цикла
                result = find_element_info(pAccChild, _info);
                //If element was setting in last search - we shouldn`t destroy him
                pAccChild->Release();
            }
            pDisp->Release();
        }
        else
        {
            //TODO если нет потомков
            qDebug() << "Нет потомков";
            //TODO if elements is equals - return true
        }
    }

    delete[] pArray;
    return result;
}

QString Scanner::get_name(IAccessible *pAcc, long _child_id)
{
    QString result;
    if (pAcc == NULL)
        return result;

    BSTR name;
    VARIANT vtChild;
    vtChild.vt = VT_I4;
    vtChild.lVal = _child_id;
    HRESULT res = pAcc->get_accName(vtChild, &name);
    if (res == S_OK && name != NULL)
    {
        wstring str_buf = name;
        result = QString::fromWCharArray(str_buf.c_str());

        SysFreeString(name);
    }

    return result;
}

QString Scanner::get_role(IAccessible* pAcc, long _child_id)
{
    QString result;
    unsigned long roleId;
    if (pAcc == NULL)
    {
        return result;
    }
    VARIANT varChild;
    varChild.vt = VT_I4;
    varChild.lVal = _child_id;
    VARIANT varResult;
    HRESULT hr = pAcc->get_accRole(varChild, &varResult);
    if ((hr == S_OK) && (varResult.vt == VT_I4))
    {
        roleId = varResult.lVal;
        UINT   roleLength;
        LPTSTR lpszRoleString;

        // Get the length of the string.
        roleLength = GetRoleText(roleId, NULL, 0);

        // Allocate memory for the string. Add one character to
        // the length you got in the previous call to make room
        // for the null character.
        lpszRoleString = (LPTSTR)malloc((roleLength + 1) * sizeof(TCHAR));
        if (lpszRoleString != NULL)
        {
            // Get the string.
            GetRoleText(roleId, lpszRoleString, roleLength + 1);
            result = QString::fromWCharArray(lpszRoleString);
            // Free the allocated memory
            free(lpszRoleString);
        }
        else
        {
            return result;
        }
    }
    return result;
}

/**
 * @brief This function return state process (run or died)
 * @param _process_id target process ID
 * @return true or false
 */
bool Scanner::process_is_valid(unsigned long _process_id)
{
    return GetProcessVersion(_process_id) == 0 ? false : true;
}

bool Scanner::element_is_valid(vector<ElementInfo> _info, unsigned long process_id)
{
    qDebug() << "start function for validation element";

    std::vector<unsigned long> thread_ids = get_thread_ids(process_id);
    if (thread_ids.empty()) {
        qCritical() << "Thread ids is empty";
        return false;
    }

    auto init_size = _info.size();
    last_accessible.clear();
    for(auto thread_id_iter : thread_ids) {
        EnumThreadWindows(thread_id_iter, EnumWndProcToValidElement, (LPARAM)(&_info));
        if (init_size != _info.size())  break;
    }
    return _info.empty();
}

/**
 * @brief function search element and put last founded element to hint_inst
 * @param _info
 * @param process_id
 * @param hint_inst
 * @return if target element was founded return true. In other case - false
 */
bool Scanner::element_is_valid(vector<ElementInfo> _info, unsigned long process_id, RuntimeObject &hint_inst)
{
    qDebug() << "element" << _info.back().name() << " is valid to proc: " << process_id;
    bool result = element_is_valid(_info, process_id);
    hint_inst = last_accessible;
    return result;
}

//EnergyCS path
bool Scanner::find_coordinate(IAccessible *pAcc, RuntimeObject& coordinate, RuntimeObject& scale)
{
    bool result = false;
    if (pAcc == NULL)
        return result;

    long childCount = 0;
    long returnCount = 0;

    HRESULT hr = pAcc->get_accChildCount(&childCount);

    if (childCount == 0)
        return result;

    VARIANT *pArray = new VARIANT[childCount];
    hr = ::AccessibleChildren(pAcc, 0L, childCount, pArray, &returnCount);

    if (FAILED(hr))
        return result;

    for (int x = 0; x < returnCount; x++)
    {
        VARIANT vtChild = pArray[x];
        if (vtChild.vt == VT_DISPATCH)
        {
            //если есть потомки
            IDispatch *pDisp = vtChild.pdispVal;
            IAccessible *pAccChild = NULL;

            hr = pDisp->QueryInterface(IID_IAccessible, (void**)&pAccChild);
            if (hr == S_OK)
            {
                if (get_role(pAccChild, CHILDID_SELF) == "строка состояния")
                {
                    /* попытаться найти координаты и масштаб*/
                    if (find_coordinate(pAccChild, coordinate) &&
                            find_scale(pAccChild, scale))
                    {
                        pAccChild->Release();
                        pDisp->Release();

                        return true;
                    }
                }
                result = find_coordinate(pAccChild, coordinate, scale);
            }

            pAccChild->Release();
            pDisp->Release();

            if (!result)
            {
                delete [] pArray;
                return result;
            }
        }
    }

    delete [] pArray;
    return result;
}

QPoint Scanner::get_coordinate_energycs(RuntimeObject &coordinate)
{
    QPoint result;

    QString name = get_name(coordinate.pacc(), coordinate.child_id());
    int colon_pos = name.indexOf(':');
    if (colon_pos != -1)
    {
        result.setX(name.left(colon_pos).toInt());
        result.setY(name.mid(colon_pos + 1, name.length()).toInt());
    }
    return result;
}

int Scanner::get_scale_energycs(RuntimeObject &scale)
{
    int result = 0;

    QString name = get_name(scale.pacc(), scale.child_id());
    auto colon_pos = name.indexOf('%');
    if (colon_pos != -1)
    {
        result = name.left(colon_pos).toInt();
    }

    return result;
}

HWND Scanner::get_element_hwnd(vector<ElementInfo> info_hierarchy, uint pid)
{

    return NULL;
}

bool Scanner::find_coordinate(IAccessible *pAcc, RuntimeObject& coordinate)
{
    long childCount = 0;
    HRESULT hr = pAcc->get_accChildCount(&childCount);

    if (FAILED(hr))
        return false;

    for (int i = 1; i < childCount; i++)
    {
        QString name = get_name(pAcc, i);
        int colon_pos = name.indexOf(':');
        if (colon_pos != -1)
        {
            coordinate = RuntimeObject(pAcc, i);
            return true;
        }
    }

    return false;
}


bool Scanner::find_scale(IAccessible *pAcc, RuntimeObject &scale)
{
    long childCount = 0;
    HRESULT hr = pAcc->get_accChildCount(&childCount);

    if (FAILED(hr))
        return false;

    for (int i = 1; i < childCount; i++)
    {
        QString name = get_name(pAcc, i);
        auto colon_pos = name.indexOf('%');
        if (colon_pos != -1)
        {
            scale = RuntimeObject(pAcc, i);
            return true;
        }
    }

    return false;
}

std::vector<unsigned long> Scanner::get_thread_ids(unsigned long process_id)
{
    qDebug() << "get_thread_ids started for process: " << process_id;

    std::vector<unsigned long> result;

    HANDLE hThreadSnap = INVALID_HANDLE_VALUE;
    THREADENTRY32 te32;

    // Take a snapshot of all running threads
    hThreadSnap = CreateToolhelp32Snapshot( TH32CS_SNAPTHREAD, 0 );
    if( hThreadSnap == INVALID_HANDLE_VALUE )
        return result;

    // Fill in the size of the structure before using it.
    te32.dwSize = sizeof(THREADENTRY32);

    // Retrieve information about the first thread,
    // and exit if unsuccessful
    if( !Thread32First( hThreadSnap, &te32 ) )
    {
        qCritical() << "cannot get thread ids";
        CloseHandle( hThreadSnap );     // Must clean up the snapshot object!
        return result;
    }

    // Now walk the thread list of the system and search threads
    do
    {
        if( te32.th32OwnerProcessID == process_id )
        {
            qDebug() << "Add thread id: " << te32.th32ThreadID;
            result.push_back(te32.th32ThreadID);
        }
    } while( Thread32Next(hThreadSnap, &te32 ) );

    return result;
}
