#include "context.h"

#include <QDebug>
#include <QJsonObject>
#include <QJsonDocument>
#include <algorithm>

const QString Context::name_file_CAIC = "info.caic";

Context::Context()
{

}

Context::Context(QuaZip &file_zip)
{
    this->load_from_file(file_zip);
}

Context::Context(uint pid)
{
    Scanner::Instance().scan_interface(m_ui_tree, pid);
}

Context::Context(const Context &config) :
    m_ui_tree(config.m_ui_tree)
{

}

Context::~Context()
{

}

bool Context::save_to_file(QuaZip& file_zip)
{
    if (save_meta_info(file_zip) &&
            save_journal(dynamic_cast<ISerializable*>(&m_ui_tree), name_file_scan, file_zip) /*&&
            save_journal(dynamic_cast<ISerializable*>(&m_cad_transactions), name_file_transaction, file_zip)*/ ) {
        return true;
    }

    return false;
}

bool Context::load_from_file(QuaZip &file_zip)
{
    if (load_meta_info(file_zip) &&
            load_journal(dynamic_cast<ISerializable*>(&m_ui_tree), name_file_scan, file_zip) /*&&
            load_journal(dynamic_cast<ISerializable*>(&m_cad_transactions), name_file_transaction, file_zip)*/ ) {
        return true;
    }

    this->clear();
    return false;
}

bool Context::is_empty()
{
    return m_cad_name.isEmpty();
}

void Context::clear()
{
    qDebug() << "clear started";
    m_ui_tree.clear();
    m_cad_transactions.clear();
    m_cad_name.clear();
}

JournalInfo &Context::ui_tree()
{
    return m_ui_tree;
}

JournalTransaction &Context::cad_transactions()
{
    return m_cad_transactions;
}

Context &Context::operator=(const Context &_lhs)
{
    m_ui_tree = _lhs.m_ui_tree;
    //TODO m_cad_transactions = _lhs.cad_transactions;
    return *this;
}

bool Context::load_journal(ISerializable *loaded_obj, QString name, QuaZip &file_zip)
{
    qDebug() << "load_journal started. Name: " << name;
    for(bool f=file_zip.goToFirstFile(); f; f=file_zip.goToNextFile()) {
        //найти в нём все сесси
        if (file_zip.getCurrentFileName() == name) {
            QuaZipFile file_tmp(&file_zip);
            if (!file_tmp.open(QIODevice::ReadOnly))
                return false;

            QByteArray buf_data = file_tmp.readAll();
            QJsonDocument load_info(QJsonDocument::fromJson(buf_data));
            loaded_obj->deserialize(load_info.object());
            file_tmp.close();
            return true;
        }
    }
    return false;
}

bool Context::save_journal(ISerializable *saving_obj, QString name, QuaZip &file_zip)
{
    qDebug() << "save_journal started. Name: " << name;
    QuaZipFile file_target(&file_zip);
    if (!file_target.open(QIODevice::WriteOnly, QuaZipNewInfo(name)))
        return false;

    QJsonObject json_to_save_data;
    saving_obj->serialize(json_to_save_data);
    QJsonDocument doc_session(json_to_save_data);
    file_target.write(doc_session.toJson());
    file_target.close();

    return true;
}

bool Context::save_meta_info(QuaZip &file_zip)
{
    QuaZipFile file_target(&file_zip);
    if (!file_target.open(QIODevice::WriteOnly, QuaZipNewInfo("meta.json")))
        return false;

    QJsonObject json_to_save_data;
    json_to_save_data["cad_name"] = m_cad_name;
    QJsonDocument doc_session(json_to_save_data);
    file_target.write(doc_session.toJson());
    file_target.close();

    return true;
}

bool Context::load_meta_info(QuaZip &file_zip)
{
    qDebug() << "load_meta_info started";
    for(bool f=file_zip.goToFirstFile(); f; f=file_zip.goToNextFile()) {
        //найти в нём все сесси
        if (file_zip.getCurrentFileName() == "meta.json") {
            QuaZipFile file_tmp(&file_zip);
            if (!file_tmp.open(QIODevice::ReadOnly))
                return false;

            QByteArray buf_data = file_tmp.readAll();
            QJsonDocument load_info(QJsonDocument::fromJson(buf_data));
            m_cad_name = load_info.object()["cad_name"].toString();

            file_tmp.close();
            return true;
        }
    }
    return false;
}
