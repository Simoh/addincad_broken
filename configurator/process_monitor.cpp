#include "process_monitor.h"
#include <QDebug>
#include <Psapi.h>
#include <iterator>

Process_monitor &Process_monitor::Instance()
{
    static Process_monitor _instance;
    return _instance;
}

QStringList Process_monitor::get_open_processes_names()
{
    QStringList result_processes;

    pid_map.clear();
    EnumWindows(&EnumWindowsGetCheckBox, NULL);

    for (auto& itList : pid_map)
        result_processes.push_back(itList.first);

    return result_processes;
}

unsigned long Process_monitor::get_pid_by_string(QString _str)
{
    if (this->pid_map.empty())
        return 0;

    return std::get<0>(pid_map[_str]);
}

HWND Process_monitor::get_hwnd_by_string(QString _str)
{
    if (this->pid_map.empty())
        return NULL;

    return std::get<1>(pid_map[_str]);
}

BOOL CALLBACK Process_monitor::EnumWindowsGetCheckBox(HWND hwnd, LPARAM lParam)
{
    if (!IsWindowVisible(hwnd) || GetWindow(hwnd, GW_OWNER) != NULL)
        return TRUE;

    TCHAR szTextWin[255];

    if (GetWindowText(hwnd, szTextWin, sizeof(szTextWin)))
    {
        unsigned long pid = 0;
        GetWindowThreadProcessId(hwnd, &pid);

        for (auto const &it : Process_monitor::Instance().pid_map)
            if (std::get<0>(it.second) == pid) //If pid already exist - skip this window
                return TRUE;

        QString buf_string = QString::fromWCharArray(szTextWin);

        if (buf_string.compare("addincad", Qt::CaseInsensitive) != 0) {
            Process_monitor::Instance().pid_map.insert(std::pair<QString, std::pair<unsigned long, HWND>>(buf_string, std::make_pair(pid, hwnd)));
        }
    }

    return TRUE;
}


Process_monitor::Process_monitor(QObject *parent) : QObject(parent)
{
}

Process_monitor::~Process_monitor()
{

}
