#ifndef SCANNER_H
#define SCANNER_H

#include <iostream>
#include <utility>
#include <Windows.h>
#include <oleacc.h>

#include <QString>
#include <QObject>
#include <QRect>
#include <QPoint>

#include "journals/journal_info.h"
#include "configurator/runtime_object.h"

using namespace std;

class Scanner : public QObject
{
    Q_OBJECT
public:
    static Scanner& Instance();

    bool scan_interface(JournalInfo& _info, unsigned long _pid_target);
    ElementInfo get_element_under_cursor();
    ElementInfo get_ui_info(HWND hwnd, ulong idObject, ulong idChild);
    std::pair<IAccessible*, ulong> get_accessible(ElementInfoPtr);

    bool process_is_valid(unsigned long _process_id);
    bool element_is_valid(vector<ElementInfo> _info, unsigned long process_id);
    bool element_is_valid(vector<ElementInfo> _info, unsigned long process_id, RuntimeObject& hint_inst);
    bool find_coordinate(IAccessible *pAcc, RuntimeObject& coordinate, RuntimeObject& scale);
    QPoint get_coordinate_energycs(RuntimeObject& coordinate);
    int get_scale_energycs(RuntimeObject& scale);

    RuntimeObject last_accessible;

    HWND get_element_hwnd(vector<ElementInfo> info_hierarchy, uint pid);
protected:
    Scanner();
    ~Scanner();
private:
    HRESULT walk_tree_Accessible(IAccessible *_input_acc, ElementInfo& _parent);
    bool find_element_info(IAccessible *_input_acc, vector<ElementInfo>& _info);

    static BOOL CALLBACK EnumWndProc(HWND _target_wnd, LPARAM lParam);
    static BOOL CALLBACK EnumWndProcToValidElement(HWND _target_wnd, LPARAM lParam);

    std::vector<unsigned long> get_thread_ids(unsigned long process_id);

    QString get_name(IAccessible *pAcc, long _child_id);
    QString get_role(IAccessible* pAcc, long _child_id);

    QRect frame; //говностиль. что-нибудь придумать

    /* Поиск координат в клиентском окне EnergyCS*/
    bool find_coordinate(IAccessible *pAcc, RuntimeObject& coordinate);
    bool find_scale(IAccessible *pAcc, RuntimeObject& scale);

};

#endif // SCANNER_H
