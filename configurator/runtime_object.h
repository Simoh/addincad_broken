#ifndef RUNTIME_OBJECT_H
#define RUNTIME_OBJECT_H

#include <Windows.h>
#include <oleacc.h>
#include <QRect>

class RuntimeObject
{
public:
    RuntimeObject(IAccessible *pacc = NULL, long child_id = CHILDID_SELF);
    RuntimeObject(HWND hwnd);
    ~RuntimeObject();
    void clear();
    bool is_empty();

    QRect get_rect();
    IAccessible* pacc() const;
    HWND hwnd() const;
    long child_id() const;

    RuntimeObject &operator=(const RuntimeObject& _lhs);
private:
    HWND m_hwnd;
    IAccessible *m_pacc;
    long m_child_id;
};

#endif // RUNTIME_OBJECT_H
