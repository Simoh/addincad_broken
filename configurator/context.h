#ifndef CONTEXT_H
#define CONTEXT_H

#include <QObject>
#include <JlCompress.h>

#include "transaction_recorder.h"
#include "journals/journal_info.h"
#include "journals/journal_transaction.h"
#include "scanner.h"

class Context : public QObject
{
    Q_OBJECT
public:
    explicit Context();
    Context(QuaZip& file_zip);
    Context(uint pid);
    Context(const Context& config);
    ~Context();

    bool save_to_file(QuaZip& file_zip);
    bool load_from_file(QuaZip& file_zip);

    bool is_empty();
    void clear();

    JournalInfo& ui_tree();
    JournalTransaction& cad_transactions();

    static const QString name_file_CAIC;

    Context &operator=(const Context& _lhs);
private:
    Transaction_recorder transaction_recorder;

    JournalInfo m_ui_tree;
    JournalTransaction m_cad_transactions;
    QString m_cad_name;

    const QString name_file_scan = "info.json";
    const QString name_file_transaction = "transactions.json";

    bool load_journal(ISerializable* loaded_obj, QString name, QuaZip& file_zip);
    bool save_journal(ISerializable* saving_obj, QString name, QuaZip& file_zip);
    bool save_meta_info(QuaZip& file_zip);
    bool load_meta_info(QuaZip& file_zip);

signals:

public slots:
};

#endif // CONTEXT_H
