#include "update_watchdog.h"
#include "ui_update_watchdog.h"

#include "errors.h"
#include "global_constants.h"

#include <QTimer>
#include <QDebug>

const int UPDATE_DELAY_MSEC = 20;

Update_watchdog::Update_watchdog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Update_watchdog)
{
    ui->setupUi(this);

    this->HookLib = nullptr;
    this->f_set_update_hook = nullptr;
    this->f_unset_hook = nullptr;
    this->active = false;
}

Update_watchdog::~Update_watchdog()
{
    delete ui;
}

void Update_watchdog::load_functions()
{
    f_set_update_hook = (function_dll)GetProcAddress(HookLib, "SetUpdateHook");
    if (!f_set_update_hook)
    {
        qCritical() << "Cannot load SetUpdateHook function";
        throw Errors::Hook::cannot_load_data;
    }

    /* Load unset hook */
    (FARPROC &)f_unset_hook = GetProcAddress(HookLib, "UnSetHook");
    if (!f_unset_hook)
    {
        qCritical() << "Cannot load UnSetHook function";
        throw Errors::Hook::cannot_load_data;
    }
}

void Update_watchdog::start(unsigned long process_id)
{
    this->HookLib = LoadLibrary(L"hook32.dll");

    if (!this->HookLib)
        throw Errors::Hook::cannot_load_dll;

    load_functions();

    if (!f_set_update_hook(process_id, (HWND)this->winId()))
        throw Errors::Hook::cannot_connect_to_app;

    this->active = true;
}

void Update_watchdog::stop()
{
    if (this->HookLib)
    {
        this->f_unset_hook();
        FreeLibrary(this->HookLib);
    }
    active = false;
    this->HookLib = nullptr;
    this->f_set_update_hook = nullptr;
    this->f_unset_hook = nullptr;
}

void Update_watchdog::pause()
{
    this->active = false;
}

void Update_watchdog::unpause()
{
    this->active = true;
}

bool Update_watchdog::is_active()
{
    return this->active;
}

bool Update_watchdog::nativeEvent(const QByteArray &_event_type, void *_message, long *_result)
{
    Q_UNUSED(_event_type)
    Q_UNUSED(_result)

    if (!this->active)
        return false;

    MSG* msg = reinterpret_cast<MSG*>(_message);

    switch (msg->message)
    {
    case WM_UPDATE_POSITION:
    case WM_UPDATE_RESEARCH:
    {
        qDebug() << "Recieve update reseach event from dll. Send signal update_reseach";
        emit update_msg(msg->message);
        //interpolation for update message
        QTimer::singleShot(UPDATE_DELAY_MSEC, this, [=](){emit update_msg(msg->message);});
        break;
    }
    default:
        break;
    }
    return false;
}
