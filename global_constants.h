#ifndef GLOBAL_CONSTANTS
#define GLOBAL_CONSTANTS
#include <Windows.h>
#include <iostream>

namespace Global_constant
{
    //TODO rename preffix WM_ to ACTION_
    #define WM_UPDATE_POSITION          (WM_USER+0x10)
    #define WM_UPDATE_RESEARCH          (WM_USER+0x11)
    #define WM_CLIENT_EVENT             (WM_USER+0x20)
    #define WM_CLIENT_CALL_MENU         (WM_USER+0x21)
    #define WM_INTEGRATION_EVENT        (WM_USER+0x29)


    static std::string mailslot_service_target_app_template("\\\\.\\mailslot\\$ServiceMailslotTargetApp$");
    static std::string mailslot_service_this_template("\\\\.\\mailslot\\$ServiceMailslotAddInCAD$");
    static std::string mailslot_data_target_app_template("\\\\.\\mailslot\\$DataMailslotTargetApp$");
    static std::string mailslot_data_this_template("\\\\.\\mailslot\\$DataMailslotAddInCAD$");

    const std::string nameMailslot = "\\\\.\\mailslot\\$AddInCAD1$";

    namespace connection_type
    {
        const std::string auto_select("auto");
        const std::string hook("hook");
        const std::string integration("integration");
    }

    namespace state
    {
        enum States
        {
           Stopped, Paused, Started
        };
    }

    namespace stop_reasons
    {
        const std::string default_reason("user canceled operation");
        const std::string connection_refused("Соединение разорвано");
        const std::string initiate_target_app("Целевое приложение сбросило соединение");
        const std::string incorrect_input_data("Ошибка в исходных данных");
        const std::string playing_was_finished("Воспроизведение журнала завершено");
        const std::string trial_reason("Заершено. Используется trial версия");
    }
}

#endif // GLOBAL_CONSTANTS

