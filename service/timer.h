#ifndef TIMER_H
#define TIMER_H

#include <chrono>
#include <thread>
#include <atomic>
#include <QObject>

class Timer: public QObject
{
    Q_OBJECT
public:
    Timer();
    ~Timer();
    void start(unsigned int milliseconds);
    void stop();
private:
    std::thread t_worker;
    std::chrono::milliseconds delay;
    std::atomic<bool> is_running;

signals:
    void timeout();
};

#endif // TIMER_H
