#include "updatechecker.h"

#include <QProcess>
#include <QDebug>

UpdateChecker::UpdateChecker(QObject *parent) : QObject(parent)
{

}

void UpdateChecker::check_update()
{
    QProcess process;
    process.start("maintenancetool --checkupdates");
    // Wait until the update tool is finished
    process.waitForFinished();

    if(process.error() != QProcess::UnknownError)
    {
        qDebug() << "Error checking for updates";
        return ;
    }

    QByteArray data = process.readAllStandardOutput();

    // No output means no updates available

    if(!data.isEmpty()) {
        if (emit install_update("New update is available")) {
            qDebug () << "install update :" << data;
            QProcess::startDetached("maintenancetool", QStringList(QLatin1String("--updater")));
        }
    }
}
