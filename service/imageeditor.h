#ifndef IMAGEEDITOR_H
#define IMAGEEDITOR_H

#include <QDialog>

namespace Ui {
class ImageEditor;
}

class ImageEditor : public QDialog
{
    Q_OBJECT

public:
    explicit ImageEditor(QWidget *parent = 0);
    ~ImageEditor();

    QImage edit_image(const QImage& img);

private:
    Ui::ImageEditor *ui;
};

#endif // IMAGEEDITOR_H
