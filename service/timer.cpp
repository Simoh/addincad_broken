#include "timer.h"

Timer::Timer():
    is_running(false)
{

}

Timer::~Timer()
{

}

void Timer::start(unsigned int milliseconds)
{
    delay = std::chrono::milliseconds(milliseconds);
    is_running.store(true);
    t_worker = std::thread([=]()
    {
        while(is_running.load())
        {
            std::this_thread::sleep_for(delay);
            emit timeout();
        }
    });
}

void Timer::stop()
{
    is_running.store(false);
    if (t_worker.joinable())
        t_worker.join();
}

