#include "globalsettings.h"

#include <QStandardPaths>
#include <QDir>

QString GlobalSettings::current_process_name()
{
    return d_current_process_name;
}

void GlobalSettings::set_current_process_name(QString new_name)
{
    d_current_process_name = new_name.toLower();
}

int GlobalSettings::current_process_id()
{
    return d_current_process_id;
}

void GlobalSettings::set_current_process_id(int new_id)
{
    d_current_process_id = new_id;
}

QString GlobalSettings::last_error_info() const
{
    return m_last_error_info;
}

void GlobalSettings::set_last_error_info(QString info)
{
    this->m_last_error_info = info;
}

void GlobalSettings::set_main_wnd(HWND hwnd)
{
    this->d_main_wnd = hwnd;
}

HWND GlobalSettings::get_main_wnd()
{
    return this->d_main_wnd;
}

QString GlobalSettings::working_path() const
{
    return m_working_path;
}

QString GlobalSettings::system_path() const
{
    return m_system_path;
}

GlobalSettings::GlobalSettings()
{
    m_working_path = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).front() + "\\AddInCAD";
    m_system_path = m_working_path + "\\system";
    QDir().mkdir(m_working_path);
    QDir().mkdir(m_system_path);
}

GlobalSettings::~GlobalSettings()
{

}
