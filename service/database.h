#ifndef DATABASE_H
#define DATABASE_H

#include <JlCompress.h>

#include "journals/journal_session.h"
#include "configurator/context.h"

using namespace std;

class Database: public QObject
{
    Q_OBJECT
public:
    Database();
    ~Database();

    void load_from_file(QString _db_path);
    void load_from_file();
    void close_db();
    void clear();
    bool is_empty();

    JournalSession get_journal();
    std::shared_ptr<Context> get_config();
    void save_journal(JournalSession& session);

private:
    QString default_catalog;
    QFileInfo m_aic_file_info;
    QuaZip m_file_zip;

    QMap<QString, QString> m_session_names; //QMap<"filename","session_name">
    QStringList m_sessions_list;
    std::shared_ptr<Context> m_config;

    bool check_db();

    QString generate_session_name();
    QString get_session_name();
    bool read_session_names();

    void copy_sessions(QuaZip& source, QuaZip& destination, QString exception = "");
    bool save_journal_session(JournalSession& _journal, QuaZip& filezip);
    bool save_context_file_to_aic(std::shared_ptr<Context> context, QuaZip& file_zip);
    JournalSession load_journal_from_file(const QString& name);
    std::shared_ptr<Context> load_context_from_caic(QuaZip& file_zip);
    QByteArray get_rawdata_from_file(QuaZip& filezip);

};

#endif // DATABASE_H
