#include "database.h"

#include <QDebug>
#include <QJsonDocument>
#include <QApplication>
#include <QFileDialog>
#include <QMessageBox>
#include <QDir>

#include "errors.h"
#include "widgets/dialogchoosejournal.h"
#include "widgets/dialogsave.h"

#include "service/globalsettings.h"

Database::Database() :
    default_catalog(GlobalSettings::Instance().working_path() + "\\user_sessions\\")
{
    QDir().mkdir(default_catalog);
}

Database::~Database()
{
    this->close_db();
}

bool Database::is_empty()
{
    return m_file_zip.getZipName().isEmpty();
}

/**
 * @brief Function check file to correct and set fileZip name to filename
 * @param _db_path relevant path to file
 * @return Result of
 * @throw std::logic_error if some problems with database file
 */
void Database::load_from_file(QString _db_path)
{
    qDebug() << "load_db started (" << _db_path << ")";

    if (!this->is_empty())
        this->close_db();

    m_aic_file_info.setFile(_db_path);
    if (!m_aic_file_info.exists())
    {
        qDebug() << "File " << _db_path << " not found";
        throw Errors::Database::filename_is_bad;
    }

    m_file_zip.setZipName(_db_path);
    if (m_aic_file_info.suffix() == "aic") {
        //TODO check sessions and config file
        qDebug() << "Chose file aic";
        if (!read_session_names())
            throw Errors::Database::file_hasnt_sessions;
    } else if (m_aic_file_info.suffix() == "caic") {
        qDebug() << "Chose file caic";
    } else
        throw Errors::Database::bad_file_format;
}

/**
 * @brief Function open dialog for choose file *.aic
 * @throw Errors::cannot_load_journal if some problem with database
 * @throw Errors::user_canceled_action if use cancel action
 */
void Database::load_from_file()
{
    //choose file
    QString filename = QFileDialog::getOpenFileName(QApplication::activeWindow(),
                                                    tr("Выберите файл AddInCAD"),
                                                    "", tr("AddInCAD files (*.aic)"));

    if (filename.isEmpty())
        throw Errors::user_canceled_action;
    else
        this->load_from_file(filename);
}

/**
 * @brief Function cleared data about file AIC
 */
void Database::close_db()
{
    qDebug() << "close_db started";
    this->clear();
}

void Database::clear()
{
    if (m_file_zip.isOpen())
        m_file_zip.close();
    m_file_zip.setZipName("");
    m_aic_file_info.setFile("");
    m_sessions_list.clear();
    m_session_names.clear();
}

/**
 * @brief This function check data base for correctness. Check configuration file and sessions files.
 * @return true or false
 */
bool Database::check_db()
{
    qDebug() << "Check internal file structure";

    if (this->is_empty())
    {
        qDebug() << "Не указан файл";
        return false;
    }

    struct flags_s
    {
        bool is_session : 1;
        bool is_info : 1;
    }file_flags;

    if (!m_file_zip.open(QuaZip::mdUnzip))
    {
        qDebug() << "Cannot open DB";
        return false;
    }

    for(bool f=m_file_zip.goToFirstFile(); f; f=m_file_zip.goToNextFile())
    {
        if (m_file_zip.getCurrentFileName() == Context::name_file_CAIC)
        {
            qDebug() << "Configuration file is absent";
            //проверить действительно ли файл caic
            file_flags.is_info = true;
        }
        else //проверить есть ли другие json
            file_flags.is_session = true;
    }
    m_file_zip.close();

    return file_flags.is_session & file_flags.is_info;
}


QByteArray Database::get_rawdata_from_file(QuaZip& filezip)
{
    qDebug() << "get_rawdata_from_file started";
    QuaZipFile file_tmp(&filezip);
    file_tmp.open(QIODevice::ReadOnly);
    QByteArray buf_data = file_tmp.readAll();
    file_tmp.close();

    return buf_data;
}

JournalSession Database::load_journal_from_file(const QString& name)
{
    qDebug() << "load_journal started : " << name;

    for(bool f=m_file_zip.goToFirstFile(); f; f=m_file_zip.goToNextFile())
    {
        //force by all sessions in file and try find target
        if (m_file_zip.getCurrentFileName().indexOf("session") == -1)
            continue;

        QuaZipFile file_tmp(&m_file_zip);
        if (file_tmp.open(QIODevice::ReadOnly)) {
            QJsonDocument load_info(QJsonDocument::fromJson( file_tmp.readAll() ));
            if (load_info.object().contains("name") &&
                    load_info.object()["name"].toString() == name) {
                    return JournalSession(load_info.object(), this->load_context_from_caic(m_file_zip));
            }
        }
        file_tmp.close();
    }
    qDebug() << "Searching session isn`t founded";
    return JournalSession();
}

std::shared_ptr<Context> Database::load_context_from_caic(QuaZip &file_zip)
{
    std::shared_ptr<Context> result_cfg = nullptr;
    //load to tmp file
    QString tmp_caic_file_name = file_zip.getZipName() + ".tmp";
    if (!JlCompress::extractFile(&file_zip, Context::name_file_CAIC, tmp_caic_file_name)) {
        return result_cfg;
    }
    QuaZip tmp_caic(tmp_caic_file_name);
    if (tmp_caic.open(QuaZip::mdUnzip))
        result_cfg = std::make_shared<Context>(tmp_caic);
    tmp_caic.close();
    if (!QFile(tmp_caic_file_name).remove())
        qCritical() << "Cannot remove file tmp";

    return result_cfg;
}

QString Database::get_session_name()
{
    qDebug() << "get_session_name started";

    if (m_sessions_list.isEmpty())
        return "";
    if (m_sessions_list.count() == 1)
        return m_sessions_list.back();

    //show list of sessions for user when sessions more than 1
    qDebug() << "open dlg for choose session";
    DialogChooseJournal dlg_choose;
    return dlg_choose.choose_name(m_sessions_list);
}

bool Database::read_session_names()
{
    if (!m_file_zip.open(QuaZip::mdUnzip))
        return false;

    for(bool f=m_file_zip.goToFirstFile(); f; f=m_file_zip.goToNextFile()) {
        //get name only sessions
        if (m_file_zip.getCurrentFileName().indexOf("session-") != -1) {
            QByteArray buf_data = get_rawdata_from_file(m_file_zip);
            QJsonDocument load_info(QJsonDocument::fromJson(buf_data));
            if (load_info.object().contains("name")) {
                //add to map and string list
                m_sessions_list.push_back(load_info.object()["name"].toString());
                m_session_names.insert(m_file_zip.getCurrentFileName(), m_sessions_list.back());
            }
        }
    }

    m_file_zip.close();
    return !m_session_names.isEmpty();
}

JournalSession Database::get_journal()
{
    JournalSession result;
    try {
        if (!m_file_zip.open(QuaZip::mdUnzip))
            throw Errors::Database::file_zip_is_empty;

        //check jorunals in file
        QString name = get_session_name();
        if (name.isEmpty())
            throw Errors::Database::file_hasnt_sessions;

        //load journal
        result = load_journal_from_file( name );
    }
    catch(const std::logic_error &ex) {
        qWarning() << ex.what();
        this->clear();
        QMessageBox::warning(QApplication::activeWindow(), tr("Ошибка"), ex.what());
        throw Errors::cannot_load_journal;
    }

    m_file_zip.close();
    return result;
}

void Database::save_journal(JournalSession &session)
{
    //show window to redact/save/discard
    qDebug() << "save journal";
    DialogSave saveDlg(QApplication::activeWindow());
    QString result_name = m_aic_file_info.baseName();
    if (saveDlg.run(result_name, session, m_sessions_list) == true) {
        //create tmp file
        qDebug() << "create tmp file ~" << result_name;
        QuaZip tmp_file(default_catalog + result_name + ".aic.tmp");
        if (tmp_file.open(QuaZip::mdAdd)) {
            //make file as hidden
            SetFileAttributes(tmp_file.getZipName().toStdWString().c_str(), FILE_ATTRIBUTE_HIDDEN); //FILE_ATTRIBUTE_TEMPORARY

            //WARNIGN TODO need process bad situation
            //save config file
            save_context_file_to_aic(session.configurator(), tmp_file) ;
            //add other sessions from old file
            copy_sessions(m_file_zip, tmp_file, session.m_name);
            //save journal
            save_journal_session(session, tmp_file);

            //remove old file and rename tmp
            tmp_file.close();
            if (!QFile(m_aic_file_info.absoluteFilePath()).remove())
                qCritical() << "Cannot remove file : " << m_aic_file_info.absoluteFilePath();
            m_aic_file_info.setFile(default_catalog + result_name + ".aic");
            QFile(tmp_file.getZipName()).rename(m_aic_file_info.absoluteFilePath());
        }
    }
}

void Database::copy_sessions(QuaZip &source, QuaZip &destination, QString exception)
{
    qDebug() << "copy_sessions except : " << exception;
    if (!source.open(QuaZip::mdUnzip))
        return;
    //copy all sessions except file from :exception:
    for(bool f=source.goToFirstFile(); f; f=source.goToNextFile())
    {
        if ( (source.getCurrentFileName().indexOf("session") != -1) &&
             (m_session_names[source.getCurrentFileName()] != exception) ) {
            //save to dest file
            QByteArray buf_data = get_rawdata_from_file(source);
            QuaZipFile file_buf(&destination);
            if (file_buf.open(QIODevice::WriteOnly, QuaZipNewInfo(source.getCurrentFileName()))) {
                file_buf.write(buf_data);
                file_buf.close();
            } else {
                qCritical() << "Cannot copy session: " << source.getCurrentFileName();
            }
        }
    }
    source.close();
}

bool Database::save_journal_session(JournalSession &_journal, QuaZip& filezip)
{
    bool result = false;
    QString name = generate_session_name();
    qDebug() << "save_journal_session started. Name session: " << _journal.m_name << "generate name : " << name;

    QuaZipFile file_target(&filezip);
    if (file_target.open(QIODevice::WriteOnly, QuaZipNewInfo(name))) {
        QJsonObject json_to_save_data;
        _journal.serializing(json_to_save_data);
        QJsonDocument doc_session(json_to_save_data);
        file_target.write(doc_session.toJson());
        result = true;
    }
    file_target.close();

    return result;
}

bool Database::save_context_file_to_aic(std::shared_ptr<Context> context, QuaZip& file_zip)
{
    bool result = false;
    qDebug() << "save_context_file_to_aic started";

    //create tmp file for caic
    QuaZip tmp_file(file_zip.getZipName() + "_context.tmp");
    if (tmp_file.open(QuaZip::mdCreate)) {
        //make file as hidden
        SetFileAttributes(tmp_file.getZipName().toStdWString().c_str(), FILE_ATTRIBUTE_TEMPORARY); //FILE_ATTRIBUTE_TEMPORARY

        result = context.get()->save_to_file(tmp_file);
    } else
        qWarning() << "Cannot open tmp file for saiving context";

    tmp_file.close();

    if (result)
        JlCompress::compressFile(&file_zip, tmp_file.getZipName(), "info.caic");
    QFile(tmp_file.getZipName()).remove();

    return result;
}

QString Database::generate_session_name()
{
    qDebug() << "get_free_number started";
    QString result = "session-1.json";
    QVector<uint> numbers_busy = { 0 };

    for (auto& itSession: m_session_names.keys()) {
        QString buf_str = itSession.remove(0, itSession.indexOf("-") + 1); //name "session-<number>.json"
        buf_str.remove(buf_str.lastIndexOf(".json"), 5);
        bool ok = false;
        numbers_busy.push_back(buf_str.toUInt(&ok));
        if (!ok) {
            qWarning() << "Error parsing session name :" << itSession;
            //TODO error?
        }
    }
    qSort(numbers_busy.begin(), numbers_busy.end());

    if (numbers_busy.isEmpty()) {
        return result;
    }

    for (int i = 0; i < numbers_busy.size() - 1; i++) {
        if ((numbers_busy[i + 1] - numbers_busy[i]) != 1)
            return QString("session-" + QString::number(numbers_busy[i] + 1) + ".json");
    }

    return QString("session-" + QString::number(numbers_busy.back() + 1) + ".json");
}

/**
 * @brief This function load confugurates files from file *.aic and save in internal structure
 * @param _cad_info Link to cad_info which will be containt info from file
 * @throw file_zip_is_empty if file hasn`t config file
 */
std::shared_ptr<Context> Database::get_config()
{
    qDebug() << "get_config started";
    std::shared_ptr<Context> result_cfg;

    if (!m_file_zip.open(QuaZip::mdUnzip))
        return result_cfg;

    if (m_aic_file_info.suffix() == "aic") {
        result_cfg = load_context_from_caic(m_file_zip);
    } else {
        result_cfg = std::make_shared<Context>(m_file_zip);
    }

    m_file_zip.close();

    return result_cfg;
}
