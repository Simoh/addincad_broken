#ifndef LOGGER_H
#define LOGGER_H

#include <QFile>
#include <QtGlobal>

class Logger
{
public:
    static Logger& Instance()
    {
        static Logger _instance;
        return _instance;
    }

    void initialize(bool debug_mode = false);
    void debug_message(QString message);

    QString get_debug_filename();

private:
    Logger();
    ~Logger();
    Logger(Logger const&) = delete;
    Logger& operator= (Logger const&) = delete;

    QFile m_log_file;
    bool m_is_debug_mode;
    bool create_debug_file();
};

#endif // LOGGER_H
