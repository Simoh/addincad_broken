#ifndef UPDATECHECKER_H
#define UPDATECHECKER_H

#include <QObject>

class UpdateChecker : public QObject
{
    Q_OBJECT
public:
    explicit UpdateChecker(QObject *parent = nullptr);

signals:
    bool install_update(QString description);

public slots:
    void check_update();
};

#endif // UPDATECHECKER_H
