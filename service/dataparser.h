#ifndef DATAPARSER_H
#define DATAPARSER_H

#include <QObject>
#include <QRect>

#include <Windows.h>

#include "base_elements/actionhook.h"
#include "base_elements/actionintegration.h"
#include "configurator/context.h"

class DataParser : public QObject
{
    Q_OBJECT
public:
    explicit DataParser(QObject *parent = 0);
    ~DataParser();
    int detect_data_type(QString _input_str);
    QPair<QRect,HWND> parse_update_event(QString _input_str);
    ActionIntegration parse_integration_event(QString _input_str);
    ActionHook parse_hook_event(QString _input_str, std::shared_ptr<Context> context);
};

#endif // DATAPARSER_H
