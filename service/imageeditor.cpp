#include "imageeditor.h"
#include "ui_imageeditor.h"

ImageEditor::ImageEditor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ImageEditor)
{
    ui->setupUi(this);
}

ImageEditor::~ImageEditor()
{
    delete ui;
}

QImage ImageEditor::edit_image(const QImage &img)
{
    ui->img_label->setPixmap(QPixmap::fromImage(img));
    if (this->exec() == QDialog::Accepted) {
        return ui->img_label->pixmap()->toImage();
    }
}
