#include "logger.h"

#include "service/globalsettings.h"

#include <Windows.h>
#include <iostream>

#include <QStandardPaths>
#include <QTextStream>
#include <QDateTime>
#include <QFileInfo>


void debugMsgHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

void Logger::initialize(bool debug_mode)
{
    setlocale(LC_ALL, "ru");

    if (create_debug_file())
       qInstallMessageHandler(debugMsgHandler);

    if (debug_mode) {
        m_is_debug_mode = true;
        //initialize console
        FreeConsole();
        AllocConsole();
        (void)freopen("CONOUT$", "w", stdout);
        SetConsoleMode(GetStdHandle(STD_OUTPUT_HANDLE),ENABLE_QUICK_EDIT_MODE| ENABLE_EXTENDED_FLAGS);
    }
}

void Logger::debug_message(QString message)
{
    if (m_is_debug_mode)
        std::wcout << message.toStdWString() << std::endl;
}

QString Logger::get_debug_filename()
{
    return m_log_file.fileName();
}

Logger::Logger()
{
    m_is_debug_mode = false;
}

Logger::~Logger()
{
    if (m_is_debug_mode)
        FreeConsole();
}

bool Logger::create_debug_file()
{
    m_log_file.setFileName(GlobalSettings::Instance().system_path() + "\\addincad.log");
    if(m_log_file.open(QIODevice::Append | QIODevice::Text)) {
        QTextStream writeStream(&m_log_file);
        writeStream << "\n\n*******************NEW SESSION WITH ADDINCAD******************* \n\n";
        m_log_file.close();
    } else
        return false;

    return true;
}


void debugMsgHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg) {
    QFile log_file(Logger::Instance().get_debug_filename());
    if (log_file.open(QIODevice::Append | QIODevice::Text)) {

        QTextStream writeStream(&log_file);
        writeStream << QDateTime::currentDateTime().toString("hh:mm:ss") << " [" <<
                       QFileInfo(context.file).baseName().toUpper() << "]: ";
        switch (type) {
        case QtDebugMsg:
            writeStream << msg << "\n";
            break;
        case QtWarningMsg:
            writeStream << "(Warning): " << msg << "\n";
            break;
        case QtCriticalMsg:
            writeStream << "(" << QString(context.line) << ") " << "(Critical): " << msg <<
                           ".In function:" << QString(context.function) << "\n";
            break;
        case QtFatalMsg:
            writeStream << "(" << QString(context.line) << ") " << "(Fatal): " << msg <<
                           ".In function:" << QString(context.function) << "\n";
            abort();
        }
        log_file.close();
    }
}
