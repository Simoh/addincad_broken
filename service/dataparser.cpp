#include "dataparser.h"
#include "global_constants.h"
#include "errors.h"
#include "base_elements/element_info.h"

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDebug>
#include <QByteArray>
#include <QPixmap>


DataParser::DataParser(QObject *parent) : QObject(parent)
{

}

DataParser::~DataParser()
{

}

int DataParser::detect_data_type(QString _input_str)
{
    QJsonDocument info(QJsonDocument::fromJson(_input_str.toUtf8()));
    /*get event code*/
    if (info.object()["event code"].isNull())
        return 0;
    return info.object()["event code"].toInt();
}

QPair<QRect, HWND> DataParser::parse_update_event(QString _input_str)
{
    qDebug() << " parse update event";

    QPair<QRect, HWND> result;

    try
    {
        QJsonObject info = (QJsonDocument::fromJson(_input_str.toUtf8())).object();

        if (info["update"].isNull())
            throw Errors::Database::json_data_error;

        QJsonObject input_obj = info["update"].toObject();

        if (!input_obj["x"].isNull())
            result.first.setX(input_obj["x"].toInt());

        if (!input_obj["y"].isNull())
            result.first.setY(input_obj["y"].toInt());

        if (!input_obj["width"].isNull())
            result.first.setWidth(input_obj["width"].toInt());

        if (!input_obj["height"].isNull())
            result.first.setHeight(input_obj["height"].toInt());

        if (!input_obj["handle"].isNull())
            result.second = (HWND)input_obj["handle"].toInt();
    }
    catch (const std::logic_error &ex)
    {
        qWarning() << ex.what();
        return qMakePair(QRect(), (HWND) NULL);
    }

    return result;
}

ActionIntegration DataParser::parse_integration_event(QString _input_str)
{
    qDebug() << "parse_integration_event (QSTRING) started";

    ActionIntegration result;
    QJsonObject input_json = (QJsonDocument::fromJson(_input_str.toUtf8())).object();

    try
    {
        if (!input_json["body"].isArray())
        {
            qDebug() << "INPUT: " << input_json["body"];
            if (input_json["body"].isArray())
            {
                QJsonDocument info_buf;
                info_buf.setArray(input_json["body"].toArray());
                result.set_app_data(QString(info_buf.toJson()));
            }
            else
                result.set_app_data(input_json["body"].toString());

            if (!input_json["coordinates"].isNull())
            {
               result.set_coordinate(QPoint(input_json["coordinates"].toObject()["x"].toInt(),
                                            input_json["coordinates"].toObject()["y"].toInt()));
            }

            if (!input_json["comment"].isNull())
                result.set_comment(input_json["comment"].toString());
        }
        else
            throw Errors::Database::json_data_error;
    }
    catch (const std::exception &ex)
    {
        qWarning() << ex.what();
        result.clear();
        return result;
    }

    return result;
}

ActionHook DataParser::parse_hook_event(QString _input_str, std::shared_ptr<Context> context)
{
    qDebug() << "parse_hook_event (QSTRING) started";

    try
    {        
        QJsonObject input_json = (QJsonDocument::fromJson(_input_str.toUtf8())).object();

        //get main hook info
        if (!input_json["elements"].isArray())
            throw Errors::Database::json_data_error;

        /*parse elements*/
        vector<ElementInfo> buf_ei_vector;
        QJsonArray input_array = input_json["elements"].toArray();

        for(int index_element = 0; index_element < input_array.size(); index_element++) {

            QJsonObject buf_obj = input_array[index_element].toObject();

            if (buf_obj["class name"].isNull() || buf_obj["name"].isNull())
                throw Errors::Database::json_data_error; //if data cannot be read - return empty result

            ElementInfo buf_ei(buf_obj["class name"].toString(), buf_obj["name"].toString());
            buf_ei_vector.push_back(buf_ei);
        }

        /*transform vector_EI to EI*/
        std::reverse(buf_ei_vector.begin(), buf_ei_vector.end());

        //add to ui tree if needed
        int element_id = context.get()->ui_tree().get_id( JournalInfo::vector2EI(buf_ei_vector) );
        if (element_id < 0)
            element_id = context.get()->ui_tree().insert_element( JournalInfo::vector2EI(buf_ei_vector));
        ElementInfoPtr ei_ptr = context.get()->ui_tree().get_EI(element_id);

        int action_id = input_json["event code"].toInt();
        QPoint coordinate = QPoint();
        QImage img = QImage();

        //get coordinate if action is client
        if (action_id == WM_CLIENT_CALL_MENU || action_id == WM_CLIENT_EVENT) {
            if (input_json["coordinate"].isNull())
                throw Errors::Database::json_data_error;

            coordinate = QPoint(input_json["coordinate"].toObject()["x"].toInt(),
                                input_json["coordinate"].toObject()["y"].toInt());
        }

        //load image
        if (input_json.contains("image")) {
            QByteArray encoded = input_json["image"].toString().toLatin1();
            QPixmap tmp_pixmap;
            tmp_pixmap.loadFromData(QByteArray::fromBase64(encoded), "BMP"); //TODO COMPRESS IT
            img = tmp_pixmap.toImage();
        }
        if (input_json.contains("image_path")) {
            img = QImage(input_json["image_path"].toString());
        }

        return ActionHook(action_id, ei_ptr, "", img, coordinate);
    }
    catch (const std::exception &ex)
    {
        qWarning() << ex.what();
        return ActionHook();
    }
}

