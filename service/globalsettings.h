#ifndef GLOBAL_SETTING_H
#define GLOBAL_SETTING_H

#include <QString>
#include <QDebug>
#include <Windows.h>

class GlobalSettings
{
public:
    static GlobalSettings& Instance()
    {
        static GlobalSettings _instance;
        return _instance;
    }

    QString current_process_name();
    void set_current_process_name(QString new_name);
    int current_process_id();
    void set_current_process_id(int new_id);
    QString last_error_info() const;
    void set_last_error_info(QString info);
    void set_main_wnd(HWND hwnd);
    HWND get_main_wnd();

    QString working_path() const;
    QString system_path() const;
private:
    GlobalSettings();
    ~GlobalSettings();
    GlobalSettings(GlobalSettings const&) = delete;
    GlobalSettings& operator= (GlobalSettings const&) = delete;
    QString m_last_error_info;

    QString m_working_path;
    QString m_system_path;

    QString d_current_process_name;
    int d_current_process_id;
    HWND d_main_wnd;
};

#endif // GLOBAL_SETTING_H
