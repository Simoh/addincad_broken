#include "actionintegrationviewer.h"
#include "ui_actionintegrationviewer.h"

ActionIntegrationViewer::ActionIntegrationViewer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ActionIntegrationViewer)
{
    ui->setupUi(this);
}

ActionIntegrationViewer::ActionIntegrationViewer(ActionIntegration *action) :
    ui(new Ui::ActionIntegrationViewer)
{
    ui->setupUi(this);

    ui->label_comment->setText(action->comment());
    m_action = action;
}

ActionIntegrationViewer::~ActionIntegrationViewer()
{
    delete ui;
}

void ActionIntegrationViewer::update_data()
{
    ui->label_comment->setText(m_action->comment() );
}

bool ActionIntegrationViewer::is_picture()
{
    return !m_action->image().isNull();
}
