#include "actionhookviewer.h"
#include "ui_actionhookviewer.h"

ActionHookViewer::ActionHookViewer(ActionHook *action) :
    ui(new Ui::ActionHookViewer)
{
    ui->setupUi(this);

    ui->label_name->setText( action->element_info()->name() );
    ui->label_classname->setText( action->element_info()->class_name() );
    ui->label_comment->setText( action->comment() );
    m_action = action;
}

ActionHookViewer::~ActionHookViewer()
{
    delete ui;
}

void ActionHookViewer::update_data()
{
    ui->label_name->setText( m_action->element_info()->name() );
    ui->label_classname->setText( m_action->element_info()->class_name() );
    ui->label_comment->setText( m_action->comment() );
}

bool ActionHookViewer::is_picture()
{
    return !m_action->image().isNull();
}
