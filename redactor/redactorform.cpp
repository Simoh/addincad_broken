#include "redactorform.h"
#include "ui_redactorform.h"

#include <QDebug>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>

#include "global_constants.h"
#include "service/database.h"
#include "service/imageeditor.h"
#include "widgets/dialogcomment.h"

#include "actionhookviewer.h"
#include "actionintegrationviewer.h"

RedactorForm::RedactorForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RedactorForm)
{
    ui->setupUi(this);

    m_widgets_layout = new QVBoxLayout(ui->scrollAreaWidgetContents);
}

RedactorForm::~RedactorForm()
{
    /* Erase all elements*/
    m_list_of_actions.clear();

    delete m_widgets_layout;
    delete ui;
}

/**
 * @brief RedactorForm::remove_element remove element : call destructor
 * and remove element from viewers list
 * @param id_action - id element in @b listActions
 */
void RedactorForm::remove_element()
{
    for(auto& itAction : m_list_of_actions) {
        if (itAction.get() == qobject_cast<ActionViewer *>(QObject::sender())) {
            disconnect(itAction.get(), &ActionViewer::remove, this, &RedactorForm::remove_element);
            disconnect(itAction.get(), &ActionViewer::edit, this, &RedactorForm::edit_element);
            disconnect(itAction.get(), &ActionViewer::show_picture, this, &RedactorForm::show_picture_of_element);
            auto id_removing = m_list_of_actions.indexOf(itAction);
            m_list_of_actions.removeAt(id_removing);
            m_session_tmp.remove(id_removing, 1);
            for (int i = id_removing; i < m_list_of_actions.size(); i++)
                m_list_of_actions[i].get()->decrease_display_id();
            return;
        }
    }
}

void RedactorForm::edit_element()
{
    //generate dialog
    DialogComment comment_dlg(this);
    for(auto& itAction : m_list_of_actions) {
        if (itAction.get() == qobject_cast<ActionViewer *>(QObject::sender())) {
            auto id_changing = m_list_of_actions.indexOf(itAction);
            //get new comment
            QString new_comment = m_session_tmp.get_action(id_changing)->comment();
            if (comment_dlg.edit_comment(new_comment)) {
                //if comment was changed - update comment in session and in viewer
                m_session_tmp.get_action(id_changing)->set_comment(new_comment);
                m_list_of_actions[id_changing].get()->update_data();
            }
        }//end if
    }//end for
}

void RedactorForm::show_picture_of_element()
{
    for(auto& itAction : m_list_of_actions) {
        if (itAction.get() == qobject_cast<ActionViewer *>(QObject::sender())) {
            auto id_changing = m_list_of_actions.indexOf(itAction);
            //get picture
            QImage picture = m_session_tmp.get_action(id_changing)->image();
            if (!picture.isNull()) {
                ImageEditor editor(this);
                m_session_tmp.get_action(id_changing)->set_image(editor.edit_image(picture));
            }
        }// end if
    } //end for
}

/**
 * @brief RedactorForm::show_session create window with viewers from related journals session and journal info.
 * If changes are accepted - save this to input @b _session
 * @param _info link on @b Journal_info
 * @param _session link on @b Journal_session. Can be changed.
 */
bool RedactorForm::redacting_session(JournalSession &_session)
{
    /* Load data to List */
    m_session_tmp = _session;
    load_data_to_list(m_session_tmp);
    /* Run form */
    if (this->exec() == QDialog::Accepted)
    {
        /* Make changes in input Journal session */
        qDebug() << "Save changes";
        _session = m_session_tmp;
        return true;
    }

    return false;
}

/**
 * @brief RedactorForm::show_session show session from file. Load journals and if this operation is success -
 * call @b show_session from journals. This function called if user try open *.aic file via AddInCAD
 * @param _filename - path to file *.aic
 */
bool RedactorForm::redacting_session(QString _filename)
{
    /* Check _filename */
    Database db;
    try
    {
        db.load_from_file(_filename);
        JournalSession tmp_session(db.get_journal());
        if (redacting_session(tmp_session)) {
            //update session in DB
            db.save_journal(tmp_session);
        }
    }
    catch (const std::logic_error &ex)
    {
        qWarning() << ex.what();
        this->close();
    }
    return true;
}

/**
 * @brief RedactorForm::load_data_to_list this function create new object @Viewer and add to list.
 * Connect this object with remove event.
 * @param _info - link to @ref Journal_info
 * @param _session - link to @ref Journal_session
 */
void RedactorForm::load_data_to_list(JournalSession &_session)
{
    for (int it = 0; it < _session.get_size(); it++)
    {
        if (_session.connection_type() == Global_constant::connection_type::hook.c_str()) {
            m_list_of_actions.push_back(
                        std::make_shared<ActionViewer>(
                            it,
                            new ActionHookViewer( dynamic_cast<ActionHook*>(_session.get_action(it).get()) )
                            )
                        );
        } else if (_session.connection_type() == Global_constant::connection_type::integration.c_str()) {
            m_list_of_actions.push_back(
                        std::make_shared<ActionViewer>(
                            it,
                            new ActionIntegrationViewer( dynamic_cast<ActionIntegration*>(_session.get_action(it).get()) )
                            )
                        );
        } else
            return;

        // add widget to layout
        m_widgets_layout->addWidget(m_list_of_actions.back().get());
        connect(m_list_of_actions.back().get(), &ActionViewer::remove, this, &RedactorForm::remove_element, Qt::QueuedConnection);
        connect(m_list_of_actions.back().get(), &ActionViewer::edit, this, &RedactorForm::edit_element, Qt::QueuedConnection);
        connect(m_list_of_actions.back().get(), &ActionViewer::show_picture, this, &RedactorForm::show_picture_of_element, Qt::QueuedConnection);
    }
}
