#include "actionviewer.h"
#include "ui_actionviewer.h"

#include "iactionviewers.h"

ActionViewer::ActionViewer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ActionViewer)
{
    ui->setupUi(this);
}

ActionViewer::ActionViewer(uint id, QWidget *action) :
    m_action(action),
    ui(new Ui::ActionViewer)
{
    ui->setupUi(this);

    ui->label_id->setText(QString::number(id));
    ui->horizontalLayout->insertWidget(1, m_action);
    ui->picture_btn->setVisible(dynamic_cast<IPicturableViewer*>(m_action)->is_picture());
}

ActionViewer::~ActionViewer()
{
    delete ui;
}

QWidget *ActionViewer::get_action_widget()
{
    return m_action;
}

void ActionViewer::decrease_display_id()
{
    ui->label_id->setText( QString::number( ui->label_id->text().toInt() - 1 ) );
}

void ActionViewer::update_data()
{
    //update text in labels
    dynamic_cast<IUpdatableViewer*>(m_action)->update_data();
    //update visible for image button
    ui->picture_btn->setVisible(dynamic_cast<IPicturableViewer*>(m_action)->is_picture());
}

void ActionViewer::on_edit_btn_clicked()
{
    emit edit();
}

void ActionViewer::on_remove_btn_clicked()
{
    emit remove();
}

void ActionViewer::on_picture_btn_clicked()
{
    emit show_picture();
}
