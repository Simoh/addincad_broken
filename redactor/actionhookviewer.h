#ifndef ACTIONHOOKVIEWER_H
#define ACTIONHOOKVIEWER_H

#include <QWidget>

#include "base_elements/actionhook.h"
#include "iactionviewers.h"

namespace Ui {
class ActionHookViewer;
}

class ActionHookViewer : public QWidget,
                         public IUpdatableViewer,
                         public IPicturableViewer
{
    Q_OBJECT
public:
    ActionHookViewer(ActionHook* action);
    ~ActionHookViewer();
    void update_data() override;
    bool is_picture() override;

private:
    Ui::ActionHookViewer *ui;

    ActionHook* m_action;
};

#endif // ACTIONHOOKVIEWER_H
