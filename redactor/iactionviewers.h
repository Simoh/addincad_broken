#ifndef IUPDATABLEVIEWER_H
#define IUPDATABLEVIEWER_H

class IUpdatableViewer
{
public:
    virtual ~IUpdatableViewer() = default;
    virtual void update_data() = 0;
};

class IPicturableViewer
{
public:
    virtual ~IPicturableViewer() = default;
    virtual bool is_picture() = 0;
};

#endif // IUPDATABLEVIEWER_H
