#ifndef REDACTORFORM_H
#define REDACTORFORM_H

#include <QDialog>
#include <QList>
#include <QVBoxLayout>

#include <memory>

#include "journals/journal_session.h"
#include "actionviewer.h"

namespace Ui {
class RedactorForm;
}

class RedactorForm : public QDialog
{
    Q_OBJECT

public:
    explicit RedactorForm(QWidget *parent = 0);
    ~RedactorForm();
    bool redacting_session(JournalSession &_session);
    bool redacting_session(QString _filename);

private:
    Ui::RedactorForm *ui;
    QVBoxLayout* m_widgets_layout;

    void load_data_to_list(JournalSession &_session);

    QList<std::shared_ptr<ActionViewer>> m_list_of_actions;

    JournalSession m_session_tmp;

public slots:
    void remove_element();
    void edit_element();
    void show_picture_of_element();
};

#endif // REDACTORFORM_H
