#ifndef ACTIONINTEGRATIONVIEWER_H
#define ACTIONINTEGRATIONVIEWER_H

#include <QWidget>

#include "base_elements/actionintegration.h"
#include "iactionviewers.h"

namespace Ui {
class ActionIntegrationViewer;
}

class ActionIntegrationViewer : public QWidget,
                                public IUpdatableViewer,
                                public IPicturableViewer
{
    Q_OBJECT

public:
    explicit ActionIntegrationViewer(QWidget *parent = 0);
    ActionIntegrationViewer(ActionIntegration* action);
    ~ActionIntegrationViewer();
    void update_data() override;
    bool is_picture() override;

private:
    Ui::ActionIntegrationViewer *ui;
    ActionIntegration* m_action;
};

#endif // ACTIONINTEGRATIONVIEWER_H
