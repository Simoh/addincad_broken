#ifndef ACTIONVIEWER_H
#define ACTIONVIEWER_H

#include <QWidget>

namespace Ui {
class ActionViewer;
}

class ActionViewer : public QWidget
{
    Q_OBJECT

public:
    explicit ActionViewer(QWidget *parent = 0);
    ActionViewer(uint id, QWidget* action);
    ~ActionViewer();

    QWidget *get_action_widget();
    void decrease_display_id();
    void update_data();

private slots:
    void on_edit_btn_clicked();
    void on_remove_btn_clicked();
    void on_picture_btn_clicked();

signals:
    void remove();
    void edit();
    void show_picture();

private:
    Ui::ActionViewer *ui;
    QWidget* m_action;
};

#endif // ACTIONVIEWER_H
