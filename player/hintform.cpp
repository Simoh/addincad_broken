#include "hintform.h"
#include "ui_hintform.h"

#include <QDesktopWidget>


HintForm::HintForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HintForm)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::Tool);
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setStyleSheet("background:transparent;");
    this->setGeometry(QApplication::desktop()->availableGeometry());

    QObject::connect(this, SIGNAL(redraw()), this, SLOT(repaint()));
    viewer = nullptr;
}

HintForm::~HintForm()
{
    delete ui;
}

bool HintForm::show_hint(const IAction &action, JournalInfo &journal_info, unsigned long process_id)
{
    //If hint already viewed - destroy him and show new
    remove_hint();

    /*if (action.is_client_action())
    {
        qDebug() << "Constructing client hint object";
        this->viewer = new client_action_viewer(action, journal_info, process_id);
    }
    else
    {
        qDebug() << "Constructing ui hint object";
        this->viewer = new hint_viewer(action, journal_info, process_id);
    }*/

    return true;
}

bool HintForm::show_hint(QPair<QRect,HWND> update_info)
{
    remove_hint();
    if (update_info.first.height() == 0  && update_info.first.width() == 0)
        this->viewer = new client_action_viewer(QPoint(update_info.first.x(),
                                                       update_info.first.y()),
                                                       update_info.second);
    else
        this->viewer = new hint_viewer(update_info.first, update_info.second);

    emit redraw();

    return true;
}

void HintForm::remove_hint()
{
    if (viewer)
    {
        delete viewer;
        viewer = nullptr;
    }
}

//вызывается по событиям
void HintForm::update()
{
    if (this->viewer == nullptr)
        return;
    /*
     * Update positions
     */
    if (this->viewer->update_position())
    {
        emit redraw();
    }
}

void HintForm::reseach_element()
{
    if (this->viewer->search_element())
        emit redraw();
}

void HintForm::paintEvent(QPaintEvent *_event)
{
    Q_UNUSED(_event);

    if (this->viewer == nullptr)
        return;

    //установить окно подсказки сразу после целевого окна в порядке z-order
    SetWindowPos((HWND)this->winId(), this->viewer->get_target_element_wnd(), 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);

    QPainter painter(this);
    //отрисовывем текущий элемент
    this->viewer->draw(painter);
}
