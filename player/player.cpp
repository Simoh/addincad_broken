#include "player.h"

#include "errors.h"
#include "global_constants.h"
#include "service/globalsettings.h"

Player::Player()
{
    QObject::setObjectName("Player");
}

Player::~Player()
{
    if (m_state != Global_constant::state::States::Stopped)
        clear();
}

void Player::start(ulong target_pid, JournalSession session, int position)
{
    qDebug() << "************STARTING PLAYER************";
    try
    {
        //check pid
        if (!Scanner::Instance().process_is_valid(target_pid))
            throw Errors::target_app_is_closed;

        m_playback_journal = session;
        m_current_position = position;

        worker_impl::start(target_pid, session);
        if (session.connection_type() == Global_constant::connection_type::hook.c_str()) {
            m_ui_watchdog.initialize( target_pid, m_playback_journal.configurator() );
        } else {
            //TODO Use other watchdog??
        }

        //play first action
        play_current_action();
    }
    catch (const cancel_event &_event) {
        qDebug() << _event.what();
        clear();
        worker_impl::stop();
    }
    catch(const critical_error &error) {
        qCritical() << error.what();
        clear();
        worker_impl::stop(Global_constant::stop_reasons::connection_refused.c_str());
    }
    catch(const logic_error &error) {
        qWarning() << error.what();
        clear();
        worker_impl::stop(Global_constant::stop_reasons::incorrect_input_data.c_str());
    }

    qDebug() << "************PLAYER STARTED************";
}

void Player::stop(QString reason)
{
    qDebug() << "Receive signal STOP: " << reason;
    m_ui_watchdog.clear();
    worker_impl::stop(reason);
    qDebug() << "************PLAYER STOPPED************";
}

/**
 * @brief Function get tree elements info via CAD_action, search this element on target window,
 * draw hint
 * @param _action - action which be played
 */
void Player::play_current_action()
{
    auto action = m_playback_journal.get_action(m_current_position);
    if(m_playback_journal.connection_type() == Global_constant::connection_type::integration.c_str()) {
        request_info_from_app(action);
        //emit signal?
    } else {
        emit hint_created( m_ui_watchdog.watch_for_element(
                               ( dynamic_cast<ActionHook*>( action.get() ))->element_info() )
                           );
    }
}

void Player::on_data_received()
{
    qDebug() << "Receive signal on_data_received";
    //compare last action with current in playback journal
    if (*m_journal_session.get_last() == *this->m_playback_journal.get_action(m_current_position)) {
        qDebug() << "Data is identity";
        play_next_action();
    }
    //TODO analyze  action and suggest for cancel playing
}

void Player::clear()
{
    m_ui_watchdog.clear();
    m_playback_journal.clear();
    worker_impl::clear();
}

void Player::on_update_event(int type)
{
    switch(type) {
    case WM_UPDATE_POSITION:
    {

        break;
    }
    case WM_UPDATE_RESEARCH:
    {
        break;
    }
    default:
        break;
    }
}

void Player::play_next_action()
{
    qDebug() << "Play next action";
    m_current_position++;

    if (m_current_position < m_playback_journal.get_size()) {
        emit new_action_started();
        play_current_action();
    } else {
        this->stop(Global_constant::stop_reasons::playing_was_finished.c_str());
    }
}

void Player::next_action()
{
    qDebug() << "Receive signal next_action";
    play_next_action();
}

void Player::edit_journal(int _id)
{
    qDebug() << "Receive signal edit_journal";
}
