#include "elementwatchdog.h"
#include "service/logger.h"

ElementInfoPtr ElementWatchdog::m_watched_element;
std::shared_ptr<DisplayedActionCore> ElementWatchdog::m_action_wrap;
std::shared_ptr<Context> ElementWatchdog::m_context;


ElementWatchdog::ElementWatchdog(QObject *parent) : QObject(parent)
{

}

void ElementWatchdog::initialize(ulong pid, std::shared_ptr<Context> context)
{
    CoInitialize(NULL);
    m_hook = SetWinEventHook(EVENT_SYSTEM_MENUPOPUPSTART, EVENT_OBJECT_INVOKED,
                             nullptr, f_hook, pid, 0,
                             WINEVENT_OUTOFCONTEXT | WINEVENT_SKIPOWNTHREAD);
    m_context = context;
    m_watched_element = nullptr;
    m_action_wrap = nullptr;
}

void ElementWatchdog::clear()
{
    m_watched_element = nullptr;
    m_action_wrap = nullptr;
    m_context = nullptr;
    UnhookWinEvent(m_hook);
    CoUninitialize();
}

std::shared_ptr<DisplayedActionCore> ElementWatchdog::watch_for_element(ElementInfoPtr element)
{
    m_watched_element = element;
    m_action_wrap = std::make_shared<DisplayedActionCore>(element); //initial find element
    return m_action_wrap;
}

void ElementWatchdog::f_hook(HWINEVENTHOOK hWinEventHook, DWORD _event, HWND hwnd, LONG idObject, LONG idChild, DWORD dwEventThread, DWORD dwmsEventTime)
{
    if (m_watched_element == nullptr)
        return;

    switch (_event)
    {
    case EVENT_OBJECT_CREATE:
    case EVENT_OBJECT_DESTROY:
    case EVENT_OBJECT_HIDE:
    case EVENT_OBJECT_SHOW:
    {
        Logger::Instance().debug_message("Research element");
        //compare m_watched_element and current
        auto id_element = m_context.get()->ui_tree().get_id( Scanner::Instance().get_ui_info(hwnd, idObject, idChild) );
        if (id_element == m_watched_element->id_element()){
            Logger::Instance().debug_message("Target element founded");
            //update info m_action_wrap
        }
        break;
    }
    case EVENT_OBJECT_REORDER:
    case EVENT_SYSTEM_SCROLLINGSTART:
    case EVENT_SYSTEM_SCROLLINGEND:
    case EVENT_SYSTEM_MOVESIZESTART:
    case EVENT_SYSTEM_MOVESIZEEND:
    case EVENT_OBJECT_CONTENTSCROLLED:
    {
        Logger::Instance().debug_message("Update element position");
        m_action_wrap.get()->update();
        break;
    }
    default:
        return;
    }
}
