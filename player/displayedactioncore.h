#ifndef DISPLAYEDACTION_H
#define DISPLAYEDACTION_H

#include <QObject>
#include <QPoint>
#include <QRect>

#include <Windows.h>
#include <oleacc.h>
#include <utility>

#include "base_elements/element_info.h"

enum class ActionHint_t {
    EMPTY,
    FRAME,
    TARGET,
    HIDDEN_TARGET,
    HIDDEN
};

class DisplayedActionCore : public QObject
{
    Q_OBJECT
public:
    DisplayedActionCore(ElementInfoPtr element);
    void update();

    std::pair<QRect, QPoint> get_frame_pos();

signals:
    void updated();

private:
    QString m_displayed_text;

    IAccessible* m_pAcc;
    ulong m_child_id;
    HWND m_hwnd;

};

#endif // DISPLAYEDACTION_H
