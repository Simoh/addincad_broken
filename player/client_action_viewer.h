#ifndef CLIENT_ACTION_VIEWER_H
#define CLIENT_ACTION_VIEWER_H

#include <QObject>
#include <QPoint>
#include "hint_viewer.h"
#include "service/globalsettings.h"
#include <Windows.h>
#include <QCursor>

class client_action_viewer : public hint_viewer
{
public:
    client_action_viewer(QPoint pt, HWND hwnd);
    client_action_viewer(const IAction &action, JournalInfo &journal_info, unsigned long process_id);
    ~client_action_viewer();
    void draw(QPainter &painter);
    bool update_position();
    bool search_element();
private:
    QPoint m_input_coordinate; //input coordinate from action
    QPoint m_target_coordinate; //calculated coordinate in runtime

    QPoint calculate_absolute_coord_for_energycs();
    QPoint calculate_absolute_coord_via_scrollbars();
    int get_delta_scrollbar(int nBar);

    QImage img_client_hint;
    QImage img_arrow;

    void draw_arrow(QPainter &painter);

    RuntimeObject coordinate_object;
    RuntimeObject scale_object;
};

#endif // CLIENT_ACTION_VIEWER_H
