#ifndef PLAYER_H
#define PLAYER_H

#include "journals/journal_session.h"
#include "configurator/context.h"
#include "base_elements/worker_impl.h"
#include "elementwatchdog.h"

#include <QObject>

class Player: public worker_impl
{
    Q_OBJECT
//EXTERNAL API
public slots:
    void start(ulong target_pid, JournalSession session, int position = 0) override;
    void stop(QString reason = Global_constant::stop_reasons::default_reason.c_str()) override;
    void next_action();

//INTERNAL API
public slots:
    void edit_journal(int _id); //unused

signals:
    void new_action_started();
    void active_action_changed(ActionHook hint);
    void hint_created(std::shared_ptr<DisplayedActionCore> action);
    void hint_pos_updated(QRect new_position);

private:
    JournalSession m_playback_journal;
    int m_current_position;
    ElementWatchdog m_ui_watchdog;
    std::shared_ptr<DisplayedActionCore> m_current_action_pos;

    void play_current_action();
    void play_next_action();

protected:
    void on_data_received() override;
    void clear() override;
    void on_update_event(int type) override;

public:
    Player();
    ~Player();
};

#endif // PLAYER_H
