#ifndef HINTMANAGER_H
#define HINTMANAGER_H

#include <QObject>

#include "displayedactioncore.h"
#include "widgets/actionhintviewer.h"

class HintManager : public QObject
{
    Q_OBJECT
public:
    explicit HintManager(QObject *parent = nullptr);
    ~HintManager();
    void clear();

public slots:
    void hint_created(std::shared_ptr<DisplayedActionCore> action);

private:
    std::vector<std::shared_ptr<ActionHintViewer>> mActions;
};

#endif // HINTMANAGER_H
