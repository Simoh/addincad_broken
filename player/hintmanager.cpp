#include "hintmanager.h"

HintManager::HintManager(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<std::shared_ptr<DisplayedActionCore>>("std::shared_ptr<DisplayedActionCore>");
}

HintManager::~HintManager()
{
    mActions.clear();
}

void HintManager::clear()
{
    mActions.clear();
}

void HintManager::hint_created(std::shared_ptr<DisplayedActionCore> action)
{
    mActions.clear(); //TODO temporarily
    mActions.push_back(std::make_shared<ActionHintViewer>(action));
}
