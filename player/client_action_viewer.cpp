#include "client_action_viewer.h"
#include <chrono>

client_action_viewer::client_action_viewer(QPoint pt, HWND hwnd):
    hint_viewer(hwnd),
    m_target_coordinate(pt)
{
}

client_action_viewer::client_action_viewer(const IAction &action, JournalInfo &journal_info, unsigned long process_id):
    hint_viewer(action, journal_info, process_id)
{
    this->m_input_coordinate = action.coordinate();
    this->img_client_hint.load(":/images/img/target.png");
    this->img_arrow.load(":/images/img/arrow.png");
}

client_action_viewer::~client_action_viewer()
{
}

void client_action_viewer::draw(QPainter &painter)
{
    qDebug() << "draw";
    //if coordinate is valid - show target
    if (!this->m_target_coordinate.isNull())
    {
        //TODO :If absolute coordinate outside current client window - show pointer to target place
        if (this->hint_instance.get_rect().contains(this->m_target_coordinate))
        {
            qDebug () << "Draw target. Coordinate is : " << this->m_target_coordinate;
            painter.drawImage(this->m_target_coordinate.x() - 20, this->m_target_coordinate.y() - 20, img_client_hint.scaled(40,40));
        }
        else
        {
            qDebug () << "Draw arrow. Coordinate is : " << this->m_target_coordinate;
            // Draw arrow
            draw_arrow(painter);
        }
    }
    else
    {
        qDebug() << "Coordinate isn`t found. Check draw rect";
        //in other case - if need focus to window - draw rect from base class
        if (!this->m_hint_rect.isNull())
        {
            qDebug() << "Draw rect : " << this->m_hint_rect;
            hint_viewer::draw(painter);
        }
    }
}

bool client_action_viewer::update_position()
{
    qDebug () << "Update position";
    auto start = std::chrono::steady_clock::now();
    //Calculate coordinate
    QPoint buf_point;

    if (!m_target_element_founded)
    {
        hint_viewer::update_position();
        m_target_coordinate = QPoint();
        return false;
    }
    this->m_hint_rect = QRect();

    //TODO: this can be removed for universal version
    //if program is energycs - calculate via coordinate
    /*if (global_setting::Instance().current_process_name().indexOf("energycs") != -1)
    {
        qDebug() << "Target app is EnergyCS. Calculate coordinate via coord";
        buf_point = calculate_absolute_coord_for_energycs();
    }*/
    //calculate via scrollbars
    if (buf_point.isNull() && this->m_hint_rect.isNull())
    {
        qDebug() << "Calculate coordinate via scrollbars";
        buf_point = calculate_absolute_coord_via_scrollbars();
    }

    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>
                        (std::chrono::steady_clock::now() - start);
    qDebug() << "calculate client event time: " << duration.count() << "ms";

    if (buf_point != m_target_coordinate)
    {
        m_target_coordinate = buf_point;
        return true;
    }
    if (!m_hint_rect.isNull())
    {
        return true;
    }

    return false;
}

bool client_action_viewer::search_element()
{
    //search coordinate
    Scanner::Instance().find_coordinate(this->hint_instance.pacc(),
                                        this->coordinate_object,
                                        this->scale_object);

    return hint_viewer::search_element();
}

QPoint client_action_viewer::calculate_absolute_coord_for_energycs()
{
    qDebug() << "calculate_absolute_coord_for_energycs";
    QPoint result_point;
    this->m_hint_rect.setRect(0, 0, 0, 0); // clear rect for hint


    auto start = std::chrono::steady_clock::now();
    //Try find coordinate and scale
    std::pair<QPoint,int> pt_and_scale;
    if (coordinate_object.is_empty() && scale_object.is_empty())
    {
       pt_and_scale.first  = Scanner::Instance().get_coordinate_energycs(coordinate_object);
       pt_and_scale.second = Scanner::Instance().get_scale_energycs(scale_object);
    }


    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>
                        (std::chrono::steady_clock::now() - start);
    qDebug() << "find coordinate time: " << duration.count() << "ms";

    //If we can`t find - try calculate via scrollbars
    if (pt_and_scale.first.isNull() && pt_and_scale.second == 0)
    {
        qDebug () << "coordinate doesn`t founded "<< pt_and_scale.first;
        return result_point;
    }

    //If mouse under client target - calculate absolute coordinate
    if (this->m_element_info.back() == Scanner::Instance().get_element_under_cursor())
    {
        pt_and_scale.first = this->m_input_coordinate - pt_and_scale.first;
        pt_and_scale.first *= (pt_and_scale.second / (double) 100) * 16; // 16 - koef for energycs
        result_point = QCursor::pos() + pt_and_scale.first;
        qDebug() << "Coordinate was founded. Target coordinate: " << result_point;
    }
    else //In other CASE - set dlg for move cursor to client area
    {
        qDebug() << "client area hasn`t mouse focus";
        //update rect field. Will be show to user. User should move to client area
        hint_viewer::update_position();
    }

    return result_point;
}

QPoint client_action_viewer::calculate_absolute_coord_via_scrollbars()
{
    qDebug() << "Calculate absolute coordinate via scrollbars";
    m_input_coordinate;
    //get absolute coordinate top left client corner
    QPoint absolute_client_coordinate(get_delta_scrollbar(SB_HORZ), get_delta_scrollbar(SB_VERT));
    //get difference with input value
    QPoint difference = this->m_input_coordinate - absolute_client_coordinate;
    //transfotm to screen value
    POINT screen_pt;
    screen_pt.x = difference.x();
    screen_pt.y = difference.y();
    ClientToScreen(this->hint_instance.hwnd(), &screen_pt);
    return QPoint(screen_pt.x, screen_pt.y);
}

int client_action_viewer::get_delta_scrollbar(int nBar)
{
    qDebug() << "get_delta_scrollbar started";
    int min, max;
    GetScrollRange(this->hint_instance.hwnd(), nBar, &min, &max);
    qDebug() << "get_delta_scrollbar Max horizontal range :" << min << " , " << max;

    int real_max = max - min;
    if (real_max == 0)
        return 0;

    //Get window rect
    RECT rect_window;
    GetClientRect(this->hint_instance.hwnd(), &rect_window);

    double delta = (nBar == SB_HORZ) ? rect_window.right : (nBar == SB_VERT) ? rect_window.bottom : 0;
    delta /= real_max;

    //Get value of division
    double scroll_pos = GetScrollPos(this->hint_instance.hwnd(), nBar);
    qDebug() << "get_delta_scrollbar scroll position: " << scroll_pos;

    return delta * scroll_pos;
}


void client_action_viewer::draw_arrow(QPainter &painter)
{
    QRect border_client = this->hint_instance.get_rect();
    QPoint arrow_coordinate = this->m_target_coordinate;

    double rotate_angle = 0;

    //Arrow to right
    if ( border_client.right() < this->m_target_coordinate.x() )
    {
        arrow_coordinate.setX(border_client.right());
        qDebug() << "Rotate to 90";
        rotate_angle = 90;
    }
    else
    {
        //Arrow to left
        if ( border_client.left() > this->m_target_coordinate.x() )
        {
            arrow_coordinate.setX(border_client.left());
            qDebug() << "Rotate to 270";
            rotate_angle = 270;
        }
    }
    //Arrow to bottom
    if ( border_client.bottom() < this->m_target_coordinate.y())
    {
        arrow_coordinate.setY(border_client.bottom());
        qDebug() << "Rotate to 180";
        rotate_angle = 180;
    }
    else
    {
        //Arrow to top
        if ( border_client.top() > this->m_target_coordinate.y() )
        {
            arrow_coordinate.setY(border_client.top());
            qDebug() << "Rotate to 0";
            rotate_angle = 0;
        }
    }

    //Rotate image
    if (rotate_angle)
    {
        qDebug() << "rotate image";
        QMatrix rotate_matrix;
        rotate_matrix = rotate_matrix.rotate(rotate_angle);
        QTransform transform(rotate_matrix);
        painter.drawImage(arrow_coordinate.x() - 20, arrow_coordinate.y() - 20, img_arrow.transformed(transform).scaled(40,40));
    }
    else
    {
        painter.drawImage(arrow_coordinate.x() - 20, arrow_coordinate.y() - 20, img_arrow.scaled(40,40));
    }
}
