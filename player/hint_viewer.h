#ifndef HINT_VIEWER_H
#define HINT_VIEWER_H

#include <QPoint>
#include <QRect>
#include <vector>
#include <QPainter>

#include "base_elements/element_info.h"
#include "journals/journal_info.h"
#include "base_elements/action.h"
#include "configurator/scanner.h"
#include "configurator/runtime_object.h"

/**
 * @brief The hint_viewer class stored and update information about action with UI element hint
 */
class hint_viewer
{
public:
    hint_viewer(HWND hwnd);
    hint_viewer(QRect rect, HWND hwnd = NULL);
    hint_viewer(const IAction &action, JournalInfo &journal_info, unsigned long process_id);
    ~hint_viewer();
    virtual void draw(QPainter &painter);
    virtual bool update_position();
    virtual bool search_element();
    void set_rect(QRect rect);
    HWND get_target_element_wnd();
protected:
    vector<ElementInfo> m_element_info;
    bool m_target_element_founded;
    QRectF m_hint_rect;
    RuntimeObject hint_instance;
    unsigned long m_process_id;
};

#endif // HINT_VIEWER_H
