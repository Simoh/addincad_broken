#ifndef HINTFORM_H
#define HINTFORM_H

#include <QDialog>
#include <QPainter>
#include <QPair>

#include "base_elements/element_info.h"
#include "base_elements/action.h"
#include "journals/journal_info.h"
#include "service/globalsettings.h"
#include "hint_viewer.h"
#include "client_action_viewer.h"

namespace Ui {
class HintForm;
}

class HintForm : public QDialog
{
    Q_OBJECT

public:
    explicit HintForm(QWidget *parent = 0);
    ~HintForm();
    bool show_hint(const IAction &action, JournalInfo &journal_info, unsigned long thread_id);
    bool show_hint(QPair<QRect, HWND> update);
    void remove_hint();
private:
    Ui::HintForm *ui;

    hint_viewer *viewer;

public slots:
    void update();
    void reseach_element();

signals:
    void redraw();//фиктивный сигнал для вызова перерисовки формы

protected:
    void paintEvent(QPaintEvent* _event);
};

#endif // HINTFORM_H
