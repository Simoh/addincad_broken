#ifndef PLAYERLISTWIDGET_H
#define PLAYERLISTWIDGET_H

#include <QWidget>

#include "journals/journal_session.h"

namespace Ui {
class PlayerListWidget;
}

class PlayerListWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PlayerListWidget(QWidget *parent = 0);
    ~PlayerListWidget();
    void show_actions(const QStringList& action_list);
    void show_actions(const JournalSession& _session);

private:
    Ui::PlayerListWidget *ui;
    uint m_current_action_id;

public slots:
    void new_active_action();
};

#endif // PLAYERLISTWIDGET_H
