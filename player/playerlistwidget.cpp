#include "playerlistwidget.h"
#include "ui_playerlistwidget.h"

#include <QTreeWidgetItem>

#include "global_constants.h"

PlayerListWidget::PlayerListWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PlayerListWidget)
{
    ui->setupUi(this);
    ui->treeWidget->setColumnWidth(0, 30);
}

PlayerListWidget::~PlayerListWidget()
{
    delete ui;
}

void PlayerListWidget::show_actions(const QStringList& action_list)
{
    m_current_action_id = 0;
    ui->treeWidget->clear();

    for (auto& action: action_list) {
        QTreeWidgetItem *newItem = new QTreeWidgetItem();
        newItem->setText(1, action);
        ui->treeWidget->addTopLevelItem(newItem);
    }
    ui->treeWidget->topLevelItem(m_current_action_id)->setSelected(true);
    ui->treeWidget->topLevelItem(m_current_action_id)->setIcon(0, QIcon(":/images/img/buttons/play_black.png"));
}

void PlayerListWidget::show_actions(const JournalSession& _session)
{
    show_actions(_session.to_string_list());
}

void PlayerListWidget::new_active_action()
{
    //set previous element disabled and striked
    ui->treeWidget->topLevelItem(m_current_action_id)->setSelected(false);
    ui->treeWidget->topLevelItem(m_current_action_id)->setIcon(0, QIcon());
    ui->treeWidget->topLevelItem(m_current_action_id)->setDisabled(true);
    QFont font = ui->treeWidget->topLevelItem(m_current_action_id)->font(1);
    font.setStrikeOut(true);
    ui->treeWidget->topLevelItem(m_current_action_id)->setFont(1, font);

    m_current_action_id++;
    ui->treeWidget->topLevelItem(m_current_action_id)->setSelected(true);
    ui->treeWidget->topLevelItem(m_current_action_id)->setIcon(0, QIcon(":/images/img/buttons/play_black.png"));
}
