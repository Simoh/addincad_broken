#ifndef ELEMENTWATCHDOG_H
#define ELEMENTWATCHDOG_H

#include <QObject>
#include <QRect>

#include <Windows.h>

#include <memory>

#include "base_elements/element_info.h"
#include "configurator/context.h"
#include "displayedactioncore.h"

class ElementWatchdog : public QObject
{
    Q_OBJECT
public:
    explicit ElementWatchdog(QObject *parent = nullptr);
    void initialize(ulong pid, std::shared_ptr<Context> context);
    void clear();
    std::shared_ptr<DisplayedActionCore> watch_for_element(ElementInfoPtr element);

private:
    HWINEVENTHOOK m_hook;
    static ElementInfoPtr m_watched_element;
    static std::shared_ptr<DisplayedActionCore> m_action_wrap;
    static std::shared_ptr<Context> m_context;

    static void CALLBACK f_hook(HWINEVENTHOOK hWinEventHook,
        DWORD _event,
        HWND  hwnd,
        LONG  idObject,
        LONG  idChild,
        DWORD dwEventThread,
        DWORD dwmsEventTime
        );
};

#endif // ELEMENTWATCHDOG_H
