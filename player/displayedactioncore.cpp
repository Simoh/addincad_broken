#include "displayedactioncore.h"

#include "configurator/scanner.h"

DisplayedActionCore::DisplayedActionCore(ElementInfoPtr element):
   m_pAcc(NULL),
   m_child_id(CHILDID_SELF),
   m_hwnd(NULL)
{

}

void DisplayedActionCore::update()
{
    emit updated();
}

std::pair<QRect, QPoint> DisplayedActionCore::get_frame_pos()
{
    QRect result_rect = QRect(qrand() % 300, qrand() % 300, 300, 140);
    QPoint result_point = QPoint(qrand() % 300, qrand() % 140);
    return std::make_pair(result_rect, result_point);
}
