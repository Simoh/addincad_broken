#include "hint_viewer.h"

#include <QDebug>

hint_viewer::hint_viewer(HWND hwnd)
{
    this->hint_instance = RuntimeObject(hwnd);
    m_hint_rect = this->hint_instance.get_rect();
}

hint_viewer::hint_viewer(QRect rect, HWND hwnd):
    m_hint_rect(rect)
{
    this->hint_instance = RuntimeObject(hwnd);
    //WARNING bad style
    if (hwnd)
    {
        POINT buf_pt = { rect.x(), rect.y() };
        ClientToScreen(hwnd, &buf_pt);
        m_hint_rect.moveTo(buf_pt.x, buf_pt.y);
    }
}

hint_viewer::hint_viewer(const IAction &action, JournalInfo &journal_info, unsigned long process_id):
    m_process_id(process_id), m_target_element_founded(false)
{
    /* Save action to buffer and find element hirerarhy elements for this action and save this for next updates */
    //m_element_info = journal_info.get_EI_with_parents(action.id_element());

    //search_element();
}

hint_viewer::~hint_viewer()
{

}

bool hint_viewer::update_position()
{
    qDebug() << "update position current element";
    //for ui element doesn`t matter that element was founded fully
    QRectF new_rect = hint_instance.get_rect();
    if (m_hint_rect != new_rect)
    {
        m_hint_rect = new_rect;
        return true;
    }
    return false;
}

bool hint_viewer::search_element()
{
    qDebug() << "search element";
    //Get instance for target element
    m_target_element_founded = Scanner::Instance().element_is_valid(m_element_info, m_process_id, hint_instance);

    return update_position();
}

void hint_viewer::set_rect(QRect rect)
{
    m_hint_rect = rect;
}

HWND hint_viewer::get_target_element_wnd()
{
    return this->hint_instance.hwnd();
}

void hint_viewer::draw(QPainter &painter)
{
    painter.setPen(QPen(Qt::blue, 3, Qt::SolidLine));
    painter.drawRect(m_hint_rect);
}
