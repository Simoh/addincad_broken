#include "widgets/mainwindow.h"
#include "redactor/redactorform.h"

#include <QApplication>
#include <QProcess>
#include <QCommandLineParser>
#include <QDebug>

#include "service/logger.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QCoreApplication::setApplicationName("AddInCAD");
    QCoreApplication::setApplicationVersion("0.9.0");

    //setup parser for command line arguments
    QCommandLineParser parser;
    parser.setApplicationDescription("Test helper");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("filename", QCoreApplication::translate("main", "File to open (session or configuration)"));
    parser.addOptions({ // A mode option (-d, --debug)
                        {{"d", "debug"},
                         QCoreApplication::translate("main", "Run with command line output for debug information")},
                      });

    // Process the actual command line arguments given by the user
    parser.process(app);

    const QStringList args = parser.positionalArguments();

    Logger::Instance().initialize( parser.isSet("debug") );

    if (args.isEmpty()) {
        //run in mode for registration/playing
        MainWindow w;
        w.show();
        
        return app.exec();
    } else {
        RedactorForm redactor;
        qDebug() << "Open file via Redactor Form";
        redactor.redacting_session(args.front());
        
        return app.exec();
    }
    
    return app.exit(0);
}
