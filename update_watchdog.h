#ifndef UPDATE_WATCHDOG_H
#define UPDATE_WATCHDOG_H

#include <QDialog>
#include <Windows.h>


namespace Ui {
    class Update_watchdog;
}

class Update_watchdog : public QDialog
{
    Q_OBJECT

public:
    explicit Update_watchdog(QWidget *parent = 0);
    ~Update_watchdog();
    void start(unsigned long process_id);
    void stop();
    void pause();
    void unpause();
    bool is_active();
protected:
    bool nativeEvent(const QByteArray& _event_type, void* _message, long* _result);

private:
    Ui::Update_watchdog *ui;

    void load_functions();

    HMODULE HookLib;
    typedef BOOL(*function_dll)(unsigned long, HWND);
    function_dll f_set_update_hook;
    BOOL(*f_unset_hook)();

    bool active;

signals:
    void update_msg(int type); //Link Qt::QueuedConnection
};

#endif // UPDATE_WATCHDOG_H
