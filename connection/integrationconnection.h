#ifndef INTEGRATION_CONNECTION_H
#define INTEGRATION_CONNECTION_H

#include <QObject>

#include "link.h"
#include "iconnection.h"

class IntegrationConnection : public IConnection
{
    Q_OBJECT
public:
    explicit IntegrationConnection(QObject *parent = 0);
    ~IntegrationConnection();
    bool connect_to_app(unsigned int pid, std::chrono::seconds timeout = std::chrono::seconds(10));
    void disconnect_from_app();
    void pause();
    void unpause();
    bool data_request(QString msg);

private:
    void destroy_links();

    bool create_service_link();
    bool create_data_link();

    Link service_link;
    Link data_link;

    unsigned int process_id;

signals:
    void disconnect_from_app_sig();

private slots:
    void service_data_received(QString msg);
    void data_received(QString msg);
    void connection_lost();
};

#endif // INTEGRATION_CONNECTION_H
