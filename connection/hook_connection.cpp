#include "hook_connection.h"

#include <QDebug>

#include "global_constants.h"
#include "errors.h"
#include "service/logger.h"

Hook_connection::Hook_connection(QObject *parent) :
    IConnection(parent)
{
    this->HookLib = LoadLibrary(L"hook32.dll");
    this->f_hook = nullptr;
    this->f_set_pause_hook = nullptr;
    this->f_unset_pause_hook = nullptr;
    this->hMailslot = nullptr;

    if (!this->load_functions())
    {
        qCritical() << "Cannot load functions";
        if (this->HookLib)
            FreeLibrary(this->HookLib);
        this->HookLib = nullptr;
    }

    QObject::connect(&data_checker, &Timer::timeout, this, &Hook_connection::data_in_channel);

    m_connection_status = connection_status_t::DISCONNECTED;
}

Hook_connection::~Hook_connection()
{
    if (m_connection_status == connection_status_t::CONNECTED)
        disconnect_from_app();

    if (this->HookLib)
        FreeLibrary(this->HookLib);
}

bool Hook_connection::connect_to_app(uint pid, std::chrono::seconds timeout)
{
    qDebug() << "set_hook started. Process id: " << pid;
    Logger::Instance().debug_message("Try connect by hook");
    try
    {
        //create mailslot
        if (!initialize_mailslot(pid))
            throw Errors::Hook::cannot_load_dll;

        //установить хук
        CoInitialize(NULL);
        m_hook = SetWinEventHook(EVENT_SYSTEM_MENUPOPUPSTART, EVENT_OBJECT_INVOKED,
                                 HookLib, f_hook, pid, 0,
                                 WINEVENT_INCONTEXT | WINEVENT_SKIPOWNTHREAD);
        if (m_hook == NULL) {
            throw Errors::Hook::cannot_connect_to_app;
        }
        data_checker.start(200);

        if (IConnection::connect_to_app(pid, timeout) == false) {
            throw Errors::Hook::cannot_connect_to_dll;
        }
        Logger::Instance().debug_message("Hook connection established");
    }
    catch (const critical_error &ex)
    {
        qCritical() << ex.what();
        Logger::Instance().debug_message("Cannot setup hook connection: " + QString(ex.what()));
        disconnect_from_app();
        return false;
    }
    return true;
}

void Hook_connection::disconnect_from_app()
{
    qDebug() << "Unset hook";

    IConnection::disconnect_from_app();
    UnhookWinEvent(m_hook);

    data_checker.stop();

    //check data if it was written
    data_in_channel();

    if (this->hMailslot)
        CloseHandle(this->hMailslot);
    this->hMailslot = nullptr;

    CoUninitialize();

    Logger::Instance().debug_message("Disconnect Successed");
}


void Hook_connection::pause()
{
    qDebug() << "Pause hook";
    f_set_pause_hook();
    data_checker.stop();
}

void Hook_connection::unpause()
{
    qDebug() << "Unpause hook";
    this->f_unset_pause_hook();
    data_checker.start(200);
}

/**
 * @brief Function create mailslot with dll
 * @return nothing or
 * @retval cannot_connect_to_dll excecption.
 */
bool Hook_connection::initialize_mailslot(uint pid)
{
    qDebug() << "create_mailslot started";
    std::string channel_name = Global_constant::nameMailslot + std::to_string(pid);
    hMailslot = CreateMailslotA(channel_name.c_str(), 0, 0, NULL);
    // Если возникла ошибка, завершаем работу приложения
    if(hMailslot == INVALID_HANDLE_VALUE)
    {
        qCritical() << "Cannot create mailslot : " << channel_name.c_str();
        return false;
    }
    return true;
}

/**
 * @brief Function read data from mailslot and emit signal with QString msg
 */
void Hook_connection::data_in_channel()
{
    unsigned long cbMessages = 0, cbMsgNumber = 0;
    do
    {
        //посмотреть сколько сообщений в стеке
        if (!GetMailslotInfo(hMailslot, NULL, &cbMessages, &cbMsgNumber, NULL)) {
            //TODO catch this error
            qDebug() << "Get MailSlotInfo Error!!!";
        }

        if (cbMessages > 0 && cbMsgNumber > 0)  {
            std::wstring recv_data;
            unsigned long cbRead = 0;
            wchar_t* buf = new wchar_t[cbMessages];
            if(ReadFile(hMailslot, buf, 4096, &cbRead, NULL))
            {
                qDebug() << "Received bytes from mailslot:" << cbRead;
                recv_data.assign(buf, cbRead / sizeof(wchar_t));
                emit receive_data_sig(QString::fromWCharArray(recv_data.c_str()));
            }
            else
                qDebug() << "Error Data Transfer!!!"; //TODO catch this error
        }
    } while (cbMsgNumber > 0);
}

/**
 * @brief function load function Set and Unset Hook from dll
 * @return nothing or
 * @throw critical_error if problems with load functions from dll
 */
bool Hook_connection::load_functions()
{
    qDebug() << "load_functions started";
    /* Load set hook */
    f_hook = (function_hook_dll)GetProcAddress(HookLib, "hook");
    if (f_hook == NULL)
    {
        qCritical() << "Cannot load HOOK function";
        return false;
    }

    /* Load set pause hook */
    (FARPROC &)f_set_pause_hook = GetProcAddress(HookLib, "Paused");
    if (f_set_pause_hook == NULL)
    {
        qCritical() << "Cannot load Paused function";
        return false;
    }

    /* Load unset pause hook */
    (FARPROC &)f_unset_pause_hook = GetProcAddress(HookLib, "Unpaused");
    if (f_unset_pause_hook == NULL)
    {
        qDebug() << "Cannot load Unpaused function";
        return false;
    }
    return true;
}
