#include "integrationconnection.h"

#include <QDebug>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

#include "global_constants.h"
#include "service/logger.h"

IntegrationConnection::IntegrationConnection(QObject *parent) : IConnection(parent)
{
}

IntegrationConnection::~IntegrationConnection()
{
    if (m_connection_status == connection_status_t::CONNECTED) {
        disconnect_from_app();
    }
}

bool IntegrationConnection::connect_to_app(unsigned int pid, std::chrono::seconds timeout)
{    
    Logger::Instance().debug_message("Try connect by integration lib");
    this->process_id = pid;

    if (!create_service_link()) {
        qCritical() << "Create service link is failed";
        Logger::Instance().debug_message("Connect Failure!");
        return false;
    }

    if (!create_data_link()) {
        qCritical() << "Create data link is failed";
        Logger::Instance().debug_message("Connect Failure!");
        return false;
    }

    //generate response for connect
    QJsonObject response;
    response["action"] = "connect";

    //Send response
    if (!service_link.send_message( QJsonDocument(response).toJson().toStdString() )) {
        qCritical() << "Cannot send message for initiate connection";
        Logger::Instance().debug_message("Connect Failure!");
        disconnect_from_app();
        return false;
    }

    IConnection::connect_to_app(pid, timeout);
    qDebug() << "Integration connection established";
    Logger::Instance().debug_message("Connect Successed!");

    return true;
}

void IntegrationConnection::disconnect_from_app()
{
    qDebug () << "integration link was disconnected";

    //generate response for disconnect
    QJsonObject response;
    response["action"] = "disconnect";

    //Send response
    if (service_link.send_message( QJsonDocument(response).toJson().toStdString() )) {
        qCritical() << "Cannot send message to disconnect. Connection will be close automatically";
        Logger::Instance().debug_message("Cannot send message to disconnect. Connection will be close automatically");
    }

    destroy_links();

    IConnection::disconnect_from_app();
    Logger::Instance().debug_message("Disconnect Successed!");
}

void IntegrationConnection::pause()
{
    //generate response for pause
    QJsonObject response;
    response["action"] = "pause";

    //Send response
    service_link.send_message( QJsonDocument(response).toJson().toStdString() );
}

void IntegrationConnection::unpause()
{
    //generate response for unpause
    QJsonObject response;
    response["action"] = "unpause";

    //Send response
    service_link.send_message( QJsonDocument(response).toJson().toStdString() );
}

bool IntegrationConnection::data_request(QString msg)
{
    Logger::Instance().debug_message("Data request for target_app: " + msg);
    auto request_str = std::wstring((wchar_t*)msg.unicode(), msg.length());

    return data_link.send_message(request_str);
}

void IntegrationConnection::destroy_links()
{
    qDebug () << "integration links destroyed";

    QObject::disconnect(&service_link, &Link::input_message, this, &IntegrationConnection::service_data_received);
    QObject::disconnect(&service_link, &Link::connection_lost, this, &IntegrationConnection::connection_lost);
    service_link.destroy_link();

    QObject::disconnect(&data_link, &Link::input_message, this, &IntegrationConnection::data_received);
    QObject::disconnect(&data_link, &Link::connection_lost, this, &IntegrationConnection::connection_lost);
    data_link.destroy_link();
}

bool IntegrationConnection::create_service_link()
{
    //TODO clear this code part
    const auto process_str = std::to_string(this->process_id);

    //Generate names for links
    std::string mailslot_service_sender_name(Global_constant::mailslot_service_target_app_template);
    mailslot_service_sender_name.insert(mailslot_service_sender_name.length() - 1, process_str);
    qDebug() << "try connect to sender: " << mailslot_service_sender_name.c_str();

    //create server for incoming service messages
    std::string mailslot_service_listener_name(Global_constant::mailslot_service_this_template);
    mailslot_service_listener_name.insert(mailslot_service_listener_name.length() - 1, process_str);
    qDebug() << "try connect to listener: " << mailslot_service_listener_name.c_str();

    if (service_link.set_link(mailslot_service_sender_name, mailslot_service_listener_name) == false)
        return false;

    QObject::connect(&service_link, &Link::input_message, this, &IntegrationConnection::service_data_received, Qt::QueuedConnection);
    QObject::connect(&service_link, &Link::connection_lost, this, &IntegrationConnection::connection_lost, Qt::QueuedConnection);

    return true;
}

bool IntegrationConnection::create_data_link()
{
    const auto process_str = std::to_string(this->process_id);

    //Create server for listen messages
    std::string mailslot_data_sender_name(Global_constant::mailslot_data_target_app_template);
    mailslot_data_sender_name.insert(mailslot_data_sender_name.length() - 1, process_str);
    qDebug() << "try connect to data sender: " << mailslot_data_sender_name.c_str();

    //Create server for incoming data messages
    std::string mailslot_data_listener_name(Global_constant::mailslot_data_this_template);
    mailslot_data_listener_name.insert(mailslot_data_listener_name.length() - 1, process_str);
    qDebug() << "try connect to data receiver: " << mailslot_data_listener_name.c_str();

    if (data_link.set_link_unicode(mailslot_data_sender_name, mailslot_data_listener_name) == false)
        return false;

    QObject::connect(&data_link, &Link::input_message, this, &IntegrationConnection::data_received, Qt::QueuedConnection);
    QObject::connect(&data_link, &Link::connection_lost, this, &IntegrationConnection::connection_lost, Qt::QueuedConnection);

    return true;
}


void IntegrationConnection::service_data_received(QString msg)
{
    qDebug() << "receive service data: " << msg;
    //parse json
    QJsonDocument info(QJsonDocument::fromJson( msg.toUtf8()));
    if (!info.object()["action"].isNull()) {
        //check connectivity
        if (info.object()["action"].toString() == "disconnect") {
            this->destroy_links();
            emit disconnect_from_app_sig();
            return;
        }
    }
}

void IntegrationConnection::data_received(QString msg)
{
    qDebug() << "receive client data: " << msg;
    //put data to field - app_data
    QJsonDocument jsonDoc_buf = QJsonDocument::fromJson(msg.toUtf8());
    QJsonObject json_obj_result;
    if (!jsonDoc_buf.isNull()) {
        //action for update
        if (jsonDoc_buf.object().contains("update")) {
            json_obj_result["update"] = jsonDoc_buf.object()["update"];
            json_obj_result["event code"] = WM_UPDATE_POSITION;
        }
        else {
            if (!jsonDoc_buf.object().contains("comment"))
                json_obj_result["comment"] = tr("Невозможно получить информацию о действии");
            else
                json_obj_result["comment"] = jsonDoc_buf.object()["comment"];

            if (!jsonDoc_buf.object().contains("body"))
                json_obj_result["body"] = msg;
            else
                json_obj_result["body"] = jsonDoc_buf.object()["body"];

            if (jsonDoc_buf.object().contains("coordinates"))
                json_obj_result["coordinates"] = jsonDoc_buf.object()["coordinates"];

            if (jsonDoc_buf.object().contains("handle"))
                json_obj_result["handle"] = jsonDoc_buf.object()["handle"];

            json_obj_result["event code"] = WM_INTEGRATION_EVENT;
        }

        qDebug() << "receive client data: " << json_obj_result;
        Logger::Instance().debug_message("Received message : " + QString(jsonDoc_buf.toJson(QJsonDocument::Compact)));
    } else
        Logger::Instance().debug_message("Cannot parse json : " +  msg);

    jsonDoc_buf = QJsonDocument(json_obj_result);

    emit receive_data_sig( QString(jsonDoc_buf.toJson(QJsonDocument::Compact)) );
}

void IntegrationConnection::connection_lost()
{
    Logger::Instance().debug_message("Error! Connection was lost!");
    qDebug() << "Connection lost";

    this->destroy_links();
    emit connection_lost_sig();
}
