#include "iconnection.h"

#include <QDebug>
#include <future>
#include "errors.h"

IConnection::IConnection(QObject *parent) : QObject(parent)
{

}

bool IConnection::connect_to_app(uint pid, std::chrono::seconds timeout)
{
    m_timeout_to_connect = timeout;

    m_connection_status.store(connection_status_t::CONNECTED);

    std::future<HANDLE> health_obj = std::async(std::launch::async, &IConnection::sync_with_app, this, pid);
    if (health_obj.wait_for(m_timeout_to_connect) != std::future_status::ready)
    {
        m_connection_status.store(connection_status_t::DISCONNECTED);
        return false;
    }

    health_checker_thread = std::thread(&IConnection::health_check, this, health_obj.get());

    return true;
}

void IConnection::disconnect_from_app()
{
    m_connection_status.store(connection_status_t::DISCONNECTED);
    if (health_checker_thread.joinable()) {
        health_checker_thread.join();
    }
}

bool IConnection::is_established()
{
    return m_connection_status.load() == connection_status_t::CONNECTED;
}

HANDLE IConnection::sync_with_app(uint pid)
{
    HANDLE hSem = NULL;
    QString name_sync("Global\\AddInCADSyncObject" + QString::number(pid));
    qDebug() << "Waiting to open sync object: " << name_sync;
    do {
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
        hSem = OpenMutex(SYNCHRONIZE, FALSE, name_sync.toStdWString().c_str());
    } while (hSem == NULL && m_connection_status == connection_status_t::CONNECTED);

    return hSem;
}

void IConnection::health_check(HANDLE health_obj)
{
    while(m_connection_status.load() == connection_status_t::CONNECTED) {
        if (WaitForSingleObject(health_obj, 200) != WAIT_TIMEOUT) {
             m_connection_status.store(connection_status_t::BROKEN);
        }
    }
    CloseHandle(health_obj);

    if (m_connection_status.load() == connection_status_t::BROKEN) {
        emit connection_lost_sig();
    }
}




/*
unsigned int get_thread_id(unsigned int pid)
{
    HANDLE h = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
    if (h != INVALID_HANDLE_VALUE) {
        THREADENTRY32 te;
        te.dwSize = sizeof(te);
        if (Thread32First(h, &te)) {
            do {
                if (te.dwSize >= FIELD_OFFSET(THREADENTRY32, th32OwnerProcessID) + sizeof(te.th32OwnerProcessID)) {
                    if (pid == te.th32OwnerProcessID) {

                        PostThreadMessage(te.th32ThreadID, WM_NULL, 0, 0);
                        //return te.th32ThreadID;
                    }
                }
                te.dwSize = sizeof(te);
            } while (Thread32Next(h, &te));
        }
        CloseHandle(h);
    }

    return 0;
}
*/
