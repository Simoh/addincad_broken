#ifndef LINK_H
#define LINK_H

#include <QObject>
#include <Windows.h>
#include <thread>
#include <atomic>
#include <QTimer>

class Link : public QObject
{
    Q_OBJECT
public:
    explicit Link(QObject *parent = 0);
    ~Link();
    bool set_link(std::string name_sender, std::string name_receiver);
    bool set_link_unicode(std::string name_sender, std::string name_receiver);
    void destroy_link();
    bool send_message(const std::string &msg);
    bool send_message(const std::wstring &msg);

private:
    HANDLE hMailslotReceiver;
    HANDLE hMailslotSender;

    std::thread t_receiver;
    void process_msg(unsigned long _msgs, unsigned long _msgs_count);
    void process_msg_unicode(unsigned long _msgs, unsigned long _msgs_count);

    std::atomic<bool> establish_connection;
    QTimer health_timer;

signals:
    void input_message(QString msg); //Link with Qt::QueuedConnection
    void connection_lost();

private slots:
    void health_checker();

public slots:
};

#endif // LINK_H
