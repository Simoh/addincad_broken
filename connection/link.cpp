#include "link.h"
#include <QDebug>

Link::Link(QObject *parent):
    QObject(parent)
  , establish_connection(false)
  , hMailslotReceiver(NULL)
  , hMailslotSender(NULL)
{
    QObject::connect(&health_timer, &QTimer::timeout, this, &Link::health_checker);
}

Link::~Link()
{
    if (establish_connection.load())
        destroy_link();

    QObject::disconnect(&health_timer, &QTimer::timeout, this, &Link::health_checker);
}

bool Link::set_link(std::string name_sender, std::string name_receiver)
{
    //Set sender link
    hMailslotSender = CreateFileA(name_sender.c_str(), GENERIC_WRITE,
        FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
    if (hMailslotSender == INVALID_HANDLE_VALUE)
    {
        qCritical() << "Cannot open sender mailslot";
        return false;
    }

    //Set receiver link
    hMailslotReceiver = CreateMailslotA(name_receiver.c_str(), 0, 0, NULL);
    if (hMailslotReceiver == INVALID_HANDLE_VALUE)
    {
        qCritical() << "Cannot create receiver mailslot";
        destroy_link();
        return false;
    }

    establish_connection.store(true);

    //Initialize thread for check message in receiver channel
    t_receiver = std::thread([&]()
    {
        while(establish_connection.load())
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(200));

            DWORD cbMessages = MAILSLOT_NO_MESSAGE, cbMessagesNumb;
            if (!GetMailslotInfo(hMailslotReceiver, NULL, &cbMessages, &cbMessagesNumb, NULL))
            {
                //If can`t read msg from channel - disconnect
                establish_connection.store(false);
                return;
            }
            if (cbMessages != MAILSLOT_NO_MESSAGE)
                process_msg(cbMessages, cbMessagesNumb);
        }
    });

    //start timer for checking connection state
    health_timer.start(200);

    return true;
}

bool Link::set_link_unicode(std::string name_sender, std::string name_receiver)
{
    //TODO dulicate code
    //Set sender link
    hMailslotSender = CreateFileA(name_sender.c_str(), GENERIC_WRITE,
        FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
    if (hMailslotSender == INVALID_HANDLE_VALUE)
    {
        qCritical() << "Cannot open sender mailslot";
        return false;
    }

    //Set receiver link
    hMailslotReceiver = CreateMailslotA(name_receiver.c_str(), 0, 0, NULL);
    if (hMailslotReceiver == INVALID_HANDLE_VALUE)
    {
        qCritical() << "Cannot create receiver mailslot";
        destroy_link();
        return false;
    }

    establish_connection.store(true);

    //Initialize thread for check message in receiver channel
    t_receiver = std::thread([&]()
    {
        while(establish_connection.load())
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(200));

            DWORD cbMessages = MAILSLOT_NO_MESSAGE, cbMessagesNumb;
            if (!GetMailslotInfo(hMailslotReceiver, NULL, &cbMessages, &cbMessagesNumb, NULL))
            {
                //If can`t read msg from channel - disconnect
                establish_connection.store(false);
                return;
            }
            if (cbMessages != MAILSLOT_NO_MESSAGE)
                process_msg_unicode(cbMessages, cbMessagesNumb);
        }
    });

    //start timer for checking connection state
    health_timer.start(200);

    return true;
}

void Link::destroy_link()
{
    health_timer.stop();
    establish_connection.store(false);

    if (hMailslotReceiver)
    {
        //Wait receiver thread
        if (t_receiver.joinable())
            t_receiver.join();
        CloseHandle(hMailslotReceiver);
    }
    if (hMailslotSender)
        CloseHandle(hMailslotSender);

    hMailslotReceiver = NULL;
    hMailslotSender = NULL;
}

bool Link::send_message(const std::string &msg)
{
    if (establish_connection.load() == false)
        return false;

    if (!WriteFile(hMailslotSender, msg.c_str(), msg.length() * sizeof(char), NULL, NULL))
    {
        if (GetLastError() != ERROR_IO_PENDING)
        {
            qCritical() << "Cannot send message";
            return false;
        }
    }
    return true;
}

bool Link::send_message(const std::wstring &msg)
{
    if (establish_connection.load() == false)
        return false;

    if (!WriteFile(hMailslotSender, msg.c_str(), msg.length() * sizeof(wchar_t), NULL, NULL))
    {
        if (GetLastError() != ERROR_IO_PENDING)
        {
            qCritical() << "Cannot send message";
            return false;
        }
    }
    return true;
}

void Link::process_msg(unsigned long _msgs, unsigned long _msgs_count)
{
    while( _msgs_count )
    {
        if (_msgs > 0)
        {
            qDebug() << "Message in channel. Count : " << _msgs_count;
            std::string recv_data;
            unsigned long cbRead = 0;
            char* buf = new char[_msgs];
            if(ReadFile(hMailslotReceiver, buf, 4096, &cbRead, NULL))
            {
                recv_data.assign(buf, cbRead / sizeof(char));
                emit input_message(QString(recv_data.c_str()));
            }
            else
            {
                qCritical() << "Error Data Transfer";
                establish_connection.store(false);
                return;
            }
        }
        if (!GetMailslotInfo(hMailslotReceiver, NULL, &_msgs, &_msgs_count, NULL))
        {
            qCritical() << "can`t read msg from channel";
            establish_connection.store(false);
            return;
        }
    }
}

void Link::process_msg_unicode(unsigned long _msgs, unsigned long _msgs_count)
{
    while( _msgs_count )
    {
        if (_msgs > 0)
        {
            qDebug() << "Message in channel. Count : " << _msgs_count;
            std::wstring recv_data;
            unsigned long cbRead = 0;
            wchar_t* buf = new wchar_t[_msgs];
            if(ReadFile(hMailslotReceiver, buf, 4096, &cbRead, NULL))
            {
                recv_data.assign(buf, cbRead / sizeof(wchar_t));
                emit input_message(QString::fromWCharArray(recv_data.c_str()));
            }
            else
            {
                qCritical() << "Error Data Transfer";
                establish_connection.store(false);
                return;
            }
        }
        if (!GetMailslotInfo(hMailslotReceiver, NULL, &_msgs, &_msgs_count, NULL))
        {
            qCritical() << "can`t read msg from channel";
            establish_connection.store(false);
            return;
        }
    }
}

void Link::health_checker()
{
    if (establish_connection.load() == false)
    {
        emit connection_lost();
        this->destroy_link();
    }
}

