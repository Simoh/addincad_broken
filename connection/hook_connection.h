#ifndef HOOK_CONNECTION_H
#define HOOK_CONNECTION_H

#include <QDialog>

#include "connection/iconnection.h"
#include "service/timer.h"

class Hook_connection : public IConnection
{
    Q_OBJECT

public:
    explicit Hook_connection(QObject *parent = 0);
    ~Hook_connection();
    bool connect_to_app(uint pid, std::chrono::seconds timeout = std::chrono::seconds(10));
    void disconnect_from_app();
    void pause();
    void unpause();

private:
    typedef void(CALLBACK *function_hook_dll)(HWINEVENTHOOK hWinEventHook,
                                          DWORD event, HWND hwnd,
                                          LONG idObject, LONG idChild,
                                          DWORD dwEventThread,
                                          DWORD dwmsEventTime);
    HMODULE HookLib;
    /* function in dll*/
    function_hook_dll f_hook;
    void(*f_set_pause_hook)();
    void(*f_unset_pause_hook)();    
    bool load_functions();

    HANDLE hMailslot;
    bool initialize_mailslot(uint pid);

    Timer data_checker;

    HWINEVENTHOOK m_hook;

private slots:
    void data_in_channel();
};

#endif // HOOK_CONNECTION_H
