#ifndef ICONNECTION_H
#define ICONNECTION_H

#include <QObject>
#include <atomic>
#include <chrono>
#include <thread>
#include <Windows.h>

enum connection_status_t {
    DISCONNECTED,
    CONNECTED,
    BROKEN
};

class IConnection : public QObject
{
    Q_OBJECT
public:
    explicit IConnection(QObject *parent = nullptr);
    virtual bool connect_to_app(uint pid, std::chrono::seconds timeout = std::chrono::seconds(10));
    virtual void disconnect_from_app();
    bool is_established();
    virtual void pause() = 0;
    virtual void unpause() = 0;

signals:
    void connection_lost_sig();
    void receive_data_sig(QString msg);

public slots:

protected:
    std::atomic<connection_status_t> m_connection_status;

private:
    std::thread health_checker_thread;
    std::chrono::seconds m_timeout_to_connect;
    HANDLE sync_with_app(uint pid);
    void health_check(HANDLE health_obj);

};

#endif // ICONNECTION_H
