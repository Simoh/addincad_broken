#ifndef ERRORS
#define ERRORS

#include <QString>
#include <iostream>
#include <exception>


class cancel_event: public std::runtime_error
{
public:
    cancel_event(const std::string &str) : std::runtime_error(str.c_str()){}
};

class critical_error: public std::runtime_error
{
public:
    critical_error(const std::string &str) : std::runtime_error(str){}
};


namespace Errors
{
    const std::logic_error target_app_is_closed("Целевое приложение закрыто");
    const std::logic_error handle_null("Не указано целевое приложение");
    const std::logic_error no_data_journal("В журнале нет данных");
    const std::logic_error file_not_changed("Файл не выбран");

    const std::logic_error cannot_load_journal("Ошибка при загрузке данных");

    const cancel_event user_canceled_action("Пользователь отменил действие");

    namespace Database
    {
        const std::logic_error filename_is_empty("Файл не выбран");
        const std::logic_error filename_is_bad("Некорректное имя файла");
        const std::logic_error bad_file_format("Некорректный файл");
        const std::logic_error file_zip_is_empty("Не выбран файл aic");
        const std::logic_error json_data_error("Ошибка в формате json файла");
        const std::logic_error file_hasnt_sessions("В файле остутствуют сессии");
    }
    namespace Hook
    {
        const critical_error cannot_load_data("Невозможно загрузить данные");
        const critical_error cannot_load_dll("Невозможно загрузить функцию хука");
        const critical_error cannot_connect_to_dll("Невозможно установить подключение к dll");
        const critical_error cannot_set_hook("Ошибка при установке хука");
        const critical_error cannot_initialize_receiver_data("Ошибка при инициализации объекта Reciever_data");
        const critical_error cannot_connect_to_app("Невозможно установить подключение к целевому приложению");
    }
    namespace Registrator
    {
        const critical_error registrator_cannot_start("Ошибка при запуске регистратора");
    }
    namespace Player
    {
        const critical_error player_cannot_start("Ошибка при запуске проигрывателя");
    }
}
#endif // ERRORS

