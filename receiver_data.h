#ifndef RECEIVER_DATA_H
#define RECEIVER_DATA_H

#include <QDialog>

#include "connection/iconnection.h"
#include "update_watchdog.h"

namespace Ui {
class Receiver_data;
}

class Connection_manager : public QDialog
{
    Q_OBJECT

public:
    explicit Connection_manager(QWidget *parent = 0);
    ~Connection_manager();

    QString set_connection_with_app(unsigned int pid);
    void set_connection_with_app_hook(unsigned int pid);
    void set_connection_with_app_integration(unsigned int pid);
    void set_update_watchdog(unsigned int pid);
    void disconnect_app();
    void set_pause();
    void unset_pause();
    bool send_msg_to_connection_app(QString raw_data);

signals:
    void recv_hot_msg();
    void recv_data_in_channel(QString msg);
    void dll_connection_refused(QString);
    void update_position_msg(); //Link Qt::QueuedConnection
    void update_msg(int); //Link Qt::QueuedConnection

private:
    Ui::Receiver_data *ui;

    //Connections
    std::shared_ptr<IConnection> m_connection;

    Update_watchdog update_watch;

    void connect_to_iconnection_signals();
    void disconnect_from_iconnection_signals();

public slots:
    void data_in_link(QString msg);
    void connection_lost();
    void disconnect_from_app();
};

#endif // RECEIVER_DATA_H
