#include "dialogchoosejournal.h"
#include "ui_dialogchoosejournal.h"
#include <QDebug>

DialogChooseJournal::DialogChooseJournal(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogChooseJournal)
{
    ui->setupUi(this);
}

DialogChooseJournal::~DialogChooseJournal()
{
    delete ui;
}

QString DialogChooseJournal::choose_name(QStringList &_input_list)
{
    qDebug() << "choose_name started";
    QString result;

    this->setModal(true);
    this->ui->listWidget->addItems(_input_list);
    if (this->exec() == QDialog::Accepted)
        result = this->ui->listWidget->currentItem()->text();

    return result;
}
