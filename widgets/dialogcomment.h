#ifndef DIALOGCOMMENT_H
#define DIALOGCOMMENT_H

#include <QDialog>

namespace Ui {
class DialogComment;
}

class DialogComment : public QDialog
{
    Q_OBJECT

public:
    explicit DialogComment(QWidget *parent = 0);
    ~DialogComment();
    bool edit_comment(QString& comment);

private:
    Ui::DialogComment *ui;

private slots:
    void on_buttonBox_accepted();
};

#endif // DIALOGCOMMENT_H
