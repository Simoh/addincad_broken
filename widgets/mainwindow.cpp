#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMenu>
#include <QResource>
#include <QDebug>
#include <QMetaType>
#include <QDesktopServices>
#include <QMessageBox>

#include "errors.h"
#include "service/globalsettings.h"
#include "configurator/process_monitor.h"

void MainWindow::set_start_position()
{
    QRect rect = QApplication::desktop()->availableGeometry();
    rect.setX(rect.width() - this->width());
    rect.setY(rect.height() - this->height());
    this->setGeometry(rect);
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qRegisterMetaType<std::shared_ptr<Context>>("std::shared_ptr<Context>");
    setlocale(LC_ALL, "ru");

    this->setTrayIconActions();
    this->showTrayIcon();

    this->setWindowFlags(Qt::WindowStaysOnTopHint);
    //this->setAttribute(Qt::WA_DeleteOnClose);
    m_load_viewer.setModal(true);

    ui->stackedWidget->addWidget(&this->registrator_widget);
    ui->stackedWidget->addWidget(&this->main_widget);
    ui->stackedWidget->addWidget(&this->m_player_widget);
    QObject::connect(&m_registrator, &Registrator::action_saved, &registrator_widget, &RegistratorWidget::add_action);
    QObject::connect(&registrator_widget, &RegistratorWidget::comment_changed, &m_registrator, &Registrator::comment_changed);
    QObject::connect(&m_player, &Player::new_action_started, &m_player_widget, &PlayerListWidget::new_active_action);
    QObject::connect(&main_widget, &MainWidget::app_choosen, this, &MainWindow::app_choosen);


    m_registrator.moveToThread(&registrator_thread);
    m_player.moveToThread(&player_thread);
    m_update_checker.moveToThread(&updater_thread);
    QObject::connect(&updater_thread, &QThread::started, &m_update_checker, &UpdateChecker::check_update);
    QObject::connect(&m_update_checker, &UpdateChecker::install_update, this, &MainWindow::install_update, Qt::BlockingQueuedConnection);

    registrator_thread.start();
    player_thread.start();
    updater_thread.start();

    ui->stackedWidget->setCurrentWidget(&this->main_widget);
    //set_start_position();
}


MainWindow::~MainWindow()
{
    registrator_thread.quit();
    player_thread.quit();
    updater_thread.quit();

    trayIcon->hide();
    delete ui;
}

/***************************************************************************************************
 *
 *                                P U B L I C    S L O T S
 *
 **************************************************************************************************/

void MainWindow::edit_journal(QString _db_name, QString _journal_name, int _id_element)
{
    qDebug() << "Edit Journal Accept from MainWindow";
    qDebug() << "name DB : " << _db_name << " name Journal : " << _journal_name << " id : " << _id_element;
    //останавливаем проигрыватель
    emit stop();
    //disconnect from player
    //connect to registrator
    //emit start registrator with journal
    qDebug() << "TODO: Set buttons and emit start(pid,player)";
}

void MainWindow::state_changed(QString description)
{
    m_load_viewer.set_description(description);
}

void MainWindow::app_choosen(uint pid)
{
    set_default_parameters();
}

void MainWindow::started()
{
    qDebug() << "object was started : " << QObject::sender()->objectName();
    m_load_viewer.loading_success(tr("Соединение успешно установлено"));
    m_status_widget.set_caption(QObject::sender()->objectName(), "running");
}

void MainWindow::stopped(QString reason)
{
    qDebug() << "Object was stopped: " << reason;

    this->disconnect_from_worker( dynamic_cast<worker_impl*>( QObject::sender() ) );

    if (m_status == state::REGISTRATION){
        //disconnect form registrator specific slosts
        QObject::disconnect(&m_registrator, QOverload<QString, JournalSession>::of(&Registrator::stopped),
                            this, QOverload<QString, JournalSession>::of(&MainWindow::stopped));
    } else if (m_status == state::PLAYING) {
        QObject::disconnect(this, &MainWindow::next_action, &m_player, &Player::next_action);
        QObject::disconnect(&m_player, &Player::hint_created, &m_hint_manager, &HintManager::hint_created);
        m_hint_manager.clear();
    }

    set_default_parameters();

    if ( QString::compare(reason, Global_constant::stop_reasons::playing_was_finished.c_str()) == 0)
        QMessageBox::information(this, tr("Завершено"), reason);
    else if ( QString::compare(reason, Global_constant::stop_reasons::default_reason.c_str()) != 0) {
        QMessageBox::critical(this, tr("Ошибка"), reason);
        m_load_viewer.loading_failed();
    }

    m_status_widget.clear();
}

void MainWindow::stopped(QString reason, JournalSession session)
{
    if (QString::compare(reason, Global_constant::stop_reasons::default_reason.c_str()) == 0) {
        if (session.is_empty())
            QMessageBox::information(QApplication::activeWindow(), tr("Регистрация остановлена"), tr("Сессия пуста"));
        else {
            try {
                m_data_base.save_journal(session);
            }
            catch(exception& ex) {
                qDebug() << ex.what();
                QMessageBox::critical(QApplication::activeWindow(),
                                      tr("Ошибка"),tr(ex.what()));
            }
        }
    }
    this->stopped(reason);
}

void MainWindow::resumed()
{
    /* Switch picture on play button to pause */
    this->ui->play_button->setStyleSheet("qproperty-icon: url(:/images/img/buttons/pause.png);");
    m_status_widget.update_status(tr("running"));
}

void MainWindow::paused()
{
    /* Switch picture on play button to play */
    this->ui->play_button->setStyleSheet("qproperty-icon: url(:/images/img/buttons/playback-start.png);");
    m_status_widget.update_status(tr("paused"));
}

/***************************************************************************************************
 *
 *                                  F U N C T I O N S
 *
 **************************************************************************************************/

/**
 * @brief Function set standart enable status for all buttons on main form. Connect registrator button
 * with registrator signal, play button with player signal
 */
void MainWindow::set_default_parameters()
{
    qDebug() << "default_params started";

    this->ui->Panel_main->setEnabled(true);
    this->ui->record_button->setEnabled(true);
    this->ui->play_button->setEnabled(true);
    this->ui->play_button->setStyleSheet("qproperty-icon: url(:/images/img/buttons/playback-start.png);");
    this->ui->stop_button->setEnabled(false);
    this->ui->next_button->setEnabled(false);

    ui->stackedWidget->setCurrentWidget(&this->main_widget);

    m_status = state::IDLE;
}

/**
 * @brief This function search HWND (process ID) window and try start registrator
 */
void MainWindow::on_record_button_clicked()
{
    qDebug() << "on_record_button_clicked started";

    try {
        set_state_registration();
        //load or create new db
        if (QMessageBox::question(this, tr("Выбор журнала"),
                                     tr("Записать в новый файл?")) == QMessageBox::Yes)
        {
            m_data_base.clear();
            m_load_viewer.loading_start();
            emit start(GlobalSettings::Instance().current_process_id());
        } else {
            m_data_base.load_from_file();
            m_load_viewer.loading_start();
            emit start(GlobalSettings::Instance().current_process_id(),
                       m_data_base.get_config());
        }
    }
    catch(const cancel_event& ex) {
        set_default_parameters();
    }
    catch(const exception& ex) {
        qCritical() << ex.what();
        set_default_parameters();
        QMessageBox::critical(QApplication::activeWindow(), tr("Ошибка"), ex.what());
    }
}

/**
 * @brief This function search HWND window and try start player or pause
 */
void MainWindow::on_play_button_clicked()
{
    qDebug() << "on_play_button_clicked started";

    //проверяем статус
    switch (m_status)
    {
    case state::IDLE:
    {
        qDebug() << "Starting player";
        try {
            m_data_base.load_from_file();
            set_state_player();
            auto tmp_session = m_data_base.get_journal();
            m_player_widget.show_actions(tmp_session);
            m_load_viewer.loading_start();
            emit start(GlobalSettings::Instance().current_process_id(), tmp_session);
        }
        catch(const cancel_event& ex) {
            set_default_parameters();
        }
        catch(const exception& ex) {
            qCritical() << ex.what();
            QMessageBox::critical(QApplication::activeWindow(), tr("Ошибка"), ex.what());
            set_default_parameters();
        }
        break;
    }
    default:
    {
        emit pause();
    }
    }
}

void MainWindow::on_stop_button_clicked()
{
    qDebug() << "on_stop_button_clicked started";
    //остановить действия
    emit stop();
}

void MainWindow::on_MainWindow_destroyed()
{
    //TODO fix it
    emit stop();
    this->destroy();
}

/**
 * @brief Function link stop button and registrator stop slot,
 * Make button play to "pause".
 */
void MainWindow::set_state_registration()
{
    qDebug() << "set_state_registration started";

    /* Lock record button, comboBox */
    this->ui->record_button->setEnabled(false);
    this->ui->stop_button->setEnabled(true);
    this->ui->next_button->setEnabled(false);

    /* Switch picture on play button to pause */
    this->ui->play_button->setStyleSheet("qproperty-icon: url(:/images/img/buttons/pause.png);");

    /* Connect signals */
    connect_to_worker(dynamic_cast<worker_impl*>(&m_registrator));
    //specific signals for registrator
    QObject::connect(&m_registrator, QOverload<QString, JournalSession>::of(&Registrator::stopped),
                     this, QOverload<QString, JournalSession>::of(&MainWindow::stopped));

    registrator_widget.clear();
    ui->stackedWidget->setCurrentWidget(&registrator_widget);

    m_status = state::REGISTRATION;
}

void MainWindow::set_state_player()
{
    qDebug() << "set_state_player started";

    //заблокирвать все элементы UI кроме кнопок управления
    this->ui->record_button->setEnabled(false);
    this->ui->stop_button->setEnabled(true);
    this->ui->next_button->setEnabled(true);

    this->ui->stackedWidget->setCurrentWidget(&m_player_widget);

    /* Switch picture on play button to pause */
    this->ui->play_button->setStyleSheet("qproperty-icon: url(:/images/img/buttons/pause.png);");

    /* Connect signals */
    connect_to_worker(dynamic_cast<worker_impl*>(&m_player));
    //specific signals for player
    QObject::connect(this, &MainWindow::next_action, &m_player, &Player::next_action);
    QObject::connect(&m_player, &Player::hint_created, &m_hint_manager, &HintManager::hint_created);

    m_status = state::PLAYING;
}

void MainWindow::disconnect_from_worker(worker_impl *sender_obj)
{
    QObject::disconnect(this, QOverload<ulong>::of(&MainWindow::start),
                        sender_obj, QOverload<ulong>::of(&worker_impl::start) );

    QObject::disconnect(this, QOverload<ulong, std::shared_ptr<Context>>::of(&MainWindow::start),
                        sender_obj, QOverload<ulong, std::shared_ptr<Context>>::of(&worker_impl::start) );

    QObject::disconnect(this, QOverload<ulong,JournalSession,int>::of(&MainWindow::start),
                       sender_obj, QOverload<ulong,JournalSession,int>::of(&worker_impl::start) );

    QObject::disconnect(this, &MainWindow::stop,       sender_obj, &worker_impl::stop);
    QObject::disconnect(this, &MainWindow::pause,      sender_obj, &worker_impl::pause);
    QObject::disconnect(this, &MainWindow::get_state,  sender_obj, &worker_impl::get_status);

    QObject::disconnect(sender_obj, &worker_impl::paused,  this, &MainWindow::paused);
    QObject::disconnect(sender_obj, &worker_impl::resumed, this, &MainWindow::resumed);
    QObject::disconnect(sender_obj, &worker_impl::started, this, &MainWindow::started);
    QObject::disconnect(sender_obj, &worker_impl::stopped, this, QOverload<QString>::of(&MainWindow::stopped) );

    QObject::disconnect(sender_obj, &worker_impl::state_changed, this, &MainWindow::state_changed);
}

void MainWindow::connect_to_worker(worker_impl *connectable_obj)
{
    QObject::connect(this, QOverload<ulong>::of(&MainWindow::start),
                        connectable_obj, QOverload<ulong>::of(&worker_impl::start) );

    QObject::connect(this, QOverload<ulong, std::shared_ptr<Context>>::of(&MainWindow::start),
                        connectable_obj, QOverload<ulong, std::shared_ptr<Context>>::of(&worker_impl::start) );

    QObject::connect(this, QOverload<ulong,JournalSession,int>::of(&MainWindow::start),
                       connectable_obj, QOverload<ulong,JournalSession,int>::of(&worker_impl::start) );

    QObject::connect(this, &MainWindow::stop,       connectable_obj, &worker_impl::stop);
    QObject::connect(this, &MainWindow::pause,      connectable_obj, &worker_impl::pause);
    QObject::connect(this, &MainWindow::get_state,  connectable_obj, &worker_impl::get_status, Qt::BlockingQueuedConnection);

    QObject::connect(connectable_obj, &worker_impl::paused,  this, &MainWindow::paused);
    QObject::connect(connectable_obj, &worker_impl::resumed, this, &MainWindow::resumed);
    QObject::connect(connectable_obj, &worker_impl::started, this, &MainWindow::started);
    QObject::connect(connectable_obj, &worker_impl::stopped, this, QOverload<QString>::of(&MainWindow::stopped) );

    QObject::connect(connectable_obj, &worker_impl::state_changed, this, &MainWindow::state_changed);
}

void MainWindow::on_CreateNewConfig_triggered()
{
    qDebug() << "on_CreateNewConfig_triggered started";
    /*if (this->ui->comboBox->currentText().isEmpty())
    {
        QMessageBox::information(QApplication::activeWindow(), "Нет данных для данного действия",
                                 "Не указана среда для которой будет создан файл.", "Ок");
        qDebug() << "No data for this action";
        return;
    }*/

    DialogConfig conf_dlg;
    conf_dlg.setModal(true);
    conf_dlg.exec();
}


/*
 * Tray operations
 */

void MainWindow::changeEvent(QEvent *event)
{
    //Tray
    if (event->type() == QEvent::WindowStateChange)
    {
        if (isMinimized())
        {
            this->hide();
        }
    }
    //end Tray
}

void MainWindow::showTrayIcon()
{
    //Create Icon class
    trayIcon = new QSystemTrayIcon();
    QIcon trayImage(":/images/img/tray_icon.png");
    trayIcon->setIcon(trayImage);
    trayIcon->setContextMenu(trayIconMenu);

    connect(trayIcon, &QSystemTrayIcon::activated, this, &MainWindow::trayIconActivated);

    trayIcon->show();
}

void MainWindow::trayActionExecute()
{
    if (this->isHidden())
        this->showNormal();
    else this->hide();
}

void MainWindow::trayIconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason)
    {
    case QSystemTrayIcon::DoubleClick:
        this->trayActionExecute();
        break;
    default:
        break;
    }
}

void MainWindow::setTrayIconActions()
{
    // Setting actions
    minimizeAction = new QAction("Свернуть", this);
    restoreAction = new QAction("Восстановить", this);
    quitAction = new QAction("Выход", this);

    // Connecting actions to slots
    connect(minimizeAction, &QAction::triggered, this, &MainWindow::hide);
    connect(restoreAction, &QAction::triggered, this, &MainWindow::showNormal);
    connect(quitAction, &QAction::triggered, qApp, &QCoreApplication::quit);

    // Setting system tray's icon menu
    trayIconMenu = new QMenu();
    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addAction(quitAction);
}
//end Tray

void MainWindow::on_next_button_clicked()
{
    if (m_status == state::PLAYING) {
        emit next_action();
    }
}

void MainWindow::on_GoToAddincad_triggered()
{
    QDesktopServices::openUrl(QUrl("http://www.addincad.com/"));
}

bool MainWindow::install_update(QString description)
{
    return !QMessageBox::question(this, tr("Доступно новое обновление"),
                                  description,
                                  tr("Установить сейчас"),
                                  tr("Установить позже"));
}
