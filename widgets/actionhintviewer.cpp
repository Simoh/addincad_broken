#include "actionhintviewer.h"
#include "ui_actionhintviewer.h"

#include <QPainter>


ActionHintViewer::ActionHintViewer(std::shared_ptr<DisplayedActionCore> p_action, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ActionHintViewer)
{
    ui->setupUi(this);

    this->setFocusPolicy( Qt::NoFocus );
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::Tool | Qt::WindowTransparentForInput);
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setAttribute( Qt::WA_NoSystemBackground);
    this->setStyleSheet("background:transparent;");
    m_target_animation = new QMovie(":/images/img/buttons/target.gif");
    ui->target_label->setScaledContents(true);
    ui->target_label->setMovie(m_target_animation);
    ui->target_label->setVisible(true);

    m_action = p_action;
    QObject::connect(m_action.get(), &DisplayedActionCore::updated, this, &ActionHintViewer::update_pos);
    this->update_pos();
}

ActionHintViewer::~ActionHintViewer()
{
    QObject::disconnect(m_action.get(), &DisplayedActionCore::updated, this, &ActionHintViewer::update_pos);
    delete ui;
}

void ActionHintViewer::update_pos()
{
    this->show();
    auto coords = m_action.get()->get_frame_pos();
    if (!coords.first.isEmpty()) {
        this->setGeometry(coords.first);
        if (!coords.second.isNull()) {
            m_target_animation->start();
            ui->target_label->setVisible(true);
        } else {
            m_target_animation->stop();
            ui->target_label->setVisible(false);
        }
        this->setVisible(true);
    } else {
        this->setVisible(false);
    }
    //TODO
}

void ActionHintViewer::paintEvent(QPaintEvent *_event)
{
    QPainter painter(this);
    painter.setPen(QPen(Qt::blue, 3, Qt::SolidLine));
    painter.drawRect(this->rect());
    QWidget::paintEvent(_event);
}
