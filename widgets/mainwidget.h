#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>

namespace Ui {
class MainWidget;
}

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MainWidget(QWidget *parent = 0);
    ~MainWidget();

private:
    Ui::MainWidget *ui;

protected:
    bool eventFilter(QObject* _obj, QEvent* _event);

signals:
    void app_choosen(uint pid);
private slots:
    void on_comboBox_activated(const QString &arg1);
};

#endif // MAINWIDGET_H
