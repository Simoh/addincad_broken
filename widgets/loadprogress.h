#ifndef LOADPROGRESS_H
#define LOADPROGRESS_H

#include <QDialog>
#include <QMovie>
#include <QMouseEvent>

namespace Ui {
    class LoadProgress;
}

class LoadProgress : public QDialog
{
    Q_OBJECT

public:
    explicit LoadProgress(QWidget *parent = 0);
    ~LoadProgress();

    void loading_start(QString comment = tr("Подождите, идёт загрузка"));
    void loading_success(QString comment = tr("Операция завершена успешно"));
    void loading_failed(QString comment = tr("Операция завершена неудачно"));

public slots:
    void set_description(QString description = tr("Подождите, идёт загрузка"));

private:
    Ui::LoadProgress *ui;
    QMovie *movie_load;
    QMovie *movie_success;
    QMovie *movie_failed;

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    int m_nMouseClick_X_Coordinate;
    int m_nMouseClick_Y_Coordinate;
};

#endif // LOADPROGRESS_H
