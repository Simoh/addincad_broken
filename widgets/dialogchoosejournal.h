#ifndef DIALOGCHOOSEJOURNAL_H
#define DIALOGCHOOSEJOURNAL_H

#include <QDialog>

namespace Ui {
class DialogChooseJournal;
}

class DialogChooseJournal : public QDialog
{
    Q_OBJECT
public:
    explicit DialogChooseJournal(QWidget *parent = 0);
    ~DialogChooseJournal();
    QString choose_name(QStringList &_input_list);

private:
    Ui::DialogChooseJournal *ui;
};

#endif // DIALOGCHOOSEJOURNAL_H
