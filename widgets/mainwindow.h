#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDesktopWidget>
#include <QWindow>
#include <QSystemTrayIcon>
#include <QThread>

#include <Windows.h>

#include "service/database.h"
#include "service/updatechecker.h"

#include "base_elements/worker_impl.h"
#include "journals/journal_session.h"
#include "configurator/context.h"

#include "player/player.h"
#include "player/playerlistwidget.h"
#include "player/hintmanager.h"
#include "registrator/registrator.h"
#include "registrator/registratorwidget.h"

#include "statuswidget.h"
#include "mainwidget.h"
#include "dialogconfig.h"
#include "loadprogress.h"

namespace Ui {
class MainWindow;
}

enum state
{
    IDLE,
    REGISTRATION,
    PLAYING
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void started();
    void stopped(QString reason);
    void stopped(QString reason, JournalSession session);
    void resumed();
    void paused();

    void edit_journal(QString _db_name, QString _journal_name, int _id_element);

    void state_changed(QString description); //object should use it for change description on load

    void app_choosen(uint pid);

private slots:
    void on_record_button_clicked();
    void on_stop_button_clicked();
    void on_play_button_clicked();

    //Tray
    void trayIconActivated(QSystemTrayIcon::ActivationReason reason);
    void trayActionExecute();
    void setTrayIconActions();
    void showTrayIcon();
    //end Tray

    void changeEvent(QEvent*);
    void on_MainWindow_destroyed();
    void on_CreateNewConfig_triggered();

    void on_next_button_clicked();

    void on_GoToAddincad_triggered();

    bool install_update(QString description);

signals:
    void stop( QString reason = Global_constant::stop_reasons::default_reason.c_str() );
    void start(unsigned long pid);
    void start(unsigned long pid, std::shared_ptr<Context> config);
    void start(unsigned long pid, JournalSession session, int position = 0);
    void pause();
    void next_action();

    int get_state();

private:
    Ui::MainWindow *ui;
    state m_status;

    QThread registrator_thread;
    QThread player_thread;
    QThread updater_thread;

    Registrator m_registrator;
    Player m_player;
    Database m_data_base;
    UpdateChecker m_update_checker;

    //Tray
    QMenu *trayIconMenu;
    QAction *minimizeAction;
    QAction *restoreAction;
    QAction *quitAction;
    QSystemTrayIcon *trayIcon;
    //end tray

    //default
    void set_default_parameters();
    void set_start_position();
    void set_state_registration();
    void set_state_player();
    void disconnect_from_worker(worker_impl *sender_obj);
    void connect_to_worker(worker_impl *connectable_obj);

    LoadProgress m_load_viewer;
    RegistratorWidget registrator_widget;
    MainWidget main_widget;
    PlayerListWidget m_player_widget;
    HintManager m_hint_manager;
    StatusWidget m_status_widget;
};

#endif // MAINWINDOW_H
