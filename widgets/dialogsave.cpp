#include "dialogsave.h"
#include "ui_dialogsave.h"
#include "redactor/redactorform.h"

#include <QDebug>
#include <QMessageBox>
#include <QApplication>

DialogSave::DialogSave(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogSave)
{
    ui->setupUi(this);
}

bool DialogSave::run(QString& _file_name, JournalSession& session, const QStringList& sessions_list)
{
    if (!_file_name.isEmpty())
    {
        this->ui->NameFileSession->setEnabled(false);
        this->ui->NameFileSession->setText(_file_name);
        this->ui->NameCAD->setEnabled(false);
        this->ui->NameCAD->setText("TODO");
    }

    m_session = &session;
    m_sessions_list = sessions_list;

    if (this->exec() == QDialog::Accepted)
    {
        _file_name = this->ui->NameFileSession->text();
        session.m_name = this->ui->NameSession->text();
        return true;
    }

    return false;
}

DialogSave::~DialogSave()
{
    delete ui;
}

void DialogSave::on_RedactBtn_clicked()
{
    RedactorForm redactor(QApplication::activeWindow());
    redactor.setModal(true);
    redactor.redacting_session(*m_session);
}

void DialogSave::on_SaveBtn_clicked()
{
    if (m_sessions_list.contains(ui->NameSession->text(), Qt::CaseInsensitive)) {
        if (QMessageBox::question(this, tr("Конфликт"),
                              tr("Сессия с таким именем уже существует. Перезаписать её?")) == QMessageBox::No) {
            return;
        }
    }
    this->accept();
}

void DialogSave::on_CancelBtn_clicked()
{
    this->reject();
}
