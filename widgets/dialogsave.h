#ifndef DIALOGSAVE_H
#define DIALOGSAVE_H

#include <QDialog>

#include "journals/journal_session.h"

namespace Ui {
class DialogSave;
}

class DialogSave : public QDialog
{
    Q_OBJECT
public:
    explicit DialogSave(QWidget *parent = 0);
    bool run(QString& _file_name, JournalSession& _session_name, const QStringList& sessions_list);

    ~DialogSave();
private slots:
    void on_RedactBtn_clicked();

    void on_SaveBtn_clicked();

    void on_CancelBtn_clicked();

private:
    Ui::DialogSave *ui;
    JournalSession *m_session;
    QStringList m_sessions_list;
};

#endif // DIALOGSAVE_H
