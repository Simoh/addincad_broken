#include "mainwidget.h"
#include "ui_mainwidget.h"
#include "service/globalsettings.h"

#include "configurator/process_monitor.h"

MainWidget::MainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWidget)
{
    ui->setupUi(this);

    this->ui->comboBox->insertItem(0, "Выберите среду");
    this->ui->comboBox->installEventFilter(this);
}

MainWidget::~MainWidget()
{
    delete ui;
}

bool MainWidget::eventFilter(QObject *_obj, QEvent *_event)
{
    //для того, чтобы сканирование открытых окон выполнялось при клике на combobox
    if ((_event->type() == QEvent::MouseButtonPress) && (_obj == this->ui->comboBox))
    {
        //очистить старые данные
        this->ui->comboBox->clear();
        //add processes list
        this->ui->comboBox->addItems(Process_monitor::Instance().get_open_processes_names());
    }
    return QWidget::eventFilter(_obj, _event);
}

void MainWidget::on_comboBox_activated(const QString &arg1)
{
    Q_UNUSED(arg1);
    qDebug() << "on_comboBox_activated started";

    GlobalSettings::Instance().set_current_process_name(arg1);
    GlobalSettings::Instance().set_current_process_id(Process_monitor::Instance().get_pid_by_string(arg1));
    GlobalSettings::Instance().set_main_wnd(Process_monitor::Instance().get_hwnd_by_string(arg1));

    emit app_choosen(Process_monitor::Instance().get_pid_by_string(arg1));
}
