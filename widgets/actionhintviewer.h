#ifndef ACTIONHINTVIEWER_H
#define ACTIONHINTVIEWER_H

#include <QWidget>
#include <QMovie>
#include <memory>
#include "player/displayedactioncore.h"

namespace Ui {
class ActionHintViewer;
}

class ActionHintViewer : public QWidget
{
    Q_OBJECT

public:
    explicit ActionHintViewer(std::shared_ptr<DisplayedActionCore> p_action, QWidget *parent = 0);
    ~ActionHintViewer();

public slots:
    void update_pos();

protected:
    void paintEvent(QPaintEvent* _event);

private:
    Ui::ActionHintViewer *ui;

    std::shared_ptr<DisplayedActionCore> m_action;
    QMovie* m_target_animation;
};

#endif // ACTIONHINTVIEWER_H
