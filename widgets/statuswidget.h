#ifndef STATUSWIDGET_H
#define STATUSWIDGET_H

#include <QMainWindow>
#include <QTimer>

namespace Ui {
class StatusWidget;
}

class StatusWidget : public QMainWindow
{
    Q_OBJECT

public:
    explicit StatusWidget(QWidget *parent = 0);
    ~StatusWidget();
    void set_caption(QString operation, QString status = "");
    void update_status(QString status);
    void clear();

private:
    Ui::StatusWidget *ui;

    QTimer m_operation_blink_timer;
};

#endif // STATUSWIDGET_H
