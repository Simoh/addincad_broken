#include "dialogcomment.h"
#include "ui_dialogcomment.h"

#include <QDebug>

DialogComment::DialogComment(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogComment)
{
    ui->setupUi(this);
}

DialogComment::~DialogComment()
{
    delete ui;
}

bool DialogComment::edit_comment(QString& comment)
{
    ui->textEditComment->setText(comment);
    if (this->exec() == QDialog::Accepted) {
        comment = ui->textEditComment->toPlainText();
        return true;
    }
    return false;
}

void DialogComment::on_buttonBox_accepted()
{
    qDebug() << "accept comment";
    this->accept();
}
