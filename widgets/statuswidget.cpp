#include "statuswidget.h"
#include "ui_statuswidget.h"

#include <QDesktopWidget>

StatusWidget::StatusWidget(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::StatusWidget)
{
    ui->setupUi(this);

    this->setFocusPolicy( Qt::NoFocus );
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::Tool | Qt::WindowTransparentForInput);
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setAttribute( Qt::WA_NoSystemBackground);
    this->setStyleSheet("background:transparent;");
    this->setGeometry(QApplication::desktop()->availableGeometry());

    connect(&m_operation_blink_timer, &QTimer::timeout, [=] () {
        if (ui->centralwidget->isVisible())
            ui->centralwidget->hide();
        else ui->centralwidget->show();
    });
}

StatusWidget::~StatusWidget()
{
    delete ui;
}

void StatusWidget::set_caption(QString operation, QString status)
{
    ui->label_operation->setText(operation + ": ");
    ui->label_status->setText(status);
    m_operation_blink_timer.start(600);

    this->show();
}

void StatusWidget::update_status(QString status)
{
    ui->label_status->setText(status);
}

void StatusWidget::clear()
{
    m_operation_blink_timer.stop();
    this->hide();
}
