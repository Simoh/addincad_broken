#include "loadprogress.h"
#include "ui_loadprogress.h"

LoadProgress::LoadProgress(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoadProgress)
{
    ui->setupUi(this);

    this->setWindowFlags( Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
    this->setAttribute(Qt::WA_TranslucentBackground);

    this->ui->verticalLayout->setAlignment(Qt::AlignCenter);

    movie_load = new QMovie(":/images/img/spin.gif");
    movie_success = new QMovie(":/img/done.gif");

    this->hide();
}

LoadProgress::~LoadProgress()
{
    movie_load->stop();
    delete ui;
}

void LoadProgress::loading_start(QString comment)
{
    this->set_description(comment);
    ui->animation_label->setMovie(movie_load);
    movie_load->start();
    this->show();
}

void LoadProgress::loading_success(QString comment)
{
    this->set_description(comment);
    movie_load->stop();
    ui->animation_label->setMovie(movie_success);
    connect(movie_success, &QMovie::frameChanged, [=] (int frame_id) {
        if(frame_id == (movie_success->frameCount()-1)) {
            movie_success->stop();
            this->hide();
        }
    });
    movie_success->start();
}

void LoadProgress::loading_failed(QString comment)
{
    movie_load->stop();
    this->hide();
}

void LoadProgress::set_description(QString description)
{
    ui->description_label->setText(description);
}


void LoadProgress::mousePressEvent(QMouseEvent *event) {
    m_nMouseClick_X_Coordinate = event->x();
    m_nMouseClick_Y_Coordinate = event->y();
}

void LoadProgress::mouseMoveEvent(QMouseEvent *event) {
    move(event->globalX()-m_nMouseClick_X_Coordinate,event->globalY()-m_nMouseClick_Y_Coordinate);
}
