#-------------------------------------------------
#
# Project created by QtCreator 2016-06-03T11:00:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

win32 {
    # workaround for qdatetime.h macro bug
    DEFINES += NOMINMAX
}

CONFIG += c++14
TARGET = AIC
TEMPLATE = app

LIBS += -lUser32 -loleacc -lOleAut32 -lOle32

CONFIG(debug, debug|release){
    LIBS += -lquazipd
} else {
    LIBS += -lquazip
}

SOURCES += main.cpp \
        widgets/mainwindow.cpp \
    widgets/dialogsave.cpp \
    service/database.cpp \
    service/dataparser.cpp \
    service/timer.cpp \
    base_elements/action.cpp \
    base_elements/element_info.cpp \
    base_elements/transaction.cpp \
    base_elements/actionintegration.cpp \
    base_elements/actionhook.cpp \
    base_elements/worker_impl.cpp \
    journals/journal_info.cpp \
    journals/journal_session.cpp \
    journals/journal_transaction.cpp \
    player/player.cpp \
    registrator/registrator.cpp \
    widgets/dialogcomment.cpp \
    dialogconfig.cpp \
    configurator/scanner.cpp \
    configurator/transaction_recorder.cpp \
    widgets/dialogchoosejournal.cpp \
    receiver_data.cpp \
    configurator/process_monitor.cpp \
    configurator/runtime_object.cpp \
    connection/link.cpp \
    connection/hook_connection.cpp \
    connection/iconnection.cpp \
    redactor/redactorform.cpp \
    update_watchdog.cpp \
    widgets/loadprogress.cpp \
    registrator/registratorwidget.cpp \
    widgets/mainwidget.cpp \
    player/playerlistwidget.cpp \
    player/hintform.cpp \
    player/hint_viewer.cpp \
    player/client_action_viewer.cpp \
    service/updatechecker.cpp \
    connection/integrationconnection.cpp \
    redactor/actionviewer.cpp \
    redactor/actionhookviewer.cpp \
    redactor/actionintegrationviewer.cpp \
    configurator/context.cpp \
    service/globalsettings.cpp \
    service/imageeditor.cpp \
    service/logger.cpp \
    player/elementwatchdog.cpp \
    widgets/statuswidget.cpp \
    player/displayedactioncore.cpp \
    widgets/actionhintviewer.cpp \
    player/hintmanager.cpp

HEADERS  += widgets/mainwindow.h \
    widgets/dialogsave.h \
    service/database.h \
    service/timer.h \
    service/dataparser.h \
    errors.h \
    global_constants.h \
    base_elements/action.h \
    base_elements/element_info.h \
    base_elements/transaction.h \
    base_elements/iobject.h \
    base_elements/worker_impl.h \
    base_elements/actionintegration.h \
    base_elements/actionhook.h \
    journals/journal_info.h \
    journals/journal_session.h \
    journals/journal_transaction.h \
    registrator/registrator.h \
    registrator/registratorwidget.h \
    widgets/dialogcomment.h \
    dialogconfig.h \
    configurator/scanner.h \
    configurator/transaction_recorder.h \
    configurator/process_monitor.h \
    configurator/runtime_object.h \
    widgets/dialogchoosejournal.h \
    receiver_data.h \
    player/hint_viewer.h \
    player/client_action_viewer.h \
    player/hintform.h \
    player/player.h \
    player/playerlistwidget.h \
    connection/link.h \
    connection/hook_connection.h \
    connection/iconnection.h \
    redactor/redactorform.h \
    update_watchdog.h \
    widgets/loadprogress.h \
    widgets/mainwidget.h \
    service/updatechecker.h \
    connection/integrationconnection.h \
    redactor/actionviewer.h \
    redactor/actionhookviewer.h \
    redactor/actionintegrationviewer.h \
    configurator/context.h \
    base_elements/iserializable.h \
    service/globalsettings.h \
    redactor/iactionviewers.h \
    service/imageeditor.h \
    service/logger.h \
    player/elementwatchdog.h \
    widgets/statuswidget.h \
    player/displayedactioncore.h \
    widgets/actionhintviewer.h \
    player/hintmanager.h

INCLUDEPATH += $$PWD/zip
DEPENDPATH += $$PWD/zip
#C:/Users/123/Documents/Qt/AddInCAD/AIC/zip

FORMS    += widgets/mainwindow.ui \
    widgets/dialogsave.ui \
    widgets/dialogcomment.ui \
    widgets/dialogchoosejournal.ui \
    widgets/mainwidget.ui \
    widgets/loadprogress.ui \
    dialogconfig.ui \
    receiver_data.ui \
    redactor/redactorform.ui \
    update_watchdog.ui \
    registrator/registratorwidget.ui \
    player/playerlistwidget.ui \
    player/playerwidget.ui \
    player/hintform.ui \
    redactor/actionviewer.ui \
    redactor/actionhookviewer.ui \
    redactor/actionintegrationviewer.ui \
    service/imageeditor.ui \
    widgets/statuswidget.ui \
    widgets/actionhintviewer.ui

RESOURCES += \
    main_resource.qrc
